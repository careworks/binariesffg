(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["health-sanitation-health-sanitation-module"],{

/***/ "./src/app/health-sanitation/health-sanitation.config.ts":
/*!***************************************************************!*\
  !*** ./src/app/health-sanitation/health-sanitation.config.ts ***!
  \***************************************************************/
/*! exports provided: HealthSanitationConfig */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HealthSanitationConfig", function() { return HealthSanitationConfig; });
var HealthSanitationConfig = /** @class */ (function () {
    function HealthSanitationConfig() {
    }
    HealthSanitationConfig.toiletDetailsConfig = {
        defaultStyle: false,
        actions: {
            position: "right",
            add: false,
            delete: false
        },
        columns: {
            name: {
                title: 'ToiletDetails',
                editable: false,
                filter: false
            },
            available: {
                title: 'Available',
                filter: false
            },
            repair: {
                title: 'Require repair',
                filter: false
            },
            pristine: {
                title: 'New requirement',
                filter: false
            }
        }
    };
    HealthSanitationConfig.toiletDetailsConfigReadOnly = {
        defaultStyle: false,
        actions: false,
        columns: {
            name: {
                title: 'ToiletDetails',
                editable: false,
                filter: false
            },
            available: {
                title: 'Available',
                filter: false
            },
            repair: {
                title: 'Require repair',
                filter: false
            },
            pristine: {
                title: 'New requirement',
                filter: false
            }
        }
    };
    HealthSanitationConfig.waterPurifierDetailsConfig = {
        defaultStyle: false,
        actions: {
            position: "right",
            add: false,
            delete: false
        },
        columns: {
            name: {
                title: 'WaterPurifierDetails',
                editable: false,
                filter: false
            },
            available: {
                title: 'Available',
                filter: false
            },
            repair: {
                title: 'Require repair',
                filter: false
            },
            pristine: {
                title: 'New requirement',
                filter: false
            }
        }
    };
    HealthSanitationConfig.toiletModel = [
        {
            "available": 0,
            "pristine": 0,
            "name": "Toilet for boys",
            "repair": 0
        },
        {
            "available": 0,
            "pristine": 0,
            "name": "Urinals for boys",
            "repair": 0
        },
        {
            "available": 0,
            "pristine": 0,
            "name": "Toilet for girls",
            "repair": 0
        },
        {
            "available": 0,
            "pristine": 0,
            "name": "Urinals for girls",
            "repair": 0
        },
        {
            "available": 0,
            "pristine": 0,
            "name": "Toilets for CWSN",
            "repair": 0
        },
        {
            "available": 0,
            "pristine": 0,
            "name": "Toilets for Staff",
            "repair": 0
        }
    ];
    HealthSanitationConfig.waterPurifierModel = [
        {
            "available": 0,
            "pristine": 0,
            "name": "RO Unit",
            "repair": 0
        },
        {
            "available": 0,
            "pristine": 0,
            "name": "UV Unit",
            "repair": 0
        },
        {
            "available": 0,
            "pristine": 0,
            "name": "Storage facility",
            "repair": 0
        }
    ];
    HealthSanitationConfig.waterPurifierDetailsConfigReadOnly = {
        defaultStyle: false,
        actions: false,
        columns: {
            name: {
                title: 'WaterPurifierDetails',
                editable: false,
                filter: false
            },
            available: {
                title: 'Available',
                filter: false
            },
            repair: {
                title: 'Require repair',
                filter: false
            },
            pristine: {
                title: 'New requirement',
                filter: false
            }
        }
    };
    return HealthSanitationConfig;
}());



/***/ }),

/***/ "./src/app/health-sanitation/health-sanitation.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/health-sanitation/health-sanitation.module.ts ***!
  \***************************************************************/
/*! exports provided: HealthSanitationPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HealthSanitationPageModule", function() { return HealthSanitationPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _health_sanitation_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./health-sanitation.page */ "./src/app/health-sanitation/health-sanitation.page.ts");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/fesm5/ng2-smart-table.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: '',
        component: _health_sanitation_page__WEBPACK_IMPORTED_MODULE_5__["HealthSanitationPage"]
    }
];
var HealthSanitationPageModule = /** @class */ (function () {
    function HealthSanitationPageModule() {
    }
    HealthSanitationPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
                ng2_smart_table__WEBPACK_IMPORTED_MODULE_6__["Ng2SmartTableModule"],
            ],
            declarations: [_health_sanitation_page__WEBPACK_IMPORTED_MODULE_5__["HealthSanitationPage"]],
            schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["CUSTOM_ELEMENTS_SCHEMA"]]
        })
    ], HealthSanitationPageModule);
    return HealthSanitationPageModule;
}());



/***/ }),

/***/ "./src/app/health-sanitation/health-sanitation.page.html":
/*!***************************************************************!*\
  !*** ./src/app/health-sanitation/health-sanitation.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content padding>\r\n    <ion-button color=\"tertiary\" (click)=\"startEditing()\" *ngIf=\"!hasStarted\">\r\n       <ion-icon name=\"arrow-forward\" padding></ion-icon>\r\n       Begin</ion-button>\r\n    <ion-button color=\"primary\" (click)=\"isEditing = !isEditing\" *ngIf=\"hasStarted && !isEditing\">\r\n      <ion-icon name=\"create\" padding></ion-icon>\r\n      Edit</ion-button>\r\n    <ion-button color=\"secondary\" (click)=\"saveHealthSanitationDetailsAndContinue()\" *ngIf=\"isEditing\" (click)=\"isEditing = !isEditing\"> \r\n     <ion-icon name=\"save\" padding></ion-icon>\r\n      Save & Continue</ion-button>\r\n    <ion-button color=\"tertiary\" (click)=\"saveHealthSanitationDetails()\" *ngIf=\"isEditing\" (click)=\"isEditing = !isEditing\"> \r\n        <ion-icon name=\"clipboard\" padding></ion-icon>\r\n    Save</ion-button>\r\n      <div *ngIf=\"hasStarted && !isEditing\">\r\n          <ion-card padding>\r\n              <ion-card-body>\r\n                  <ion-grid>\r\n                      <ion-row>\r\n                          <ion-col>\r\n                              <ion-item>\r\n                                  <ion-label>Is there a first aid box in the school?</ion-label>\r\n                              </ion-item>\r\n                          </ion-col>\r\n                          <ion-col>\r\n                              <ion-item>\r\n                                  <ion-label>{{healthSanitation.firstAidBox}}</ion-label>\r\n                              </ion-item>    \r\n                          </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                          <ion-col>\r\n                              <ion-item>\r\n                                  <ion-label>Is Medicine available?</ion-label>\r\n                              </ion-item>\r\n                          </ion-col>\r\n                          <ion-col>\r\n                              <ion-item>\r\n                                  <ion-label>{{healthSanitation.medicine}}</ion-label>\r\n                              </ion-item>\r\n                          </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                          <ion-col>\r\n                              <ion-item>\r\n                                  <ion-label>Are Health Camps Organised?</ion-label>\r\n                              </ion-item>\r\n                          </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                          <ion-col>\r\n                              <ion-item>\r\n                                  <ion-label>1. Dental?</ion-label>\r\n                              </ion-item>\r\n                          </ion-col>\r\n                          <ion-col>\r\n                              <ion-item>\r\n                                  <ion-label>{{healthSanitation.healthCamp.dental}}</ion-label>\r\n                              </ion-item>\r\n                          </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                          <ion-col>\r\n                              <ion-item>\r\n                                  <ion-label>2. Eye?</ion-label>\r\n                              </ion-item>\r\n                          </ion-col>\r\n                          <ion-col>\r\n                              <ion-item>\r\n                                  <ion-label>{{healthSanitation.healthCamp.eye}}</ion-label>\r\n                              </ion-item>\r\n                          </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                          <ion-col>\r\n                              <ion-item>\r\n                                  <ion-label>3. General?</ion-label>\r\n                              </ion-item>\r\n                          </ion-col>\r\n                          <ion-col>\r\n                              <ion-item>\r\n                                  <ion-label>{{healthSanitation.healthCamp.general}}</ion-label>\r\n                              </ion-item>\r\n                          </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>Is Wash Program implemented?</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                          </ion-row>\r\n                      <ion-row>\r\n                          <ion-col>\r\n                              <ion-item>\r\n                                  <ion-label>1. Hand wash Program?</ion-label>\r\n                              </ion-item>\r\n                          </ion-col>\r\n                          <ion-col>\r\n                              <ion-item>\r\n                                  <ion-label>{{healthSanitation.washProgram.handWash}}</ion-label>\r\n                              </ion-item>\r\n                          </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>2. Toilet usage awareness?</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>{{healthSanitation.washProgram.toiletUsage}}</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>3. Awareness on safe drinking water?</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>{{healthSanitation.washProgram.drinkingWater}}</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>Is there a Handwash area?</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>{{healthSanitation.handWashArea}}</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>Is rennovation required?</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>{{healthSanitation.renovation}}</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>Is there a seperate utensil washing area?</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>{{healthSanitation.utensilWashing}}</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                            <ion-col>\r\n                                <ion-item>\r\n                                    <ion-label>Toilet Details</ion-label>\r\n                                </ion-item>\r\n                            </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                            <ng2-smart-table [settings]=\"toiletDetailssettingsReadOnly\" \r\n                            [source]=\"toiletDetailsdata\" ></ng2-smart-table>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                            <ion-col>\r\n                                <ion-item>\r\n                                    <ion-label>Water Purifer Details</ion-label>\r\n                                </ion-item>\r\n                            </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                            <ng2-smart-table [settings]=\"waterPurifierDetailssettingsReadOnly\" \r\n                            [source]=\"waterPurifierDetailsdata\"></ng2-smart-table>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>Source of water</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>{{healthSanitation.waterSource}}</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>Water Storage System</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>{{healthSanitation.waterStorage}}</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                      </ion-row>\r\n                      <!-- <ion-row>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>Is water tested regularly?</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>{{healthSanitation.waterTested}}</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                      </ion-row> -->\r\n                      <ion-row>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>Is there a staff to clean toilet?</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>{{healthSanitation.staffToCleanToilet}}</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>Is there dustbins available for each class?</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>{{healthSanitation.dustbins}}</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>Waste Disposal system</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>{{healthSanitation.disposable}}</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                      </ion-row>\r\n                  </ion-grid>\r\n              </ion-card-body>\r\n          </ion-card>\r\n      </div>\r\n    <div *ngIf=\"hasStarted && isEditing\">\r\n    <ion-item class=\"yesorNo\">\r\n      <ion-grid class=\"radioGroup\">\r\n        <ion-row>\r\n          <ion-col>\r\n            <ion-radio-group  [(ngModel)]=\"healthSanitation.firstAidBox\" name = \"radio_1\">\r\n            <ion-row>\r\n               <ion-label class=\"schoolLocatedLabel\">Is there a first aid box in the school? </ion-label>\r\n              <ion-col class=\"radioGroupCol\">\r\n                   <ion-label>Yes</ion-label>\r\n              </ion-col>\r\n              <ion-col class=\"radioGroupCol\">   \t\r\n                  <ion-radio value=\"Yes\"  ></ion-radio>\r\n              </ion-col>\r\n              <ion-col class=\"radioGroupCol\">\r\n                  <ion-label>No</ion-label>\r\n              </ion-col>  \r\n              <ion-col class=\"radioGroupCol\">\t\r\n                    <ion-radio value=\"No\" ></ion-radio>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-radio-group>  \r\n          </ion-col>\r\n        </ion-row>\r\n    </ion-grid>\t  \r\n  \r\n  </ion-item>\r\n  <ion-item class=\"yesorNo\">\r\n  \r\n    <ion-grid class=\"radioGroup\">\r\n      <ion-row>\r\n        <ion-col>\r\n          <ion-radio-group  [(ngModel)]=\"healthSanitation.medicine\" name = \"radio_1\">\r\n          <ion-row>\r\n             <ion-label class=\"schoolLocatedLabel\">Is Medicine available? </ion-label>\r\n            <ion-col class=\"radioGroupCol\">\r\n                 <ion-label>Yes</ion-label>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">   \t\r\n                <ion-radio value=\"Yes\"  ></ion-radio>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">\r\n                <ion-label>No</ion-label>\r\n            </ion-col>  \r\n            <ion-col class=\"radioGroupCol\">\t\r\n                  <ion-radio value=\"No\" ></ion-radio>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-radio-group>  \r\n        </ion-col>\r\n      </ion-row>\r\n  </ion-grid>\t  \r\n</ion-item>\r\n<ion-item>\r\n  <ion-grid class=\"radioGroup\">\r\n      <ion-row>\r\n          <ion-col>\r\n            <ion-row>\r\n               <ion-label class=\"schoolLocatedLabel\"> Are Health Camps Organised?</ion-label>\r\n            </ion-row>\r\n          </ion-col>\r\n        </ion-row>\r\n      <ion-row >\r\n        <ion-col>\r\n          <ion-radio-group  [(ngModel)]=\"healthSanitation.healthCamp.dental\" name = \"radio_1\" class = \"yesorNo\">\r\n          <ion-row>\r\n             <ion-label class=\"schoolLocatedLabel\">1. Dental?</ion-label>\r\n            <ion-col class=\"radioGroupCol\">\r\n                 <ion-label>Yes</ion-label>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">   \t\r\n                <ion-radio value=\"Yes\"  ></ion-radio>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">\r\n                <ion-label>No</ion-label>\r\n            </ion-col>  \r\n            <ion-col class=\"radioGroupCol\">\t\r\n                  <ion-radio value=\"No\" ></ion-radio>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-radio-group>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row>\r\n          <ion-col>\r\n            <ion-radio-group  [(ngModel)]=\"healthSanitation.healthCamp.eye\" name = \"radio_1\" class = \"yesorNo\">\r\n            <ion-row>\r\n               <ion-label class=\"schoolLocatedLabel\">2. Eye?</ion-label>\r\n              <ion-col class=\"radioGroupCol\">\r\n                   <ion-label>Yes</ion-label>\r\n              </ion-col>\r\n              <ion-col class=\"radioGroupCol\">   \t\r\n                  <ion-radio value=\"Yes\"  ></ion-radio>\r\n              </ion-col>\r\n              <ion-col class=\"radioGroupCol\">\r\n                  <ion-label>No</ion-label>\r\n              </ion-col>  \r\n              <ion-col class=\"radioGroupCol\">\t\r\n                    <ion-radio value=\"No\" ></ion-radio>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-radio-group>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n            <ion-col>\r\n              <ion-radio-group  [(ngModel)]=\"healthSanitation.healthCamp.general\" name = \"radio_1\" class = \"yesorNo\">\r\n              <ion-row>\r\n                 <ion-label class=\"schoolLocatedLabel\">3. General?</ion-label>\r\n                <ion-col class=\"radioGroupCol\">\r\n                     <ion-label>Yes</ion-label>\r\n                </ion-col>\r\n                <ion-col class=\"radioGroupCol\">   \t\r\n                    <ion-radio value=\"Yes\"  ></ion-radio>\r\n                </ion-col>\r\n                <ion-col class=\"radioGroupCol\">\r\n                    <ion-label>No</ion-label>\r\n                </ion-col>  \r\n                <ion-col class=\"radioGroupCol\">\t\r\n                      <ion-radio value=\"No\" ></ion-radio>\r\n                </ion-col>\r\n              </ion-row>\r\n            </ion-radio-group>\r\n            </ion-col>\r\n          </ion-row>\r\n  </ion-grid>\t\r\n</ion-item>\r\n<ion-item>\r\n    <ion-grid class=\"radioGroup\">\r\n        <ion-row>\r\n            <ion-col>\r\n              <ion-row>\r\n                 <ion-label class=\"schoolLocatedLabel\"> Is Wash Program implemented?</ion-label>\r\n              </ion-row>\r\n            </ion-col>\r\n          </ion-row>\r\n        <ion-row >\r\n          <ion-col>\r\n            <ion-radio-group  [(ngModel)]=\"healthSanitation.washProgram.handWash\" name = \"radio_1\" class = \"yesorNo\">\r\n            <ion-row>\r\n               <ion-label class=\"schoolLocatedLabel\">1. Hand wash Program?</ion-label>\r\n              <ion-col class=\"radioGroupCol\">\r\n                   <ion-label>Yes</ion-label>\r\n              </ion-col>\r\n              <ion-col class=\"radioGroupCol\">   \t\r\n                  <ion-radio value=\"Yes\"  ></ion-radio>\r\n              </ion-col>\r\n              <ion-col class=\"radioGroupCol\">\r\n                  <ion-label>No</ion-label>\r\n              </ion-col>  \r\n              <ion-col class=\"radioGroupCol\">\t\r\n                    <ion-radio value=\"No\" ></ion-radio>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-radio-group>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n            <ion-col>\r\n              <ion-radio-group  [(ngModel)]=\"healthSanitation.washProgram.toiletUsage\" name = \"radio_1\" class = \"yesorNo\">\r\n              <ion-row>\r\n                 <ion-label class=\"schoolLocatedLabel\">2. Toilet usage awareness?</ion-label>\r\n                <ion-col class=\"radioGroupCol\">\r\n                     <ion-label>Yes</ion-label>\r\n                </ion-col>\r\n                <ion-col class=\"radioGroupCol\">   \t\r\n                    <ion-radio value=\"Yes\"  ></ion-radio>\r\n                </ion-col>\r\n                <ion-col class=\"radioGroupCol\">\r\n                    <ion-label>No</ion-label>\r\n                </ion-col>  \r\n                <ion-col class=\"radioGroupCol\">\t\r\n                      <ion-radio value=\"No\" ></ion-radio>\r\n                </ion-col>\r\n              </ion-row>\r\n            </ion-radio-group>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-row>\r\n              <ion-col>\r\n                <ion-radio-group  [(ngModel)]=\"healthSanitation.washProgram.drinkingWater\" name = \"radio_1\" class = \"yesorNo\">\r\n                <ion-row>\r\n                   <ion-label class=\"schoolLocatedLabel\">3. Awareness on safe drinking water?</ion-label>\r\n                  <ion-col class=\"radioGroupCol\">\r\n                       <ion-label>Yes</ion-label>\r\n                  </ion-col>\r\n                  <ion-col class=\"radioGroupCol\">   \t\r\n                      <ion-radio value=\"Yes\"  ></ion-radio>\r\n                  </ion-col>\r\n                  <ion-col class=\"radioGroupCol\">\r\n                      <ion-label>No</ion-label>\r\n                  </ion-col>  \r\n                  <ion-col class=\"radioGroupCol\">\t\r\n                        <ion-radio value=\"No\" ></ion-radio>\r\n                  </ion-col>\r\n                </ion-row>\r\n              </ion-radio-group>\r\n              </ion-col>\r\n            </ion-row>\r\n    </ion-grid>\t\r\n  </ion-item>\r\n  <ion-item class=\"yesorNo\">\r\n  \r\n      <ion-grid class=\"radioGroup\">\r\n        <ion-row>\r\n          <ion-col>\r\n            <ion-radio-group  [(ngModel)]=\"healthSanitation.handWashArea\" name = \"radio_1\">\r\n            <ion-row>\r\n               <ion-label class=\"schoolLocatedLabel\">Is there a Handwash area? </ion-label>\r\n              <ion-col class=\"radioGroupCol\">\r\n                   <ion-label>Yes</ion-label>\r\n              </ion-col>\r\n              <ion-col class=\"radioGroupCol\">   \t\r\n                  <ion-radio value=\"Yes\" (ionSelect) = \"ShowNextOption('rennovation')\"  ></ion-radio>\r\n              </ion-col>\r\n              <ion-col class=\"radioGroupCol\">\r\n                  <ion-label>No</ion-label>\r\n              </ion-col>  \r\n              <ion-col class=\"radioGroupCol\">\t\r\n                    <ion-radio value=\"No\" (ionSelect) = \"HideNextOption('rennovation')\" ></ion-radio>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-radio-group>  \r\n          </ion-col>\r\n        </ion-row>\r\n    </ion-grid>\t  \r\n  </ion-item>\r\n  <ion-item class=\"yesorNo\" id=\"rennovation\" class=\"visibility-hidden\">\r\n   \r\n      <ion-grid class=\"radioGroup\">\r\n        <ion-row>\r\n          <ion-col>\r\n            <ion-radio-group  [(ngModel)]=\"healthSanitation.renovation\" name = \"radio_1\">\r\n            <ion-row>\r\n               <ion-label class=\"schoolLocatedLabel\">Is rennovation required? </ion-label>\r\n              <ion-col class=\"radioGroupCol\">\r\n                   <ion-label>Yes</ion-label>\r\n              </ion-col>\r\n              <ion-col class=\"radioGroupCol\">   \t\r\n                  <ion-radio value=\"Yes\"  ></ion-radio>\r\n              </ion-col>\r\n              <ion-col class=\"radioGroupCol\">\r\n                  <ion-label>No</ion-label>\r\n              </ion-col>  \r\n              <ion-col class=\"radioGroupCol\">\t\r\n                    <ion-radio value=\"No\" ></ion-radio>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-radio-group>  \r\n          </ion-col>\r\n        </ion-row>\r\n    </ion-grid>\t  \r\n  </ion-item>\r\n  <ion-item class=\"yesorNo\">\r\n  \r\n      <ion-grid class=\"radioGroup\">\r\n        <ion-row>\r\n          <ion-col>\r\n            <ion-radio-group  [(ngModel)]=\"healthSanitation.utensilWashing\" name = \"radio_1\">\r\n            <ion-row>\r\n               <ion-label class=\"schoolLocatedLabel\">Is there a seperate utensil washing area? </ion-label>\r\n              <ion-col class=\"radioGroupCol\">\r\n                   <ion-label>Yes</ion-label>\r\n              </ion-col>\r\n              <ion-col class=\"radioGroupCol\">   \t\r\n                  <ion-radio value=\"Yes\"  ></ion-radio>\r\n              </ion-col>\r\n              <ion-col class=\"radioGroupCol\">\r\n                  <ion-label>No</ion-label>\r\n              </ion-col>  \r\n              <ion-col class=\"radioGroupCol\">\t\r\n                    <ion-radio value=\"No\" ></ion-radio>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-radio-group>  \r\n          </ion-col>\r\n        </ion-row>\r\n    </ion-grid>\t  \r\n  </ion-item>\r\n  <ion-item>\r\n    <ion-grid class=\"radioGroup\">\r\n      <ion-row>\r\n          <ion-col>\r\n            <ion-row>\r\n                <ion-label class=\"schoolLocatedLabel\"> Toilet Details</ion-label>\r\n            </ion-row>\r\n          </ion-col>\r\n        </ion-row>\r\n    <ng2-smart-table [settings]=\"toiletDetailssettings\" [source]=\"toiletDetailsdata\" ></ng2-smart-table>\r\n  </ion-grid>\r\n  </ion-item>\r\n  <ion-item class=\"yesorNo\">\r\n  \r\n      <ion-grid class=\"radioGroup\">\r\n        <ion-row>\r\n          <ion-col>\r\n            <ion-radio-group  [(ngModel)]=\"healthSanitation.waterPurifier.has\" name = \"radio_1\">\r\n            <ion-row>\r\n               <ion-label class=\"schoolLocatedLabel\">Is water purifier available? </ion-label>\r\n              <ion-col class=\"radioGroupCol\">\r\n                   <ion-label>Yes</ion-label>\r\n              </ion-col>\r\n              <ion-col class=\"radioGroupCol\">   \t\r\n                  <ion-radio value=\"Yes\" (ionSelect) = \"ShowNextOption('waterPurifierDetails')\"  ></ion-radio>\r\n              </ion-col>\r\n              <ion-col class=\"radioGroupCol\">\r\n                  <ion-label>No</ion-label>\r\n              </ion-col>  \r\n              <ion-col class=\"radioGroupCol\">\t\r\n                    <ion-radio value=\"No\" (ionSelect) = \"HideNextOption('waterPurifierDetails')\" ></ion-radio>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-radio-group>  \r\n          </ion-col>\r\n        </ion-row>\r\n    </ion-grid>\t  \r\n  </ion-item>\r\n  <ion-item id=\"waterPurifierDetails\" class=\"visibility-hidden\">\r\n      <ion-grid class=\"radioGroup\">\r\n        <ion-row>\r\n            <ion-col>\r\n              <ion-row>\r\n                  <ion-label class=\"schoolLocatedLabel\">Water Purifier Details</ion-label>\r\n              </ion-row>\r\n            </ion-col>\r\n          </ion-row>\r\n      <ng2-smart-table [settings]=\"waterPurifierDetailssettings\" [source]=\"waterPurifierDetailsdata\"></ng2-smart-table>\r\n    </ion-grid>\r\n    </ion-item>\r\n  <ion-item>\r\n\t\t<ion-label>Source of water</ion-label>\r\n\t\t<ion-select multiple=\"true\" cancelText=\"Dismiss\" okText=\"Okay\" [(ngModel)]=\"healthSanitation.waterSource\" name = \"medium\">\r\n\t\t\t<ion-select-option value=\"Bore well\">Borewell</ion-select-option>\r\n\t\t\t<ion-select-option value=\"WaterConnection\">Corporation/GP/Water Connection</ion-select-option>\r\n\t\t\t<ion-select-option value=\"Water Tanker\">Water Tanker</ion-select-option>\r\n\t\t\t<ion-select-option value=\"OpenWell\">Open Well</ion-select-option>\r\n\t\t</ion-select>\r\n  </ion-item>\r\n  <ion-item>\r\n\t\t<ion-label>Water Storage System</ion-label>\r\n\t\t<ion-select multiple=\"true\" cancelText=\"Dismiss\" okText=\"Okay\" [(ngModel)]=\"healthSanitation.waterStorage\" name = \"medium\">\r\n\t\t\t<ion-select-option value=\"Sump\">Sump</ion-select-option>\r\n\t\t\t<ion-select-option value=\"Overhead Tank\">Overhead tank</ion-select-option>\r\n\t\t</ion-select>\r\n  </ion-item>\r\n  <ion-item class=\"yesorNo\">\r\n  \r\n    <!-- <ion-grid class=\"radioGroup\">\r\n      <ion-row>\r\n        <ion-col>\r\n          <ion-radio-group  [(ngModel)]=\"healthSanitation.waterTested\" name = \"radio_1\">\r\n          <ion-row>\r\n             <ion-label class=\"schoolLocatedLabel\">Is water tested regularly? </ion-label>\r\n            <ion-col class=\"radioGroupCol\">\r\n                 <ion-label>Yes</ion-label>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">   \t\r\n                <ion-radio value=\"Yes\"  ></ion-radio>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">\r\n                <ion-label>No</ion-label>\r\n            </ion-col>  \r\n            <ion-col class=\"radioGroupCol\">\t\r\n                  <ion-radio value=\"No\" ></ion-radio>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-radio-group>  \r\n        </ion-col>\r\n      </ion-row>\r\n  </ion-grid>\t   -->\r\n</ion-item>\r\n<ion-item class=\"yesorNo\">\r\n  \r\n  <ion-grid class=\"radioGroup\">\r\n    <ion-row>\r\n      <ion-col>\r\n        <ion-radio-group  [(ngModel)]=\"healthSanitation.staffToCleanToilet\" name = \"radio_1\">\r\n        <ion-row>\r\n           <ion-label class=\"schoolLocatedLabel\">Is there a staff to clean toilet? </ion-label>\r\n          <ion-col class=\"radioGroupCol\">\r\n               <ion-label>Yes</ion-label>\r\n          </ion-col>\r\n          <ion-col class=\"radioGroupCol\">   \t\r\n              <ion-radio value=\"Yes\"  ></ion-radio>\r\n          </ion-col>\r\n          <ion-col class=\"radioGroupCol\">\r\n              <ion-label>No</ion-label>\r\n          </ion-col>  \r\n          <ion-col class=\"radioGroupCol\">\t\r\n                <ion-radio value=\"No\" ></ion-radio>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-radio-group>  \r\n      </ion-col>\r\n    </ion-row>\r\n</ion-grid>\t  \r\n</ion-item>\r\n<ion-item class=\"yesorNo\">\r\n  \r\n  <ion-grid class=\"radioGroup\">\r\n    <ion-row>\r\n      <ion-col>\r\n        <ion-radio-group  [(ngModel)]=\"healthSanitation.dustbins\" name = \"radio_1\">\r\n        <ion-row>\r\n           <ion-label class=\"schoolLocatedLabel\">Is there dustbins available for each class?  </ion-label>\r\n          <ion-col class=\"radioGroupCol\">\r\n               <ion-label>Yes</ion-label>\r\n          </ion-col>\r\n          <ion-col class=\"radioGroupCol\">   \t\r\n              <ion-radio value=\"Yes\"  ></ion-radio>\r\n          </ion-col>\r\n          <ion-col class=\"radioGroupCol\">\r\n              <ion-label>No</ion-label>\r\n          </ion-col>  \r\n          <ion-col class=\"radioGroupCol\">\t\r\n                <ion-radio value=\"No\" ></ion-radio>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-radio-group>  \r\n      </ion-col>\r\n    </ion-row>\r\n</ion-grid>\t  \r\n</ion-item>\r\n<ion-item>\r\n  <ion-label>Waste Disposal system</ion-label>\r\n  <ion-select multiple=\"true\" cancelText=\"Dismiss\" okText=\"Okay\" [(ngModel)]=\"healthSanitation.disposable\" name = \"medium\">\r\n    <ion-select-option value=\"Through corporation/GP\">Through Corporation/GP</ion-select-option>\r\n    <ion-select-option value=\"Having own waste management system\">Having own waste management system</ion-select-option>\r\n  </ion-select>\r\n</ion-item>\r\n</div>\r\n<ion-button color=\"primary\" (click)=\"isEditing = !isEditing\" *ngIf=\"hasStarted && !isEditing\">\r\n    <ion-icon name=\"create\" padding></ion-icon>\r\n    Edit</ion-button>\r\n<ion-button color=\"secondary\" (click)=\"saveHealthSanitationDetailsAndContinue()\" *ngIf=\"isEditing\" (click)=\"isEditing = !isEditing\"> \r\n    <ion-icon name=\"save\" padding></ion-icon>\r\nSave & Continue</ion-button>\r\n<ion-button color=\"tertiary\" (click)=\"saveHealthSanitationDetails()\" *ngIf=\"isEditing\" (click)=\"isEditing = !isEditing\"> \r\n    <ion-icon name=\"clipboard\" padding></ion-icon>\r\nSave</ion-button>\r\n  "

/***/ }),

/***/ "./src/app/health-sanitation/health-sanitation.page.scss":
/*!***************************************************************!*\
  !*** ./src/app/health-sanitation/health-sanitation.page.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".textField {\n  --border-radius: 5px;\n  /*--box-shadow: 0px 14px 25px rgba(182, 30, 30, 0.59);*/ }\n\n.textField ion-label {\n  width: 250px; }\n\n.yesorNo ion-label {\n  width: 280px; }\n\n.yesorNoMultiple ion-label {\n  width: 100px; }\n\n.dateLabel {\n  max-width: 200px; }\n\n.radioGroup {\n  width: 100%; }\n\nion-radio {\n  margin-top: 0px; }\n\n.schoolLocatedLabel {\n  margin-top: 25px;\n  margin-left: -10px;\n  margin-right: 20px; }\n\n/*.addressItem{\r\n\theight: 150px;\r\n\twidth: 100%;\r\n}*/\n\n/*ion-textarea{\r\n\theight: 116px;\r\n\twidth:100%;\r\n    background: #c1c4cd;\r\n}\r\n\r\n.item-input.sc-ion-label-md-h, .item-input .sc-ion-label-md-h {\r\n    -ms-flex: initial;\r\n    flex: initial;\r\n    width: 50% !important;\r\n    pointer-events: none;\r\n}*/\n\n/*ion-label{\r\n    padding: 12px;\r\n}\r\n\r\nion-radio{\r\n    margin-left: 20px;\r\n}*/\n\n.headerTextColor {\n  text-color: #DEE2DD; }\n\n.toolbarStyle {\n  background: #3B5999; }\n\n.radioGroupCol {\n  max-width: 60px !important;\n  max-height: 30px !important;\n  margin-top: 18px; }\n\n.visibility-hidden {\n  display: none; }\n\n.label-stacked.sc-ion-label-md-h {\n  font-size: 16px; }\n\n.radioLabel {\n  margin-right: 10px; }\n\n.schoolTypeCol {\n  max-width: 195px !important; }\n\n.saveButton {\n  float: right;\n  width: 100px;\n  height: 30px;\n  font-size: 14px !important;\n  font-weight: 700; }\n\nion-textarea {\n  padding-top: 10px; }\n\n.schoolLocatedLabel {\n  margin-top: 25px;\n  margin-left: -10px;\n  margin-right: 20px; }\n\n:host /deep/ ng2-smart-table table {\n  border-style: inset;\n  border-color: initial;\n  -webkit-border-image: initial;\n       -o-border-image: initial;\n          border-image: initial;\n  border-width: 2px;\n  display: table;\n  border-spacing: 2px;\n  border-color: grey; }\n\n:host /deep/ ng2-smart-table table > tbody > tr > td {\n  box-sizing: border-box;\n  border: 1px solid grey; }\n\n:host /deep/ ng2-smart-table thead > tr > th {\n  border: 1px solid grey; }\n\n:host /deep/ ng2-smart-table {\n  font-size: 16px; }\n\n:host /deep/ ng2-smart-table .ng2-smart-filters {\n  display: none; }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaGVhbHRoLXNhbml0YXRpb24vQzpcXHZhcnNoYVxcY3dcXGNhcmV3b3Jrc3VpXFxDYXJld29ya3Mvc3JjXFxhcHBcXGhlYWx0aC1zYW5pdGF0aW9uXFxoZWFsdGgtc2FuaXRhdGlvbi5wYWdlLnNjc3MiLCJzcmMvYXBwL2hlYWx0aC1zYW5pdGF0aW9uL2hlYWx0aC1zYW5pdGF0aW9uLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG9CQUFnQjtFQUNoQix1REFBQSxFQUF3RDs7QUFFNUQ7RUFDSSxZQUFZLEVBQUE7O0FBRWhCO0VBQ0ksWUFBWSxFQUFBOztBQUVoQjtFQUNJLFlBQVksRUFBQTs7QUFFaEI7RUFDRyxnQkFBZ0IsRUFBQTs7QUFFbkI7RUFDQyxXQUFXLEVBQUE7O0FBR1o7RUFDSSxlQUFlLEVBQUE7O0FBR25CO0VBQ0ksZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixrQkFBa0IsRUFBQTs7QUFHdEI7OztFQ0FFOztBREtGOzs7Ozs7Ozs7OztFQ09FOztBRE1GOzs7Ozs7RUNDRTs7QURPRDtFQUNHLG1CQUFtQixFQUFBOztBQUd2QjtFQUNJLG1CQUFtQixFQUFBOztBQUd2QjtFQUNJLDBCQUEwQjtFQUMxQiwyQkFBMkI7RUFDM0IsZ0JBQWdCLEVBQUE7O0FBRXBCO0VBRUksYUFBYSxFQUFBOztBQUVqQjtFQUNHLGVBQWUsRUFBQTs7QUFHbEI7RUFDSSxrQkFBa0IsRUFBQTs7QUFHdEI7RUFDSSwyQkFBMkIsRUFBQTs7QUFHL0I7RUFFSSxZQUFZO0VBQ1osWUFBWTtFQUNaLFlBQVk7RUFDWiwwQkFBMEI7RUFDMUIsZ0JBQWdCLEVBQUE7O0FBR3BCO0VBQ0ksaUJBQWlCLEVBQUE7O0FBRXJCO0VBQ0ksZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixrQkFBa0IsRUFBQTs7QUFFdEI7RUFDSSxtQkFBbUI7RUFDbkIscUJBQXFCO0VBQ3JCLDZCQUFxQjtPQUFyQix3QkFBcUI7VUFBckIscUJBQXFCO0VBQ3JCLGlCQUFpQjtFQUVqQixjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLGtCQUFrQixFQUFBOztBQUd0QjtFQUNJLHNCQUFzQjtFQUN0QixzQkFBc0IsRUFBQTs7QUFHMUI7RUFDSSxzQkFBc0IsRUFBQTs7QUFFMUI7RUFDSSxlQUFjLEVBQUE7O0FBRWxCO0VBRUksYUFBYSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvaGVhbHRoLXNhbml0YXRpb24vaGVhbHRoLXNhbml0YXRpb24ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRleHRGaWVsZHtcclxuICAgIC0tYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgLyotLWJveC1zaGFkb3c6IDBweCAxNHB4IDI1cHggcmdiYSgxODIsIDMwLCAzMCwgMC41OSk7Ki9cclxufVxyXG4udGV4dEZpZWxkIGlvbi1sYWJlbHtcclxuICAgIHdpZHRoOiAyNTBweDtcclxufVxyXG4ueWVzb3JObyBpb24tbGFiZWx7XHJcbiAgICB3aWR0aDogMjgwcHg7XHJcbn1cclxuLnllc29yTm9NdWx0aXBsZSBpb24tbGFiZWx7XHJcbiAgICB3aWR0aDogMTAwcHg7XHJcbn1cclxuLmRhdGVMYWJlbHtcclxuICAgbWF4LXdpZHRoOiAyMDBweDsgXHJcbn1cclxuLnJhZGlvR3JvdXB7XHJcblx0d2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbmlvbi1yYWRpb3tcclxuICAgIG1hcmdpbi10b3A6IDBweDtcclxufVxyXG5cclxuLnNjaG9vbExvY2F0ZWRMYWJlbHtcclxuICAgIG1hcmdpbi10b3A6IDI1cHg7XHJcbiAgICBtYXJnaW4tbGVmdDogLTEwcHg7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDIwcHg7XHJcbn1cclxuXHJcbi8qLmFkZHJlc3NJdGVte1xyXG5cdGhlaWdodDogMTUwcHg7XHJcblx0d2lkdGg6IDEwMCU7XHJcbn0qL1xyXG5cclxuLyppb24tdGV4dGFyZWF7XHJcblx0aGVpZ2h0OiAxMTZweDtcclxuXHR3aWR0aDoxMDAlO1xyXG4gICAgYmFja2dyb3VuZDogI2MxYzRjZDtcclxufVxyXG5cclxuLml0ZW0taW5wdXQuc2MtaW9uLWxhYmVsLW1kLWgsIC5pdGVtLWlucHV0IC5zYy1pb24tbGFiZWwtbWQtaCB7XHJcbiAgICAtbXMtZmxleDogaW5pdGlhbDtcclxuICAgIGZsZXg6IGluaXRpYWw7XHJcbiAgICB3aWR0aDogNTAlICFpbXBvcnRhbnQ7XHJcbiAgICBwb2ludGVyLWV2ZW50czogbm9uZTtcclxufSovXHJcblxyXG4vKmlvbi1sYWJlbHtcclxuICAgIHBhZGRpbmc6IDEycHg7XHJcbn1cclxuXHJcbmlvbi1yYWRpb3tcclxuICAgIG1hcmdpbi1sZWZ0OiAyMHB4O1xyXG59Ki9cclxuXHJcbiAuaGVhZGVyVGV4dENvbG9ye1xyXG4gICAgdGV4dC1jb2xvcjogI0RFRTJERDtcclxuIH1cclxuXHJcbi50b29sYmFyU3R5bGV7XHJcbiAgICBiYWNrZ3JvdW5kOiAjM0I1OTk5O1xyXG59XHJcblxyXG4ucmFkaW9Hcm91cENvbHtcclxuICAgIG1heC13aWR0aDogNjBweCAhaW1wb3J0YW50O1xyXG4gICAgbWF4LWhlaWdodDogMzBweCAhaW1wb3J0YW50O1xyXG4gICAgbWFyZ2luLXRvcDogMThweDtcclxufVxyXG4udmlzaWJpbGl0eS1oaWRkZW5cclxue1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxufVxyXG4ubGFiZWwtc3RhY2tlZC5zYy1pb24tbGFiZWwtbWQtaCB7XHJcbiAgIGZvbnQtc2l6ZTogMTZweDtcclxufVxyXG5cclxuLnJhZGlvTGFiZWx7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbn1cclxuXHJcbi5zY2hvb2xUeXBlQ29se1xyXG4gICAgbWF4LXdpZHRoOiAxOTVweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uc2F2ZUJ1dHRvbntcclxuICAgIFxyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgd2lkdGg6IDEwMHB4O1xyXG4gICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgZm9udC1zaXplOiAxNHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBmb250LXdlaWdodDogNzAwO1xyXG59XHJcblxyXG5pb24tdGV4dGFyZWF7XHJcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcclxufVxyXG4uc2Nob29sTG9jYXRlZExhYmVse1xyXG4gICAgbWFyZ2luLXRvcDogMjVweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAtMTBweDtcclxuICAgIG1hcmdpbi1yaWdodDogMjBweDtcclxufVxyXG46aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHRhYmxle1xyXG4gICAgYm9yZGVyLXN0eWxlOiBpbnNldDtcclxuICAgIGJvcmRlci1jb2xvcjogaW5pdGlhbDtcclxuICAgIGJvcmRlci1pbWFnZTogaW5pdGlhbDtcclxuICAgIGJvcmRlci13aWR0aDogMnB4O1xyXG4gICAgXHJcbiAgICBkaXNwbGF5OiB0YWJsZTtcclxuICAgIGJvcmRlci1zcGFjaW5nOiAycHg7XHJcbiAgICBib3JkZXItY29sb3I6IGdyZXk7IFxyXG5cclxufVxyXG46aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHRhYmxlID4gdGJvZHkgPiB0ciA+IHRkIHtcclxuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCBncmV5O1xyXG59XHJcblxyXG46aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHRoZWFkID4gdHIgPiB0aCAgeyBcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIGdyZXk7XHJcbn0gIFxyXG46aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHtcclxuICAgIGZvbnQtc2l6ZToxNnB4O1xyXG59XHJcbjpob3N0IC9kZWVwLyBuZzItc21hcnQtdGFibGUgLm5nMi1zbWFydC1maWx0ZXJzXHJcbntcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbn1cclxuIiwiLnRleHRGaWVsZCB7XG4gIC0tYm9yZGVyLXJhZGl1czogNXB4O1xuICAvKi0tYm94LXNoYWRvdzogMHB4IDE0cHggMjVweCByZ2JhKDE4MiwgMzAsIDMwLCAwLjU5KTsqLyB9XG5cbi50ZXh0RmllbGQgaW9uLWxhYmVsIHtcbiAgd2lkdGg6IDI1MHB4OyB9XG5cbi55ZXNvck5vIGlvbi1sYWJlbCB7XG4gIHdpZHRoOiAyODBweDsgfVxuXG4ueWVzb3JOb011bHRpcGxlIGlvbi1sYWJlbCB7XG4gIHdpZHRoOiAxMDBweDsgfVxuXG4uZGF0ZUxhYmVsIHtcbiAgbWF4LXdpZHRoOiAyMDBweDsgfVxuXG4ucmFkaW9Hcm91cCB7XG4gIHdpZHRoOiAxMDAlOyB9XG5cbmlvbi1yYWRpbyB7XG4gIG1hcmdpbi10b3A6IDBweDsgfVxuXG4uc2Nob29sTG9jYXRlZExhYmVsIHtcbiAgbWFyZ2luLXRvcDogMjVweDtcbiAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xuICBtYXJnaW4tcmlnaHQ6IDIwcHg7IH1cblxuLyouYWRkcmVzc0l0ZW17XHJcblx0aGVpZ2h0OiAxNTBweDtcclxuXHR3aWR0aDogMTAwJTtcclxufSovXG4vKmlvbi10ZXh0YXJlYXtcclxuXHRoZWlnaHQ6IDExNnB4O1xyXG5cdHdpZHRoOjEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kOiAjYzFjNGNkO1xyXG59XHJcblxyXG4uaXRlbS1pbnB1dC5zYy1pb24tbGFiZWwtbWQtaCwgLml0ZW0taW5wdXQgLnNjLWlvbi1sYWJlbC1tZC1oIHtcclxuICAgIC1tcy1mbGV4OiBpbml0aWFsO1xyXG4gICAgZmxleDogaW5pdGlhbDtcclxuICAgIHdpZHRoOiA1MCUgIWltcG9ydGFudDtcclxuICAgIHBvaW50ZXItZXZlbnRzOiBub25lO1xyXG59Ki9cbi8qaW9uLWxhYmVse1xyXG4gICAgcGFkZGluZzogMTJweDtcclxufVxyXG5cclxuaW9uLXJhZGlve1xyXG4gICAgbWFyZ2luLWxlZnQ6IDIwcHg7XHJcbn0qL1xuLmhlYWRlclRleHRDb2xvciB7XG4gIHRleHQtY29sb3I6ICNERUUyREQ7IH1cblxuLnRvb2xiYXJTdHlsZSB7XG4gIGJhY2tncm91bmQ6ICMzQjU5OTk7IH1cblxuLnJhZGlvR3JvdXBDb2wge1xuICBtYXgtd2lkdGg6IDYwcHggIWltcG9ydGFudDtcbiAgbWF4LWhlaWdodDogMzBweCAhaW1wb3J0YW50O1xuICBtYXJnaW4tdG9wOiAxOHB4OyB9XG5cbi52aXNpYmlsaXR5LWhpZGRlbiB7XG4gIGRpc3BsYXk6IG5vbmU7IH1cblxuLmxhYmVsLXN0YWNrZWQuc2MtaW9uLWxhYmVsLW1kLWgge1xuICBmb250LXNpemU6IDE2cHg7IH1cblxuLnJhZGlvTGFiZWwge1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7IH1cblxuLnNjaG9vbFR5cGVDb2wge1xuICBtYXgtd2lkdGg6IDE5NXB4ICFpbXBvcnRhbnQ7IH1cblxuLnNhdmVCdXR0b24ge1xuICBmbG9hdDogcmlnaHQ7XG4gIHdpZHRoOiAxMDBweDtcbiAgaGVpZ2h0OiAzMHB4O1xuICBmb250LXNpemU6IDE0cHggIWltcG9ydGFudDtcbiAgZm9udC13ZWlnaHQ6IDcwMDsgfVxuXG5pb24tdGV4dGFyZWEge1xuICBwYWRkaW5nLXRvcDogMTBweDsgfVxuXG4uc2Nob29sTG9jYXRlZExhYmVsIHtcbiAgbWFyZ2luLXRvcDogMjVweDtcbiAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xuICBtYXJnaW4tcmlnaHQ6IDIwcHg7IH1cblxuOmhvc3QgL2RlZXAvIG5nMi1zbWFydC10YWJsZSB0YWJsZSB7XG4gIGJvcmRlci1zdHlsZTogaW5zZXQ7XG4gIGJvcmRlci1jb2xvcjogaW5pdGlhbDtcbiAgYm9yZGVyLWltYWdlOiBpbml0aWFsO1xuICBib3JkZXItd2lkdGg6IDJweDtcbiAgZGlzcGxheTogdGFibGU7XG4gIGJvcmRlci1zcGFjaW5nOiAycHg7XG4gIGJvcmRlci1jb2xvcjogZ3JleTsgfVxuXG46aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHRhYmxlID4gdGJvZHkgPiB0ciA+IHRkIHtcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgYm9yZGVyOiAxcHggc29saWQgZ3JleTsgfVxuXG46aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHRoZWFkID4gdHIgPiB0aCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkIGdyZXk7IH1cblxuOmhvc3QgL2RlZXAvIG5nMi1zbWFydC10YWJsZSB7XG4gIGZvbnQtc2l6ZTogMTZweDsgfVxuXG46aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIC5uZzItc21hcnQtZmlsdGVycyB7XG4gIGRpc3BsYXk6IG5vbmU7IH1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/health-sanitation/health-sanitation.page.ts":
/*!*************************************************************!*\
  !*** ./src/app/health-sanitation/health-sanitation.page.ts ***!
  \*************************************************************/
/*! exports provided: HealthSanitationPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HealthSanitationPage", function() { return HealthSanitationPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_need_assessment_health_sanitation__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../models/need-assessment/health-sanitation */ "./src/app/models/need-assessment/health-sanitation.ts");
/* harmony import */ var _services_need_assessment_health_sanitation_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/need-assessment/health-sanitation.service */ "./src/app/services/need-assessment/health-sanitation.service.ts");
/* harmony import */ var _services_local_store_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/local-store-service.service */ "./src/app/services/local-store-service.service.ts");
/* harmony import */ var _health_sanitation_config__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./health-sanitation.config */ "./src/app/health-sanitation/health-sanitation.config.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var HealthSanitationPage = /** @class */ (function () {
    function HealthSanitationPage(healthSanitationService, router, localStoreService) {
        this.healthSanitationService = healthSanitationService;
        this.router = router;
        this.localStoreService = localStoreService;
        this.healthSanitation = new _models_need_assessment_health_sanitation__WEBPACK_IMPORTED_MODULE_1__["HealthSanitationModel"]();
        this.toiletDetailssettings = _health_sanitation_config__WEBPACK_IMPORTED_MODULE_4__["HealthSanitationConfig"].toiletDetailsConfig;
        this.toiletDetailssettingsReadOnly = _health_sanitation_config__WEBPACK_IMPORTED_MODULE_4__["HealthSanitationConfig"].toiletDetailsConfigReadOnly;
        this.waterPurifierDetailssettings = _health_sanitation_config__WEBPACK_IMPORTED_MODULE_4__["HealthSanitationConfig"].waterPurifierDetailsConfig;
        this.waterPurifierDetailssettingsReadOnly = _health_sanitation_config__WEBPACK_IMPORTED_MODULE_4__["HealthSanitationConfig"].waterPurifierDetailsConfigReadOnly;
    }
    HealthSanitationPage.prototype.ngOnInit = function () {
        var _this = this;
        /**
         * While fetching the status API,
         * if need assessment has started, set hasStarted to true
         * else, hasStarted to false.
         *
         * isEdited is already handled.
         */
        this.isEditing = false;
        this.hasStarted = false;
        var schoolId = "1"; // ideally schoolId should be fetched from the local storage
        var schoolName = this.localStoreService.schoolName;
        this.healthSanitationService.getHealthSanitationDetailsOfSchool(schoolId)
            .subscribe(function (result) {
            console.log("HealthSanitation", result);
            _this.initializeFormElements(result);
        }, function (err) {
            console.log(err);
        });
    };
    HealthSanitationPage.prototype.getEmptyHealthCampDetails = function () {
        var model = new _models_need_assessment_health_sanitation__WEBPACK_IMPORTED_MODULE_1__["healthCampModel"]();
        model.dental = "";
        model.eye = "";
        model.general = "";
        return model;
    };
    HealthSanitationPage.prototype.getEmptyWashProgramDetails = function () {
        var model = new _models_need_assessment_health_sanitation__WEBPACK_IMPORTED_MODULE_1__["washProgramModel"]();
        model.drinkingWater = "";
        model.handWash = "";
        model.toiletUsage = "";
        return model;
    };
    HealthSanitationPage.prototype.getEmptyToiletDetails = function () {
        var model = new _models_need_assessment_health_sanitation__WEBPACK_IMPORTED_MODULE_1__["toiletDetailModel"]();
        model.has = "Yes";
        model.details = _health_sanitation_config__WEBPACK_IMPORTED_MODULE_4__["HealthSanitationConfig"].toiletModel;
        return model;
    };
    HealthSanitationPage.prototype.getEmptyWaterPurifierDetails = function () {
        var model = new _models_need_assessment_health_sanitation__WEBPACK_IMPORTED_MODULE_1__["waterPurifierModel"]();
        model.has = "";
        model.details = _health_sanitation_config__WEBPACK_IMPORTED_MODULE_4__["HealthSanitationConfig"].waterPurifierModel;
        return model;
    };
    HealthSanitationPage.prototype.initializeFormElements = function (dataJSON) {
        this.healthSanitation = dataJSON;
        if (this.healthSanitation.firstAidBox == null || this.healthSanitation.firstAidBox == undefined) {
            this.healthSanitation.firstAidBox = "";
        }
        if (this.healthSanitation.medicine == null || this.healthSanitation.medicine == undefined) {
            this.healthSanitation.medicine = "";
        }
        if (this.healthSanitation.healthCamp == null || this.healthSanitation.healthCamp == undefined) {
            this.healthSanitation.healthCamp = this.getEmptyHealthCampDetails();
        }
        else {
            for (var index in this.healthSanitation.healthCamp) {
                if (this.healthSanitation.healthCamp[index] == null || this.healthSanitation.healthCamp[index] == undefined) {
                    this.healthSanitation.healthCamp[index] = "";
                }
            }
        }
        if (this.healthSanitation.washProgram == null || this.healthSanitation.washProgram == undefined) {
            this.healthSanitation.washProgram = this.getEmptyWashProgramDetails();
        }
        else {
            for (var index in this.healthSanitation.washProgram) {
                if (this.healthSanitation.washProgram[index] == null || this.healthSanitation.washProgram[index] == undefined) {
                    this.healthSanitation.washProgram[index] = "";
                }
            }
        }
        if (this.healthSanitation.handWashArea == null || this.healthSanitation.handWashArea == undefined) {
            this.healthSanitation.handWashArea = "";
        }
        if (this.healthSanitation.renovation == null || this.healthSanitation.renovation == undefined) {
            this.healthSanitation.renovation = "";
        }
        if (this.healthSanitation.utensilWashing == null || this.healthSanitation.utensilWashing == undefined) {
            this.healthSanitation.utensilWashing = "";
        }
        if (this.healthSanitation.toiletDetail == null || this.healthSanitation.toiletDetail == undefined || this.healthSanitation.toiletDetail.details == undefined || this.healthSanitation.toiletDetail.details.length == 0) {
            this.healthSanitation.toiletDetail = this.getEmptyToiletDetails();
        }
        if (this.healthSanitation.waterPurifier == null || this.healthSanitation.waterPurifier == undefined || this.healthSanitation.waterPurifier.details == undefined || this.healthSanitation.waterPurifier.details.length == 0) {
            this.healthSanitation.waterPurifier = this.getEmptyWaterPurifierDetails();
        }
        if (this.healthSanitation.waterSource == null || this.healthSanitation.waterSource == undefined) {
            this.healthSanitation.waterSource = [];
        }
        if (this.healthSanitation.waterStorage == null || this.healthSanitation.waterStorage == undefined || this.healthSanitation.toiletDetail.details == undefined || this.healthSanitation.toiletDetail.details.length == 0) {
            this.healthSanitation.waterStorage = [];
        }
        if (this.healthSanitation.staffToCleanToilet == null || this.healthSanitation.staffToCleanToilet == undefined) {
            this.healthSanitation.staffToCleanToilet = "";
        }
        if (this.healthSanitation.dustbins == null || this.healthSanitation.dustbins == undefined) {
            this.healthSanitation.dustbins = "";
        }
        if (this.healthSanitation.disposable == null || this.healthSanitation.disposable == undefined) {
            this.healthSanitation.disposable = [];
        }
        this.localStoreService.healthSanitation = this.healthSanitation;
        this.toiletDetailsdata = this.healthSanitation.toiletDetail.details;
        this.waterPurifierDetailsdata = this.healthSanitation.waterPurifier.details;
    };
    HealthSanitationPage.prototype.ShowNextOption = function (elementId) {
        var elem = document.getElementById(elementId);
        elem.classList.remove('visibility-hidden');
    };
    HealthSanitationPage.prototype.HideNextOption = function (elementId) {
        var elem = document.getElementById(elementId);
        if (!elem.classList.contains('visibility-hidden')) {
            elem.classList.add('visibility-hidden');
        }
    };
    HealthSanitationPage.prototype.saveHealthSanitationDetails = function () {
        this.hasStarted = true;
        this.localStoreService.healthSanitation = this.healthSanitation;
        this.healthSanitationService.saveHealthSanitationDetails(this.healthSanitation).subscribe(function (res) {
            console.log("healthSanitationSave, success");
        }, function (err) {
            console.log(err);
        });
    };
    HealthSanitationPage.prototype.saveHealthSanitationDetailsAndContinue = function () {
        var _this = this;
        this.hasStarted = true;
        this.localStoreService.healthSanitation = this.healthSanitation;
        this.healthSanitationService.saveHealthSanitationDetails(this.healthSanitation).subscribe(function (res) {
            _this.router.navigate(['../need-assessment/classroom-environment']);
            console.log("healthSanitationSave, success");
            _this.isEditing = false;
        }, function (err) {
            console.log(err);
            _this.isEditing = true;
        });
    };
    HealthSanitationPage.prototype.startEditing = function () {
        this.isEditing = true;
        this.hasStarted = true;
    };
    HealthSanitationPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-health-sanitation',
            template: __webpack_require__(/*! ./health-sanitation.page.html */ "./src/app/health-sanitation/health-sanitation.page.html"),
            styles: [__webpack_require__(/*! ./health-sanitation.page.scss */ "./src/app/health-sanitation/health-sanitation.page.scss")]
        }),
        __metadata("design:paramtypes", [_services_need_assessment_health_sanitation_service__WEBPACK_IMPORTED_MODULE_2__["HealthSanitationService"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _services_local_store_service_service__WEBPACK_IMPORTED_MODULE_3__["LocalStoreServiceService"]])
    ], HealthSanitationPage);
    return HealthSanitationPage;
}());



/***/ }),

/***/ "./src/app/models/need-assessment/health-sanitation.ts":
/*!*************************************************************!*\
  !*** ./src/app/models/need-assessment/health-sanitation.ts ***!
  \*************************************************************/
/*! exports provided: HealthSanitationModel, healthCampModel, washProgramModel, toiletDetailModel, toiletDetails, waterPurifierModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HealthSanitationModel", function() { return HealthSanitationModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "healthCampModel", function() { return healthCampModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "washProgramModel", function() { return washProgramModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "toiletDetailModel", function() { return toiletDetailModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "toiletDetails", function() { return toiletDetails; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "waterPurifierModel", function() { return waterPurifierModel; });
var HealthSanitationModel = /** @class */ (function () {
    function HealthSanitationModel() {
    }
    return HealthSanitationModel;
}());

var healthCampModel = /** @class */ (function () {
    function healthCampModel() {
    }
    return healthCampModel;
}());

var washProgramModel = /** @class */ (function () {
    function washProgramModel() {
    }
    return washProgramModel;
}());

var toiletDetailModel = /** @class */ (function () {
    function toiletDetailModel() {
    }
    return toiletDetailModel;
}());

var toiletDetails = /** @class */ (function () {
    function toiletDetails() {
    }
    return toiletDetails;
}());

var waterPurifierModel = /** @class */ (function () {
    function waterPurifierModel() {
    }
    return waterPurifierModel;
}());

var waterPurifierDetails = /** @class */ (function () {
    function waterPurifierDetails() {
    }
    return waterPurifierDetails;
}());


/***/ }),

/***/ "./src/app/services/local-store-service.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/services/local-store-service.service.ts ***!
  \*********************************************************/
/*! exports provided: LocalStoreServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocalStoreServiceService", function() { return LocalStoreServiceService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LocalStoreServiceService = /** @class */ (function () {
    function LocalStoreServiceService() {
        this._isSearchSchoolFlow = true;
    }
    Object.defineProperty(LocalStoreServiceService.prototype, "sampleJson", {
        get: function () {
            var tmp = this._jsonData;
            return tmp;
        },
        set: function (jdata) {
            this._jsonData = jdata;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "isSearchSchoolFlow", {
        get: function () {
            var tmp = this._isSearchSchoolFlow;
            return tmp;
        },
        set: function (searchSchoolFlow) {
            this._isSearchSchoolFlow = searchSchoolFlow;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "schoolName", {
        get: function () {
            var tmp = this._schoolName;
            return tmp;
        },
        set: function (schoolProfilePage) {
            this._schoolName = schoolProfilePage;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "schoolId", {
        get: function () {
            var id = this._schoolId;
            return id;
        },
        set: function (id) {
            this._schoolId = id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "schoolProfileBasic", {
        get: function () {
            var tmp = this._schoolProfileBasic;
            return tmp;
        },
        set: function (schoolProfile) {
            this._schoolProfileBasic = schoolProfile;
            console.log(this._schoolProfileBasic.name);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "eduDeptFunction", {
        get: function () {
            var tmp = this._eduDeptFunction;
            return tmp;
        },
        set: function (eduDept) {
            this._eduDeptFunction = eduDept;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "teacherDetails", {
        get: function () {
            var tmp = this._teacherDetails;
            return tmp;
        },
        set: function (teacherDetail) {
            this._teacherDetails = teacherDetail;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "stakeholderInfo", {
        get: function () {
            var tmp = this._stakeholderInfo;
            return tmp;
        },
        set: function (stakeholderInformation) {
            this._stakeholderInfo = stakeholderInformation;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "otherInfo", {
        get: function () {
            var tmp = this._otherInfo;
            return tmp;
        },
        set: function (otherInformation) {
            this._otherInfo = otherInformation;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "healthSanitation", {
        get: function () {
            var tmp = this._healthSanitation;
            return tmp;
        },
        set: function (healthSanitation) {
            this._healthSanitation = healthSanitation;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "classRoomEnvironment", {
        get: function () {
            var tmp = this._classRoomEnvironment;
            return tmp;
        },
        set: function (classRoomEnvironment) {
            this._classRoomEnvironment = classRoomEnvironment;
        },
        enumerable: true,
        configurable: true
    });
    LocalStoreServiceService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], LocalStoreServiceService);
    return LocalStoreServiceService;
}());



/***/ }),

/***/ "./src/app/services/need-assessment/health-sanitation.service.ts":
/*!***********************************************************************!*\
  !*** ./src/app/services/need-assessment/health-sanitation.service.ts ***!
  \***********************************************************************/
/*! exports provided: HealthSanitationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HealthSanitationService", function() { return HealthSanitationService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HealthSanitationService = /** @class */ (function () {
    function HealthSanitationService(httpClient, alertController) {
        this.httpClient = httpClient;
        this.alertController = alertController;
        this.baseUrl = 'http://localhost:8080/api/auth';
        this.getHealthSanitationBySchoolIdUrl = this.baseUrl + '/getHealthAssessment';
        this.saveHealthSanitationUrl = this.baseUrl + '/saveHealthAssessment';
    }
    HealthSanitationService.prototype.getHealthSanitationDetailsOfSchool = function (schoolId) {
        return this.httpClient.get(this.getHealthSanitationBySchoolIdUrl + "?schoolId=" + schoolId);
        // return this.getSampleJson();
    };
    HealthSanitationService.prototype.saveHealthSanitationDetails = function (healthSanitation) {
        var _this = this;
        return this.httpClient.post(this.saveHealthSanitationUrl, healthSanitation)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(function (e) {
            var status = e.status;
            if (status === 401) {
                _this.showAlert('You are not authorized to perform this action');
            }
            else {
                _this.showAlert("Error in saving data,Please try again");
            }
            throw new Error(e);
        }));
    };
    HealthSanitationService.prototype.getSampleJson = function () {
        var data = {
            "disposable": ["Through corporation/GP", "Having own waste management system"],
            "dustbins": "Yes",
            "firstAidBox": "Yes",
            "handWashArea": "No",
            "healthCamp": {
                "dental": "No",
                "eye": "Yes",
                "general": "No"
            },
            "medicine": "No",
            "renovation": "Yes",
            "schoolId": 1,
            "staffToCleanToilet": "Yes",
            "toiletDetail": {
                "details": [
                    {
                        "available": 2,
                        "pristine": 1,
                        "name": "Toilet for boys",
                        "repair": 2
                    },
                    {
                        "available": 2,
                        "pristine": 1,
                        "name": "Urinals for boys",
                        "repair": 2
                    },
                    {
                        "available": "",
                        "pristine": "",
                        "name": "Toilet for girls",
                        "repair": ""
                    },
                    {
                        "available": 2,
                        "pristine": 1,
                        "name": "Urinals for girls",
                        "repair": 2
                    },
                    {
                        "available": 2,
                        "pristine": 1,
                        "name": "Toilets for CWSN",
                        "repair": 2
                    },
                    {
                        "available": 2,
                        "pristine": 1,
                        "name": "Toilets for Staff",
                        "repair": 2
                    }
                ],
                "has": "Yes"
            },
            "utensilWashing": "Yes",
            "washProgram": {
                "drinkingWater": "Yes",
                "handWash": "Yes",
                "toiletUsage": "No"
            },
            "waterPurifier": {
                "details": [
                    {
                        "available": 2,
                        "pristine": 1,
                        "name": "RO Unit",
                        "repair": 2
                    },
                    {
                        "available": 2,
                        "pristine": 1,
                        "name": "UV Unit",
                        "repair": 2
                    },
                    {
                        "available": 2,
                        "pristine": 1,
                        "name": "Storage facility",
                        "repair": 2
                    }
                ],
                "has": "Yes"
            },
            "waterSource": ["Bore well", "Water Tanker"],
            "waterStorage": ["Sump", "Overhead Tank"],
        };
        return data;
    };
    HealthSanitationService.prototype.showAlert = function (msg) {
        var alert = this.alertController.create({
            message: msg,
            buttons: ['OK']
        });
        alert.then(function (alert) { return alert.present(); });
    };
    HealthSanitationService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]])
    ], HealthSanitationService);
    return HealthSanitationService;
}());



/***/ })

}]);
//# sourceMappingURL=health-sanitation-health-sanitation-module.js.map