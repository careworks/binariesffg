(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./node_modules/ngx-counter/ngx-counter.umd.js":
/*!*****************************************************!*\
  !*** ./node_modules/ngx-counter/ngx-counter.umd.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

(function (global, factory) {
	 true ? factory(exports, __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js"), __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js"), __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js")) :
	undefined;
}(this, (function (exports,_angular_core,_angular_common,_angular_forms) { 'use strict';

var CounterComponent = (function () {
    function CounterComponent() {
        this.change = new _angular_core.EventEmitter();
        this.font = 'monospace';
        this.color = 'blue';
        this.bgcolor = '#d6f0f9';
        this.size = 'medium';
        this.speed = 1000;
        this._countValue = 0;
        this.propagateChange = function (_) { };
    }
    Object.defineProperty(CounterComponent.prototype, "countValue", {
        /**
         * @return {?}
         */
        get: function () {
            return this._countValue;
        },
        /**
         * @param {?} val
         * @return {?}
         */
        set: function (val) {
            this._countValue = val;
            this.propagateChange(this._countValue);
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    CounterComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.blink = false;
        this.circuit = -1;
        this.limit = this.limit !== undefined ? this.limit : this.countValue + 10;
        switch (this.theme) {
            case 'red':
                this.color = 'red';
                this.bgcolor = '#fff0f0';
                break;
            case 'green':
                this.color = 'green';
                this.bgcolor = '#e0fff0';
                break;
            case 'gray':
                this.color = 'gray';
                this.bgcolor = '#f0f0f0';
                break;
            default:
                break;
        }
        this.timer = setInterval(function () {
            if (_this.countValue >= _this.limit) {
                _this.blink = false;
                _this.circuit = -1;
                clearInterval(_this.timer);
                return;
            }
            _this.circuit++;
            if (_this.circuit > 3) {
                _this.circuit = 0;
            }
            _this.blink = _this.circuit === 1 || _this.circuit === 3 ? !_this.blink : _this.blink;
            if (_this.circuit === 0) {
                _this.countValue++;
                _this.change.emit(_this.countValue);
            }
        }, this.speed / 4);
    };
    /**
     * @param {?} value
     * @return {?}
     */
    CounterComponent.prototype.writeValue = function (value) {
        if (value !== undefined) {
            this.countValue = value;
        }
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    CounterComponent.prototype.registerOnChange = function (fn) {
        this.propagateChange = fn;
    };
    /**
     * @return {?}
     */
    CounterComponent.prototype.registerOnTouched = function () { };
    /**
     * @return {?}
     */
    CounterComponent.prototype.getColor = function () {
        return this.color;
    };
    return CounterComponent;
}());
CounterComponent.decorators = [
    { type: _angular_core.Component, args: [{
                selector: 'ngx-counter',
                template: "\n<div class=\"ct-wrap ct-anim\"\n  [class.blink]=\"blink\"\n  [class.small]=\"size == 'small'\"\n  [class.medium]=\"size == 'medium'\"\n  [class.big]=\"size == 'big'\"\n  [class.large]=\"size == 'large'\"\n  [class.xl]=\"size == 'xl'\"\n  [style.color]=\"color\"\n  [style.background-color]=\"bgcolor\"\n  [style.border-top-color]=\"circuit === 0 ? getColor() : bgcolor\"\n  [style.border-right-color]=\"circuit === 1 ? getColor() : bgcolor\"\n  [style.border-bottom-color]=\"circuit === 2 ? getColor() : bgcolor\"\n  [style.border-left-color]=\"circuit === 3 ? getColor() : bgcolor\"\n  >\n  <div class=\"ct-count\"\n    [class.ct-1x]=\"size == 'small'\"\n    [class.ct-2x]=\"size == 'medium'\"\n    [class.ct-3x]=\"size == 'big'\"\n    [class.ct-4x]=\"size == 'large'\"\n    [class.ct-5x]=\"size == 'xl'\"\n    [style.font-family]=\"font\"\n  >\n    {{countValue}}\n  </div>\n</div>\n  ",
                styles: ["\n.ct-wrap {\n  display: inline-block;\n  /*background-color: #d6f0f9;*/\n  color: blue;\n  border-radius: 50%;\n  border-style: solid;\n  border-color: #d6f0f9;\n}\n\n.small {\n  border-width: 3px;\n  width: 40px;\n  height: 40px;\n}\n\n.medium {\n  border-width: 5px;\n  width: 50px;\n  height: 50px;\n}\n\n.big {\n  border-width: 7px;\n  width: 60px;\n  height: 60px;\n}\n\n.large {\n  border-width: 9px;\n  width: 70px;\n  height: 70px;\n}\n\n.xl {\n  border-width: 10px;\n  width: 80px;\n  height: 80px;\n}\n\n.ct-anim {\n  -webkit-transition: background-color 250ms, border-top-color 250ms, border-bottom-color 250ms, border-right-color 250ms, border-left-color 250ms;\n  transition: background-color 250ms, border-top-color 250ms, border-bottom-color 250ms, border-right-color 250ms, border-left-color 250ms;\n}\n\n.blink {\n  background-color: #fcfcfc !important;\n}\n\n.ct-count {\n  font-family: monospace;\n  position: relative;\n  text-align: center;\n  top: 50%;\n  -webkit-transform: translateY(-50%);\n  -ms-transform: translateY(-50%);\n  transform: translateY(-50%);\n}\n\n.ct-5x {\n  font-size: 48px;\n  font-weight: bold;\n}\n\n.ct-4x {\n  font-size: 40px;\n  font-weight: bold;\n}\n\n.ct-3x {\n  font-size: 32px;\n  font-weight: bold;\n}\n\n.ct-2x {\n  font-size: 24px;\n  font-weight: bold;\n}\n\n.ct-1x {\n  font-size: 16px;\n  font-weight: bold;\n}\n  "],
                providers: [
                    {
                        provide: _angular_forms.NG_VALUE_ACCESSOR,
                        useExisting: _angular_core.forwardRef(function () { return CounterComponent; }),
                        multi: true
                    }
                ]
            },] },
];
/**
 * @nocollapse
 */
CounterComponent.ctorParameters = function () { return []; };
CounterComponent.propDecorators = {
    'change': [{ type: _angular_core.Output },],
    'font': [{ type: _angular_core.Input },],
    'theme': [{ type: _angular_core.Input },],
    'color': [{ type: _angular_core.Input },],
    'bgcolor': [{ type: _angular_core.Input },],
    'size': [{ type: _angular_core.Input },],
    'speed': [{ type: _angular_core.Input },],
    'limit': [{ type: _angular_core.Input },],
    'countValue': [{ type: _angular_core.Input },],
};

var CounterModule = (function () {
    function CounterModule() {
    }
    /**
     * @return {?}
     */
    CounterModule.forRoot = function () {
        return {
            ngModule: CounterModule
        };
    };
    return CounterModule;
}());
CounterModule.decorators = [
    { type: _angular_core.NgModule, args: [{
                imports: [
                    _angular_common.CommonModule
                ],
                declarations: [
                    CounterComponent
                ],
                exports: [
                    CounterComponent
                ]
            },] },
];
/**
 * @nocollapse
 */
CounterModule.ctorParameters = function () { return []; };

exports.CounterModule = CounterModule;
exports.CounterComponent = CounterComponent;

Object.defineProperty(exports, '__esModule', { value: true });

})));


/***/ }),

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");
/* harmony import */ var ngx_counter__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-counter */ "./node_modules/ngx-counter/ngx-counter.umd.js");
/* harmony import */ var ngx_counter__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(ngx_counter__WEBPACK_IMPORTED_MODULE_6__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                ngx_counter__WEBPACK_IMPORTED_MODULE_6__["CounterModule"].forRoot(),
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild([
                    {
                        path: '',
                        component: _home_page__WEBPACK_IMPORTED_MODULE_5__["HomePage"]
                    }
                ])
            ],
            declarations: [_home_page__WEBPACK_IMPORTED_MODULE_5__["HomePage"]]
        })
    ], HomePageModule);
    return HomePageModule;
}());



/***/ }),

/***/ "./src/app/home/home.page.html":
/*!*************************************!*\
  !*** ./src/app/home/home.page.html ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-menu-button></ion-menu-button>\r\n    </ion-buttons>\r\n    <ion-title>\r\n      Search Schools\r\n    </ion-title>\r\n    <ion-button slot=\"end\" color=\"secondary\" (click)=\"logout()\">Logout\r\n      <ion-icon name=\"lock\"></ion-icon>\r\n    </ion-button>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content  class=\"background\">\r\n\r\n<ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\r\n  <ion-fab-button ion-button (click)=\"navigateSchoolInfo()\">\r\n    <ion-icon name=\"add\"></ion-icon>\r\n  </ion-fab-button>\r\n</ion-fab>\r\n  \r\n  <!----ion-item>\r\n    <ion-input placeholder=\"Search School Profile\" [(ngModel)]='text' (input)='onInputTime($event.target.value)' (ionChange)='onChangeTime($event.target.value)'></ion-input>\r\n    <ion-button class=\"cancelButton\" color=\"secondary\" *ngIf=\"showCancelButton\">\r\n      <ion-icon class=\"cancelIcon\" name=\"close-circle-outline\"></ion-icon>\r\n    </ion-button>\r\n    <ion-button color=\"secondary\" (click)=\"getSearchResults()\">Seach\r\n      <ion-icon name=\"Search\"></ion-icon>\r\n    </ion-button>\r\n    <ion-button color=\"secondary\" (click)=\"presentAlertPrompt()\">Advance Seach\r\n      <ion-icon name=\"Search\"></ion-icon>\r\n    </ion-button>\r\n  </ion-item -->\r\n  \r\n  <ion-item class=\"textField\" [hidden]=\"advancedSearchVisible\">\r\n    <ion-searchbar [(ngModel)]=\"searchTerm\" placeholder=\"Search school profile (Enter atleast 3 characters to begin search)\" \r\n    (ionChange)=\"searchChanged($event)\"></ion-searchbar>\r\n    <ion-button button (click)=\"advancedSearch()\" class=\"advancedSearch\">Advanced Search</ion-button>\r\n  </ion-item>\r\n    <div [hidden]=\"!advancedSearchVisible\">\r\n      <ion-item class=\"textField\">\r\n        <ion-label>School Name</ion-label>\r\n        <ion-input placeholder=\"Enter School Name\" [(ngModel)]=\"searchName\" name = \"name\"></ion-input>\r\n      </ion-item>\r\n      <ion-item class=\"textField\">\r\n        <ion-label>School City</ion-label>\r\n        <ion-input placeholder=\"Enter City Name\" [(ngModel)]=\"searchCity\" name = \"city\"></ion-input>\r\n      </ion-item>\r\n      <ion-item class=\"textField\">\r\n        <ion-label>School Cluster</ion-label>\r\n        <ion-input placeholder=\"Enter Cluster Name\" [(ngModel)]=\"searchCluster\" name = \"cluster\"></ion-input>\r\n      </ion-item>\r\n      <ion-item class=\"textField\">\r\n        <ion-label>School Region</ion-label>\r\n        <ion-input placeholder=\"Enter Region Name\" [(ngModel)]=\"searchRegion\" name = \"region\"></ion-input>\r\n      </ion-item>\r\n        <ion-button button (click)=\"advancedSearch()\">Search</ion-button>\r\n    </div>    \r\n  <ion-list>\r\n\r\n \r\n    <ion-item button *ngFor=\"let item of (results | async)\" (click)=\"navigateToDashboard(item.school_id, item.school_Name)\">\r\n      <!----ion-avatar slot=\"start\">\r\n        <img [src]=\"item.Poster\" *ngIf=\"item.Poster != 'N/A'\">\r\n      </ion-avatar-->\r\n\r\n      <ion-label text-wrap>\r\n        School Name: {{ item.school_Name }}\r\n        <h3>School Address: {{ item.school_Address }}</h3>\r\n      </ion-label>\r\n \r\n      <!-- <ion-icon slot=\"end\" *ngIf=\"item.Type == 'movie'\" name=\"videocam\"></ion-icon>\r\n      <ion-icon slot=\"end\" *ngIf=\"item.Type == 'series'\" name=\"tv\"></ion-icon>\r\n      <ion-icon slot=\"end\" *ngIf=\"item.Type == 'game'\" name=\"logo-game-controller-b\"></ion-icon> -->\r\n \r\n    </ion-item>\r\n \r\n  </ion-list>\r\n\r\n  <ion-item position=\"fixed\">\r\n      <!-- <ion-grid padding> -->\r\n        <!-- <ion-row padding>\r\n            \r\n          <ion-col><ion-label>Schools Onboarded</ion-label></ion-col>\r\n          <ion-col><ion-label>Assessments Completed</ion-label></ion-col>\r\n        </ion-row>\r\n        <br/>\r\n        <ion-row padding>\r\n          <ion-col>\r\n              <ngx-counter countValue=0 [speed]=1000 size=\"xl\" limit=3 color=\"green\"></ngx-counter>\r\n          </ion-col>\r\n          <ion-col>\r\n              <ngx-counter countValue=0 [speed]=1000 size=\"xl\" limit=2></ngx-counter>\r\n          </ion-col>\r\n        </ion-row> -->\r\n        <ion-slide>\r\n            <div class=\"row\" padding>\r\n              <div class=\"col col-50\" padding>\r\n                <ngx-counter countValue=0 [speed]=1000 size=\"xl\" limit=3 color=\"green\"></ngx-counter><br>\r\n                Schools Onboarded\r\n              </div>\r\n            </div>\r\n            <div class=\"row\" padding>\r\n              <div class=\"col col-50\" padding>\r\n                <ngx-counter countValue=0 [speed]=1000 size=\"xl\" limit=2></ngx-counter>\r\n                <br>\r\n                Completed Assessments\r\n              </div>\r\n            </div>\r\n            </ion-slide>\r\n      <!-- </ion-grid> -->\r\n    </ion-item> \r\n\r\n   \r\n    <!----ion-card class=\"welcome-card\" *ngIf=\"showSearchResult\">\r\n      <ion-img src=\"/assets/schoolImage.jpg\"></ion-img>\r\n      <ion-card-header>\r\n        <ion-card-subtitle>School Information</ion-card-subtitle>\r\n        <ion-card-title>Cluny Convent High School</ion-card-title>\r\n      </ion-card-header>\r\n      <ion-card-content>\r\n        <p>Cluny Convent High School, Malleswaram, India, has been in existence since the year 1948. It is located in a spacious building situated on 11th Main Road, Near 15th Cross, Malleswaram, Bangalore, Karnataka State, India.</p>\r\n      </ion-card-content>\r\n    </ion-card>\r\n    <ion-list lines=\"none\">\r\n      <ion-list-header>\r\n        <ion-label>Resources</ion-label>\r\n      </ion-list-header>\r\n      <ion-item href=\"https://beta.ionicframework.com/docs/\">\r\n        <ion-icon slot=\"start\" color=\"medium\" name=\"book\"></ion-icon>\r\n        <ion-label>Ionic Documentation</ion-label>\r\n      </ion-item>\r\n      <ion-item href=\"https://beta.ionicframework.com/docs/building/scaffolding\">\r\n        <ion-icon slot=\"start\" color=\"medium\" name=\"build\"></ion-icon>\r\n        <ion-label>Scaffold Out Your App</ion-label>\r\n      </ion-item>\r\n      <ion-item href=\"https://beta.ionicframework.com/docs/layout/structure\">\r\n        <ion-icon slot=\"start\" color=\"medium\" name=\"grid\"></ion-icon>\r\n        <ion-label>Change Your App Layout</ion-label>\r\n      </ion-item>\r\n      <ion-item href=\"https://beta.ionicframework.com/docs/theming/basics\">\r\n        <ion-icon slot=\"start\" color=\"medium\" name=\"color-fill\"></ion-icon>\r\n        <ion-label>Theme Your App</ion-label>\r\n      </ion-item>\r\n    </ion-list -->\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/home/home.page.scss":
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".welcome-card ion-img {\n  max-height: 35vh;\n  overflow: hidden; }\n\n.cancelButton {\n  height: 25px; }\n\n.cancelIcon {\n  height: 24px; }\n\n.textField {\n  /*--box-shadow: 0px 14px 25px rgba(182, 30, 30, 0.59);*/\n  margin-bottom: 5px; }\n\n.advancedSearch {\n  height: 65%; }\n\n.textField ion-label {\n  width: 30%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9DOlxcdmFyc2hhXFxjd1xcY2FyZXdvcmtzdWlcXENhcmV3b3Jrcy9zcmNcXGFwcFxcaG9tZVxcaG9tZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxnQkFBZ0I7RUFDaEIsZ0JBQWdCLEVBQUE7O0FBT2xCO0VBRUMsWUFBWSxFQUFBOztBQUdiO0VBRUMsWUFBWSxFQUFBOztBQUdiO0VBQ0ksdURBQUE7RUFDQSxrQkFBa0IsRUFBQTs7QUFFdEI7RUFDQyxXQUFXLEVBQUE7O0FBRVo7RUFDQyxVQUFVLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9ob21lL2hvbWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLndlbGNvbWUtY2FyZCBpb24taW1nIHtcclxuICBtYXgtaGVpZ2h0OiAzNXZoO1xyXG4gIG92ZXJmbG93OiBoaWRkZW47XHJcbn1cclxuXHJcbmlvbi1jb250ZW50LmJhY2tncm91bmR7XHJcbiAgLy8gLS1iYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL2NoaWxkcmVuXzIuanBlZykgMCAwLzEwMCUgMTAwJSBuby1yZXBlYXQ7XHJcbn1cclxuXHJcbi5jYW5jZWxCdXR0b24ge1xyXG5cdFxyXG5cdGhlaWdodDogMjVweDtcclxufVxyXG5cclxuLmNhbmNlbEljb24ge1xyXG5cdFxyXG5cdGhlaWdodDogMjRweDtcclxufVxyXG5cclxuLnRleHRGaWVsZHtcclxuICAgIC8qLS1ib3gtc2hhZG93OiAwcHggMTRweCAyNXB4IHJnYmEoMTgyLCAzMCwgMzAsIDAuNTkpOyovXHJcbiAgICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbn1cclxuLmFkdmFuY2VkU2VhcmNoe1xyXG5cdGhlaWdodDogNjUlO1xyXG59XHJcbi50ZXh0RmllbGQgaW9uLWxhYmVse1xyXG5cdHdpZHRoOiAzMCU7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/home/home.page.ts":
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _services_local_store_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/local-store-service.service */ "./src/app/services/local-store-service.service.ts");
/* harmony import */ var _services_school_profile_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/school-profile.service */ "./src/app/services/school-profile.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







var HomePage = /** @class */ (function () {
    function HomePage(router, menuCtrl, alertController, schoolProfileService, authService, localStoreService) {
        this.router = router;
        this.menuCtrl = menuCtrl;
        this.alertController = alertController;
        this.schoolProfileService = schoolProfileService;
        this.authService = authService;
        this.localStoreService = localStoreService;
        this.showSearchResult = false;
        this.showCancelButton = false;
        this.advancedSearchVisible = false;
        this.searchTerm = '';
        this.type = _services_school_profile_service__WEBPACK_IMPORTED_MODULE_5__["SearchType"].name;
        this.menuCtrl.enable(true);
    }
    HomePage.prototype.navigateToDashboard = function (schoolId, schoolName) {
        this.localStoreService.schoolId = schoolId;
        this.localStoreService.schoolName = schoolName;
        this.router.navigateByUrl('/school-dashboard');
    };
    HomePage.prototype.advancedSearch = function () {
        this.advancedSearchVisible = !this.advancedSearchVisible;
    };
    HomePage.prototype.navigateSchoolInfo = function () {
        this.localStoreService.isSearchSchoolFlow = false;
        this.router.navigateByUrl('/school-info/school-profile');
    };
    HomePage.prototype.searchChanged = function () {
        // Call our service function which returns an Observable
        if (this.searchTerm.length > 2) {
            this.results = this.schoolProfileService.searchData(this.searchTerm, this.type);
        }
        else {
            this.results = null;
        }
    };
    HomePage.prototype.getSearchResults = function () {
        this.showSearchResult = true;
    };
    HomePage.prototype.onChangeTime = function (value) {
        if (value != '') {
            this.showCancelButton = true;
        }
    };
    HomePage.prototype.onInputTime = function (value) {
        if (value === '') {
            this.showCancelButton = false;
        }
    };
    HomePage.prototype.presentAlertPrompt = function () {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Advanced Search',
                            inputs: [
                                {
                                    name: 'School Name',
                                    type: 'text',
                                    placeholder: 'School Name'
                                },
                                {
                                    name: 'City',
                                    type: 'text',
                                    id: 'City-id',
                                    value: '',
                                    placeholder: 'City'
                                },
                                {
                                    name: 'Region',
                                    value: '',
                                    type: 'text',
                                    placeholder: 'Region'
                                },
                                {
                                    name: 'Pin Code',
                                    value: '',
                                    type: 'number',
                                    placeholder: 'Pin Code'
                                },
                                // input date with min & max
                                {
                                    name: 'name4',
                                    type: 'date',
                                    min: '2017-03-01',
                                    max: '2018-01-12'
                                },
                                // input date without min nor max
                                {
                                    name: 'name5',
                                    type: 'date'
                                },
                                {
                                    name: 'name6',
                                    type: 'number',
                                    min: -5,
                                    max: 10
                                },
                                {
                                    name: 'name7',
                                    type: 'number'
                                }
                            ],
                            buttons: [
                                {
                                    text: 'Cancel',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () {
                                        console.log('Confirm Cancel');
                                    }
                                }, {
                                    text: 'Search',
                                    handler: function () {
                                        console.log('Confirm Search');
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.logout = function () {
        this.authService.logout();
    };
    HomePage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.page.html */ "./src/app/home/home.page.html"),
            styles: [__webpack_require__(/*! ./home.page.scss */ "./src/app/home/home.page.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            _services_school_profile_service__WEBPACK_IMPORTED_MODULE_5__["SchoolProfileService"], _services_authentication_service__WEBPACK_IMPORTED_MODULE_1__["AuthenticationService"],
            _services_local_store_service_service__WEBPACK_IMPORTED_MODULE_4__["LocalStoreServiceService"]])
    ], HomePage);
    return HomePage;
}());



/***/ })

}]);
//# sourceMappingURL=home-home-module.js.map