(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~education-dept-education-dept-module~home-home-module~other-info-other-info-module~school-in~86281da7"],{

/***/ "./src/app/services/local-store-service.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/services/local-store-service.service.ts ***!
  \*********************************************************/
/*! exports provided: LocalStoreServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocalStoreServiceService", function() { return LocalStoreServiceService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LocalStoreServiceService = /** @class */ (function () {
    function LocalStoreServiceService() {
        this._isSearchSchoolFlow = true;
    }
    Object.defineProperty(LocalStoreServiceService.prototype, "sampleJson", {
        get: function () {
            var tmp = this._jsonData;
            return tmp;
        },
        set: function (jdata) {
            this._jsonData = jdata;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "isSearchSchoolFlow", {
        get: function () {
            var tmp = this._isSearchSchoolFlow;
            return tmp;
        },
        set: function (searchSchoolFlow) {
            this._isSearchSchoolFlow = searchSchoolFlow;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "schoolName", {
        get: function () {
            var tmp = this._schoolName;
            return tmp;
        },
        set: function (schoolProfilePage) {
            this._schoolName = schoolProfilePage;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "schoolId", {
        get: function () {
            var id = this._schoolId;
            return id;
        },
        set: function (id) {
            this._schoolId = id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "schoolProfileBasic", {
        get: function () {
            var tmp = this._schoolProfileBasic;
            return tmp;
        },
        set: function (schoolProfile) {
            this._schoolProfileBasic = schoolProfile;
            console.log(this._schoolProfileBasic.name);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "eduDeptFunction", {
        get: function () {
            var tmp = this._eduDeptFunction;
            return tmp;
        },
        set: function (eduDept) {
            this._eduDeptFunction = eduDept;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "teacherDetails", {
        get: function () {
            var tmp = this._teacherDetails;
            return tmp;
        },
        set: function (teacherDetail) {
            this._teacherDetails = teacherDetail;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "stakeholderInfo", {
        get: function () {
            var tmp = this._stakeholderInfo;
            return tmp;
        },
        set: function (stakeholderInformation) {
            this._stakeholderInfo = stakeholderInformation;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "otherInfo", {
        get: function () {
            var tmp = this._otherInfo;
            return tmp;
        },
        set: function (otherInformation) {
            this._otherInfo = otherInformation;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "healthSanitation", {
        get: function () {
            var tmp = this._healthSanitation;
            return tmp;
        },
        set: function (healthSanitation) {
            this._healthSanitation = healthSanitation;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "classRoomEnvironment", {
        get: function () {
            var tmp = this._classRoomEnvironment;
            return tmp;
        },
        set: function (classRoomEnvironment) {
            this._classRoomEnvironment = classRoomEnvironment;
        },
        enumerable: true,
        configurable: true
    });
    LocalStoreServiceService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], LocalStoreServiceService);
    return LocalStoreServiceService;
}());



/***/ }),

/***/ "./src/app/services/school-profile.service.ts":
/*!****************************************************!*\
  !*** ./src/app/services/school-profile.service.ts ***!
  \****************************************************/
/*! exports provided: SearchType, SchoolProfileService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchType", function() { return SearchType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SchoolProfileService", function() { return SchoolProfileService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _util_httpWrapper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../util/httpWrapper */ "./src/util/httpWrapper.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_local_store_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/local-store-service.service */ "./src/app/services/local-store-service.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// Typescript custom enum for search types (optional)
var SearchType;
(function (SearchType) {
    SearchType["All"] = "";
    SearchType["name"] = "name";
    SearchType["city"] = "city";
    SearchType["pincode"] = "pincode";
    SearchType["state"] = "state";
})(SearchType || (SearchType = {}));
var SchoolProfileService = /** @class */ (function () {
    /**
     * Constructor of the Service with Dependency Injection
     * @param http The standard Angular HttpClient to make requests
     */
    function SchoolProfileService(http, localStoreService) {
        this.http = http;
        this.localStoreService = localStoreService;
        this.getSchoolUrl = "/school/SchoolProfilesByName/";
        this.getSchoolByIdUrl = "/school/schoolDetails/";
        this.addSchoolProfile = '';
        this.apiKey = ''; // <-- Enter your own key here!
    }
    /**
    * Get data from the OmdbApi
    * map the result to return only the results that we need
    *
    * @param {string} title Search Term
    * @param {SearchType} type movie, series, episode or empty
    * @returns Observable with the search results
    */
    SchoolProfileService.prototype.searchData = function (title, type) {
        return this.http.get(this.getSchoolUrl + "?name=" + title).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (results) {
            console.log('Raw', results);
            return results;
        }));
    };
    /**
    * Get the detailed information for an ID using the "i" parameter
    *
    * @param {string} id imdbID to retrieve information
    * @returns Observable with detailed information
    */
    SchoolProfileService.prototype.getSchoolDetails = function (id) {
        return this.http.get("" + this.getSchoolByIdUrl + id);
    };
    SchoolProfileService.prototype.getSampleJson = function () {
        var data = {
            "schoolProfileBasic": {
                "schoolId": 10111,
                "name": "Iblur High School",
                "establishedYear": "2000-03-19",
                "location": "urban",
                "address": "Iblur Main Road, Bangalore",
                "schoolCluster": 'Cluster A',
                "block": 'Block A',
                "region": 'Region A',
                "district": "Bangalore",
                "state": "Karnataka",
                "category": {
                    "category": "GLPS",
                    "description": "Govt. Lower Primary School (1-5th)"
                },
                "type": {
                    "description": "Boys",
                    "type": "Boys"
                },
                "medium": ["English", "Kannada"],
                "diseCode": 202,
                "emailId": "iblurschool@gmail.com",
                "studentCount": 230
            },
            "schoolProfileAdvanced": {
                /* This object will always return the number of objects as many as the number of mediums in the school */
                "studentDetails": [
                    {
                        "medium": "Kannada",
                        "details": [
                            {
                                "class": 1,
                                "sectionCount": 1,
                                "totalBoys": 50,
                                "totalGirls": 60,
                                "scboysNumber": 10,
                                "scgirlsNumber": 10,
                                "stboysNumber": 10,
                                "stgirlsNumber": 20
                            },
                            {
                                "class": 2,
                                "sectionCount": 1,
                                "totalBoys": 50,
                                "totalGirls": 60,
                                "scboysNumber": 10,
                                "scgirlsNumber": 10,
                                "stboysNumber": 10,
                                "stgirlsNumber": 20
                            }
                        ]
                    },
                    {
                        "medium": "English",
                        "details": [
                            {
                                "class": 1,
                                "sectionCount": 1,
                                "totalBoys": 50,
                                "totalGirls": 60,
                                "scboysNumber": 10,
                                "scgirlsNumber": 10,
                                "stboysNumber": 10,
                                "stgirlsNumber": 20
                            }
                        ]
                    }
                ],
                "eduDeptFunction": [
                    {
                        "designation": "HM",
                        "name": "Manjula",
                        "contactDetails": "9999988888",
                        "emailId": "contact@gmail.com"
                    },
                    {
                        "designation": "HMInCharge",
                        "name": "Manjunath",
                        "contactDetails": "9999988888",
                        "emailId": "contact@gmail.com"
                    }
                ],
                "teacherDetails": [
                    {
                        "teacherType": "Full-time",
                        "occupationType": "Maths",
                        "gender": "Female",
                        "teacherCount": 3
                    },
                    {
                        "teacherType": "Full-time",
                        "occupationType": "Science",
                        "gender": "Male",
                        "teacherCount": 6
                    },
                    {
                        "teacherType": "Part-time",
                        "occupationType": "Art & Craft",
                        "gender": "Male",
                        "teacherCount": 1
                    }
                ],
                "stakeholderInfo": [
                    {
                        "name": "Manjula",
                        "memberCount": 30,
                        "formationYear": "2000-03-19",
                        "spocName": "Manjunath",
                        "contactNumber": "9999988888"
                    },
                    {
                        "name": "Manjula",
                        "memberCount": 30,
                        "formationYear": "2000-03-19",
                        "spocName": "Manjunath",
                        "contactNumber": "9999988888"
                    }
                ],
                /* Any Other Information section is optional now */
                "anyOtherInfo": "sample"
            }
        };
        return data;
    };
    SchoolProfileService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_util_httpWrapper__WEBPACK_IMPORTED_MODULE_1__["HttpWrapper"], _services_local_store_service_service__WEBPACK_IMPORTED_MODULE_3__["LocalStoreServiceService"]])
    ], SchoolProfileService);
    return SchoolProfileService;
}());



/***/ })

}]);
//# sourceMappingURL=default~education-dept-education-dept-module~home-home-module~other-info-other-info-module~school-in~86281da7.js.map