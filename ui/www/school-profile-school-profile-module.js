(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["school-profile-school-profile-module"],{

/***/ "./src/app/models/school-profile-model.ts":
/*!************************************************!*\
  !*** ./src/app/models/school-profile-model.ts ***!
  \************************************************/
/*! exports provided: SchoolProfileModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SchoolProfileModel", function() { return SchoolProfileModel; });
var SchoolProfileModel = /** @class */ (function () {
    function SchoolProfileModel() {
    }
    return SchoolProfileModel;
}());



/***/ }),

/***/ "./src/app/school-profile/school-profile.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/school-profile/school-profile.module.ts ***!
  \*********************************************************/
/*! exports provided: SchoolProfilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SchoolProfilePageModule", function() { return SchoolProfilePageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _school_profile_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./school-profile.page */ "./src/app/school-profile/school-profile.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _school_profile_page__WEBPACK_IMPORTED_MODULE_5__["SchoolProfilePage"]
    }
];
var SchoolProfilePageModule = /** @class */ (function () {
    function SchoolProfilePageModule() {
    }
    SchoolProfilePageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_school_profile_page__WEBPACK_IMPORTED_MODULE_5__["SchoolProfilePage"]]
        })
    ], SchoolProfilePageModule);
    return SchoolProfilePageModule;
}());



/***/ }),

/***/ "./src/app/school-profile/school-profile.page.html":
/*!*********************************************************!*\
  !*** ./src/app/school-profile/school-profile.page.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<ion-content padding>\r\n\r\n\t<div [hidden]=\"!shouldHideTable\">\r\n\r\n\t<ion-item class=\"textField\">\r\n\t<ion-button class=\"saveContinue\" (click)=\"saveandContinueFunc()\" block>Save and Continue</ion-button>\r\n\t<ion-button class=\"saveButton\" (click)=\"saveFunc()\" block>Save</ion-button>\r\n\t</ion-item>\r\n\t<ion-item class=\"textField\">\r\n\t\t\t<ion-label>School Name</ion-label>\r\n\t\t\t<ion-input placeholder=\"Enter School Name\" [(ngModel)]=\"spm.name\" name = \"name\"></ion-input>\r\n\t</ion-item>\r\n\r\n\t<ion-item class=\"textField\">\r\n\t<ion-label class=\"dateLabel\">Established In</ion-label>\r\n\t<ion-datetime display-format=\"MMM DD, YYYY\" [(ngModel)]=\"spm.establishedYear\" name =\"establishedYear\"></ion-datetime>\r\n</ion-item>\r\n\t<ion-item>\r\n\r\n\t\t<ion-grid class=\"radioGroup\">\r\n\t\t\t<ion-row>\r\n\t\t\t\t<ion-col>\r\n\t\t\t\t\t<ion-radio-group  [(ngModel)]=\"spm.location\" name = \"radio_1\">\r\n\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t <ion-label class=\"schoolLocatedLabel\">School Located</ion-label>\r\n\t\t\t\t\t\t<ion-col class=\"radioGroupCol\">\r\n\t\t\t\t\t\t\t\t <ion-label>Rural</ion-label>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t<ion-col class=\"radioGroupCol\">   \t\r\n\t\t\t\t\t\t\t\t<ion-radio value=\"rural\"  ></ion-radio>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t<ion-col class=\"radioGroupCol\">\r\n\t\t\t\t\t\t\t\t<ion-label>Urban</ion-label>\r\n\t\t\t\t\t\t</ion-col>  \r\n\t\t\t\t\t\t<ion-col class=\"radioGroupCol\">\t\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\"urban\" ></ion-radio>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t<ion-col class=\"radioGroupCol\">\r\n\t\t\t\t\t\t\t\t<ion-label>Tribal</ion-label>\r\n\t\t\t\t\t\t</ion-col>  \r\n\t\t\t\t\t\t<ion-col class=\"radioGroupCol\">\t\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\"tribal\"  ></ion-radio>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t</ion-radio-group>  \r\n\t\t\t\t</ion-col>\r\n\t\t\t</ion-row>\r\n\t</ion-grid>\t  \r\n\r\n</ion-item>\r\n\t<ion-item class=\"textField\">\r\n\t<ion-label>School Address</ion-label>\r\n\t<ion-input class=\"address\" [(ngModel)]=\"spm.address\" name = \"address\">\r\n\t</ion-input>\r\n</ion-item>\r\n\t<ion-item class=\"textField\">\r\n\t\t\t<ion-label>School Cluster</ion-label>\r\n\t\t\t<ion-input placeholder=\"Enter Input\" [(ngModel)]=\"spm.schoolCluster\" name = \"schoolCluster\"></ion-input>\r\n\t</ion-item>\r\n\t<ion-item class=\"textField\">\r\n\t\t\t<ion-label>School Block</ion-label>\r\n\t\t\t<ion-input placeholder=\"Enter Input\" [(ngModel)]=\"spm.block\" name = \"block\"></ion-input>\r\n\t</ion-item>\r\n\t<ion-item class=\"textField\">\r\n\t\t\t<ion-label>District</ion-label>\r\n\t\t\t<ion-input placeholder=\"Enter Input\" [(ngModel)]=\"spm.district\" name = \"district\"></ion-input>\r\n\t</ion-item>\r\n\t<ion-item class=\"textField\">\r\n\t\t\t<ion-label>State</ion-label>\r\n\t\t\t<ion-input placeholder=\"Enter Input\" [(ngModel)]=\"spm.state\" name = \"state\"></ion-input>\r\n\t</ion-item>\r\n\t<ion-grid class=\"radioGroup\">\r\n\t\t<ion-row>\r\n\t\t\t<ion-col>\r\n\t\t\t\t<ion-label>School Category</ion-label>\r\n\t\t\t</ion-col>\r\n\t\t</ion-row>\r\n\t\t\t<ion-radio-group [(ngModel)]=\"spm.category\" name = \"category\">\r\n\t\t\t<ion-row>\r\n\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t <ion-label class=\"radioLabel\">GLPS</ion-label>\r\n\t\t\t\t\t\t<ion-radio value=\"GLPS\"></ion-radio>\r\n\t\t\t\t</ion-col>\r\n\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t <ion-label class=\"radioLabel\">GHPS</ion-label>\r\n\t\t\t\t\t\t<ion-radio value=\"GHPS\"></ion-radio>\r\n\t\t\t\t</ion-col>\r\n\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t<ion-label class=\"radioLabel\">GMPS</ion-label>\r\n\t\t\t\t\t\t\t<ion-radio value=\"GMPS\"></ion-radio>\r\n\t\t\t\t</ion-col>\r\n\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t<ion-label class=\"radioLabel\">GHS</ion-label>\r\n\t\t\t\t\t\t\t<ion-radio value=\"GHS\"></ion-radio>\r\n\t\t\t\t</ion-col>\r\n\t\t\t</ion-row>\r\n\t\t</ion-radio-group>  \r\n\t</ion-grid>\r\n\r\n\t<ion-grid class=\"radioGroup\">\r\n\t\t<ion-row>\r\n\t\t\t<ion-col>\r\n\t\t\t\t<ion-label>School Type</ion-label>\r\n\t\t\t</ion-col>\r\n\t\t</ion-row>\r\n\t\t\t<ion-radio-group [(ngModel)]=\"spm.type\" name = \"type\">\r\n\t\t\t<ion-row>\r\n\t\t\t\t<ion-col class=\"schoolTypeCol\">\r\n\t\t\t\t\t\t <ion-label class=\"radioLabel\">BOY</ion-label>\r\n\t\t\t\t\t\t<ion-radio value=\"Boys\"></ion-radio>\r\n\t\t\t\t</ion-col>\r\n\t\t\t\t<ion-col class=\"schoolTypeCol\">\r\n\t\t\t\t\t\t <ion-label class=\"radioLabel\">GIRL</ion-label>\r\n\t\t\t\t\t\t<ion-radio value=\"Girls\"></ion-radio>\r\n\t\t\t\t</ion-col>\r\n\t\t\t\t<ion-col class=\"schoolTypeCol\">\r\n\t\t\t\t\t\t<ion-label class=\"radioLabel\">Co-Education</ion-label>\r\n\t\t\t\t\t\t\t<ion-radio value=\"Co-Education\"></ion-radio>\r\n\t\t\t\t</ion-col>\r\n\t\t\t</ion-row>\r\n\t\t</ion-radio-group>  \r\n\t</ion-grid>\t\r\n<ion-list>\r\n\t<ion-item>\r\n\t\t<ion-label>Medium of instruction</ion-label>\r\n\t\t<ion-select multiple=\"true\" cancelText=\"Nah\" okText=\"Okay!\" [(ngModel)]=\"spm.medium\" name = \"medium\">\r\n\t\t\t<ion-select-option value=\"Kannada\">Kannada</ion-select-option>\r\n\t\t\t<ion-select-option value=\"English\">English</ion-select-option>\r\n\t\t\t<ion-select-option value=\"Tamil\">Tamil</ion-select-option>\r\n\t\t\t<ion-select-option value=\"Telugu\">Telugu</ion-select-option>\r\n\t\t\t<ion-select-option value=\"Urdu\">Urdu</ion-select-option>\r\n\t\t\t<ion-select-option value=\"Hindi\">Hindi</ion-select-option>\r\n\t\t\t<ion-select-option value=\"Other\">Others</ion-select-option>\r\n\t\t</ion-select>\r\n\t</ion-item>\r\n</ion-list>\r\n\r\n<ion-item class=\"textField\">\r\n\t\t\t<ion-label>School DISE Code</ion-label>\r\n\t\t\t<ion-input placeholder=\"Enter Input\" [(ngModel)]=\"spm.diseCode\" name = \"diseCode\"></ion-input>\r\n\t</ion-item>\r\n\r\n\t<ion-item class=\"textField\">\r\n\t\t\t<ion-label>School E-mail Id</ion-label>\r\n\t\t\t<ion-input placeholder=\"Enter Input\" [(ngModel)]=\"spm.emailId\" name = \"emailId\"></ion-input>\r\n\t</ion-item>\r\n\r\n\t<ion-item class=\"textField\">\r\n\t\t\t<ion-label>Total Students</ion-label>\r\n\t\t\t<ion-input placeholder=\"Enter Input\" [(ngModel)]=\"spm.studentCount\" name = \"studentCount\"></ion-input>\r\n\t</ion-item>\r\n\r\n</div>\r\n\t<!-- ======================================== -->\r\n\r\n\r\n\r\n\t<div [hidden]=\"shouldHideTable\">\r\n<br>\r\n<ion-item class=\"textField\">\r\n\t<ion-button class=\"saveButton\" (click)=\"editFunc()\" block>Edit</ion-button>\r\n\t</ion-item>\r\n\t\t<ion-list>\r\n\t\t\t<ion-card-content> \r\n\t\t\t<ion-item class=\"textField\">\r\n\t\t\t<ion-label class =\"view_label\"> School Name </ion-label>\r\n\t\t\t <div class=\"text-data\">{{spm.name}}</div>\r\n\t\t\t</ion-item>\r\n\t\t\t</ion-card-content> \r\n\t\t\t<ion-card-content>\r\n\t\t\t<ion-item class=\"textField\">\r\n\t\t\t<ion-label class =\"view_label\"> Established In  </ion-label>\r\n\t\t\t<div class=\"text-data\">{{this.spm.establishedYear}}</div>\r\n\t\t\t</ion-item>\r\n\t\t\t</ion-card-content>\r\n\t\t\t<ion-card-content>\r\n\t\t\t<ion-item class=\"textField\">\r\n\t\t\t<ion-label class =\"view_label\"> School Located </ion-label> \r\n\t\t\t<div class=\"text-data\">{{spm.location}} </div>\r\n\t\t\t</ion-item>\r\n\t\t\t</ion-card-content>\r\n\t\t\t<ion-card-content>\r\n\t\t\t<ion-item class=\"textField\">\r\n\t\t\t<ion-label class =\"view_label\"> School Address </ion-label> \r\n\t\t\t<div class=\"text-data\">{{this.spm.address}}</div>\r\n\t\t\t</ion-item>\r\n\t\t\t</ion-card-content>\r\n\t\t\t<ion-card-content>\r\n\t\t\t<ion-item class=\"textField\">\r\n\t\t\t<ion-label class =\"view_label\"> School Cluster </ion-label>\r\n\t\t\t<div class=\"text-data\">{{this.spm.schoolCluster}}</div>\r\n\t\t\t</ion-item>\r\n\t\t\t</ion-card-content>\r\n\t\t\t<ion-card-content>\r\n\t\t\t<ion-item class=\"textField\">\r\n\t\t\t<ion-label class =\"view_label\">School Block </ion-label> \r\n\t\t\t<div class=\"text-data\">{{this.spm.block}}</div></ion-item>\r\n\t\t\t</ion-card-content>\r\n\t\t\t<ion-card-content>\r\n\t\t\t<ion-item class=\"textField\">\r\n\t\t\t<ion-label class =\"view_label\"> District </ion-label>\r\n\t\t\t<div class=\"text-data\">{{this.spm.district}}</div></ion-item>\r\n\t\t\t</ion-card-content>\r\n\t\t\t<ion-card-content>\r\n\t\t\t<ion-item class=\"textField\">\r\n\t\t\t<ion-label class =\"view_label\"> State </ion-label> \r\n\t\t\t<div class=\"text-data\">{{this.spm.state}}</div></ion-item>\r\n\t\t\t</ion-card-content>\r\n\t\t\t<ion-card-content>\r\n\t\t\t<ion-item class=\"textField\">\r\n\t\t\t<ion-label class =\"view_label\"> School Category </ion-label>\r\n\t\t\t<div class=\"text-data\">{{this.spm.category}}</div></ion-item>\r\n\t\t\t</ion-card-content>\r\n\t\t\t<ion-card-content>\r\n\t\t\t<ion-item class=\"textField\">\r\n\t\t\t<ion-label class =\"view_label\"> School Type </ion-label>\r\n\t\t\t<div class=\"text-data\">{{this.spm.type}}</div></ion-item>\r\n\t\t\t</ion-card-content>\r\n\t\t\t<ion-card-content>\r\n\t\t\t<ion-item class=\"textField\">\r\n\t\t\t<ion-label class =\"view_label\"> Medium Of Instructions </ion-label> \r\n\t\t\t<div class=\"text-data\">{{this.spm.medium}}</div></ion-item>\r\n\t\t\t</ion-card-content>\r\n\t\t\t<ion-card-content>\r\n\t\t\t<ion-item class=\"textField\">\r\n\t\t\t<ion-label class =\"view_label\"> School DISE Code </ion-label>\r\n\t\t\t<div class=\"text-data\">{{this.spm.diseCode}}</div></ion-item>\r\n\t\t\t</ion-card-content>\r\n\t\t\t<ion-card-content>\r\n\t\t\t<ion-item class=\"textField\">\r\n\t\t\t<ion-label class =\"view_label\"> School E-mail Id </ion-label> \r\n\t\t\t<div class=\"text-data\">{{this.spm.emailId}}</div></ion-item>\r\n\t\t\t</ion-card-content>\r\n\t\t\t<ion-card-content>\r\n\t\t\t<ion-item class=\"textField\">\r\n\t\t\t<ion-label class =\"view_label\"> Total Students </ion-label> \r\n\t\t\t<div class=\"text-data\">{{this.spm.studentCount}}</div></ion-item>\r\n\t\t\t</ion-card-content>\r\n\r\n\t\t\t\r\n\t\t</ion-list>\r\n\r\n\t</div>\r\n\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/school-profile/school-profile.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/school-profile/school-profile.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".textField {\n  --border-radius: 5px;\n  /*--box-shadow: 0px 14px 25px rgba(182, 30, 30, 0.59);*/ }\n\n.textField ion-label {\n  width: 250px; }\n\n.dateLabel {\n  max-width: 200px; }\n\n.radioGroup {\n  width: 100%; }\n\nion-radio {\n  margin-top: 0px; }\n\n.schoolLocatedLabel {\n  margin-top: 25px;\n  margin-left: -10px;\n  margin-right: 20px; }\n\n/*.addressItem{\r\n\theight: 150px;\r\n\twidth: 100%;\r\n}*/\n\n/*ion-textarea{\r\n\theight: 116px;\r\n\twidth:100%;\r\n    background: #c1c4cd;\r\n}\r\n\r\n.item-input.sc-ion-label-md-h, .item-input .sc-ion-label-md-h {\r\n    -ms-flex: initial;\r\n    flex: initial;\r\n    width: 50% !important;\r\n    pointer-events: none;\r\n}*/\n\n/*ion-label{\r\n    padding: 12px;\r\n}\r\n\r\nion-radio{\r\n    margin-left: 20px;\r\n}*/\n\n.headerTextColor {\n  text-color: #DEE2DD; }\n\n.toolbarStyle {\n  background: #3B5999; }\n\n.radioGroupCol {\n  max-width: 60px !important;\n  max-height: 30px !important;\n  margin-top: 18px; }\n\n.label-stacked.sc-ion-label-md-h {\n  font-size: 16px; }\n\n.radioLabel {\n  margin-right: 10px; }\n\n.schoolTypeCol {\n  max-width: 195px !important; }\n\n.saveButton {\n  float: right;\n  width: 100px;\n  height: 30px;\n  font-size: 14px !important;\n  font-weight: 700;\n  padding-left: 10px; }\n\n.saveContinue {\n  float: right;\n  width: 200px;\n  height: 30px;\n  font-size: 14px !important;\n  font-weight: 700;\n  padding-right: 10px; }\n\nion-textarea {\n  padding-top: 10px; }\n\n.card-content-md {\n  padding: 5px 5px;\n  font-size: 14px;\n  line-height: 1.5; }\n\n.view_label {\n  width: 250px;\n  margin-left: 1.25%; }\n\n.text-data {\n  text-align: center; }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2Nob29sLXByb2ZpbGUvQzpcXHZhcnNoYVxcY3dcXGNhcmV3b3Jrc3VpXFxDYXJld29ya3Mvc3JjXFxhcHBcXHNjaG9vbC1wcm9maWxlXFxzY2hvb2wtcHJvZmlsZS5wYWdlLnNjc3MiLCJzcmMvYXBwL3NjaG9vbC1wcm9maWxlL3NjaG9vbC1wcm9maWxlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG9CQUFnQjtFQUNoQix1REFBQSxFQUF3RDs7QUFFNUQ7RUFDSSxZQUFZLEVBQUE7O0FBRWhCO0VBQ0csZ0JBQWdCLEVBQUE7O0FBRW5CO0VBQ0MsV0FBVyxFQUFBOztBQUdaO0VBQ0ksZUFBZSxFQUFBOztBQUduQjtFQUNJLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsa0JBQWtCLEVBQUE7O0FBR3RCOzs7RUNBRTs7QURLRjs7Ozs7Ozs7Ozs7RUNPRTs7QURNRjs7Ozs7O0VDQ0U7O0FET0Q7RUFDRyxtQkFBbUIsRUFBQTs7QUFHdkI7RUFDSSxtQkFBbUIsRUFBQTs7QUFHdkI7RUFDSSwwQkFBMEI7RUFDMUIsMkJBQTJCO0VBQzNCLGdCQUFnQixFQUFBOztBQUdwQjtFQUNHLGVBQWUsRUFBQTs7QUFHbEI7RUFDSSxrQkFBa0IsRUFBQTs7QUFHdEI7RUFDSSwyQkFBMkIsRUFBQTs7QUFHL0I7RUFFSSxZQUFZO0VBQ1osWUFBWTtFQUNaLFlBQVk7RUFDWiwwQkFBMEI7RUFDMUIsZ0JBQWdCO0VBQ2hCLGtCQUFpQixFQUFBOztBQUVyQjtFQUVBLFlBQWE7RUFDYixZQUFhO0VBQ2IsWUFBYTtFQUNiLDBCQUEyQjtFQUMzQixnQkFBZ0I7RUFDaEIsbUJBQW1CLEVBQUE7O0FBR25CO0VBQ0ksaUJBQWlCLEVBQUE7O0FBR3JCO0VBQ0ksZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixnQkFBZ0IsRUFBQTs7QUFHcEI7RUFDSSxZQUFZO0VBQ1osa0JBQWtCLEVBQUE7O0FBRXRCO0VBRUEsa0JBQWlCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9zY2hvb2wtcHJvZmlsZS9zY2hvb2wtcHJvZmlsZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIudGV4dEZpZWxke1xyXG4gICAgLS1ib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICAvKi0tYm94LXNoYWRvdzogMHB4IDE0cHggMjVweCByZ2JhKDE4MiwgMzAsIDMwLCAwLjU5KTsqL1xyXG59XHJcbi50ZXh0RmllbGQgaW9uLWxhYmVse1xyXG4gICAgd2lkdGg6IDI1MHB4O1xyXG59XHJcbi5kYXRlTGFiZWx7XHJcbiAgIG1heC13aWR0aDogMjAwcHg7IFxyXG59XHJcbi5yYWRpb0dyb3Vwe1xyXG5cdHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5pb24tcmFkaW97XHJcbiAgICBtYXJnaW4tdG9wOiAwcHg7XHJcbn1cclxuXHJcbi5zY2hvb2xMb2NhdGVkTGFiZWx7XHJcbiAgICBtYXJnaW4tdG9wOiAyNXB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xyXG59XHJcblxyXG4vKi5hZGRyZXNzSXRlbXtcclxuXHRoZWlnaHQ6IDE1MHB4O1xyXG5cdHdpZHRoOiAxMDAlO1xyXG59Ki9cclxuXHJcbi8qaW9uLXRleHRhcmVhe1xyXG5cdGhlaWdodDogMTE2cHg7XHJcblx0d2lkdGg6MTAwJTtcclxuICAgIGJhY2tncm91bmQ6ICNjMWM0Y2Q7XHJcbn1cclxuXHJcbi5pdGVtLWlucHV0LnNjLWlvbi1sYWJlbC1tZC1oLCAuaXRlbS1pbnB1dCAuc2MtaW9uLWxhYmVsLW1kLWgge1xyXG4gICAgLW1zLWZsZXg6IGluaXRpYWw7XHJcbiAgICBmbGV4OiBpbml0aWFsO1xyXG4gICAgd2lkdGg6IDUwJSAhaW1wb3J0YW50O1xyXG4gICAgcG9pbnRlci1ldmVudHM6IG5vbmU7XHJcbn0qL1xyXG5cclxuLyppb24tbGFiZWx7XHJcbiAgICBwYWRkaW5nOiAxMnB4O1xyXG59XHJcblxyXG5pb24tcmFkaW97XHJcbiAgICBtYXJnaW4tbGVmdDogMjBweDtcclxufSovXHJcblxyXG4gLmhlYWRlclRleHRDb2xvcntcclxuICAgIHRleHQtY29sb3I6ICNERUUyREQ7XHJcbiB9XHJcblxyXG4udG9vbGJhclN0eWxle1xyXG4gICAgYmFja2dyb3VuZDogIzNCNTk5OTtcclxufVxyXG5cclxuLnJhZGlvR3JvdXBDb2x7XHJcbiAgICBtYXgtd2lkdGg6IDYwcHggIWltcG9ydGFudDtcclxuICAgIG1heC1oZWlnaHQ6IDMwcHggIWltcG9ydGFudDtcclxuICAgIG1hcmdpbi10b3A6IDE4cHg7XHJcbn1cclxuXHJcbi5sYWJlbC1zdGFja2VkLnNjLWlvbi1sYWJlbC1tZC1oIHtcclxuICAgZm9udC1zaXplOiAxNnB4O1xyXG59XHJcblxyXG4ucmFkaW9MYWJlbHtcclxuICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxufVxyXG5cclxuLnNjaG9vbFR5cGVDb2x7XHJcbiAgICBtYXgtd2lkdGg6IDE5NXB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5zYXZlQnV0dG9ue1xyXG4gICAgXHJcbiAgICBmbG9hdDogcmlnaHQ7XHJcbiAgICB3aWR0aDogMTAwcHg7XHJcbiAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICBmb250LXNpemU6IDE0cHggIWltcG9ydGFudDtcclxuICAgIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgICBwYWRkaW5nLWxlZnQ6MTBweDtcclxufVxyXG4uc2F2ZUNvbnRpbnVle1xyXG5cclxuZmxvYXQgOiByaWdodDtcclxud2lkdGggOiAyMDBweDtcclxuaGVpZ2h0IDogMzBweDtcclxuZm9udC1zaXplIDogMTRweCAhaW1wb3J0YW50O1xyXG5mb250LXdlaWdodCA6NzAwO1xyXG5wYWRkaW5nLXJpZ2h0IDoxMHB4O1xyXG59XHJcblxyXG5pb24tdGV4dGFyZWF7XHJcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcclxufVxyXG5cclxuLmNhcmQtY29udGVudC1tZCB7XHJcbiAgICBwYWRkaW5nOiA1cHggNXB4O1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDEuNTtcclxufVxyXG5cclxuLnZpZXdfbGFiZWx7XHJcbiAgICB3aWR0aDogMjUwcHg7XHJcbiAgICBtYXJnaW4tbGVmdDogMS4yNSU7XHJcbn1cclxuLnRleHQtZGF0YVxyXG57XHJcbnRleHQtYWxpZ246Y2VudGVyO1xyXG59XHJcbiIsIi50ZXh0RmllbGQge1xuICAtLWJvcmRlci1yYWRpdXM6IDVweDtcbiAgLyotLWJveC1zaGFkb3c6IDBweCAxNHB4IDI1cHggcmdiYSgxODIsIDMwLCAzMCwgMC41OSk7Ki8gfVxuXG4udGV4dEZpZWxkIGlvbi1sYWJlbCB7XG4gIHdpZHRoOiAyNTBweDsgfVxuXG4uZGF0ZUxhYmVsIHtcbiAgbWF4LXdpZHRoOiAyMDBweDsgfVxuXG4ucmFkaW9Hcm91cCB7XG4gIHdpZHRoOiAxMDAlOyB9XG5cbmlvbi1yYWRpbyB7XG4gIG1hcmdpbi10b3A6IDBweDsgfVxuXG4uc2Nob29sTG9jYXRlZExhYmVsIHtcbiAgbWFyZ2luLXRvcDogMjVweDtcbiAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xuICBtYXJnaW4tcmlnaHQ6IDIwcHg7IH1cblxuLyouYWRkcmVzc0l0ZW17XHJcblx0aGVpZ2h0OiAxNTBweDtcclxuXHR3aWR0aDogMTAwJTtcclxufSovXG4vKmlvbi10ZXh0YXJlYXtcclxuXHRoZWlnaHQ6IDExNnB4O1xyXG5cdHdpZHRoOjEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kOiAjYzFjNGNkO1xyXG59XHJcblxyXG4uaXRlbS1pbnB1dC5zYy1pb24tbGFiZWwtbWQtaCwgLml0ZW0taW5wdXQgLnNjLWlvbi1sYWJlbC1tZC1oIHtcclxuICAgIC1tcy1mbGV4OiBpbml0aWFsO1xyXG4gICAgZmxleDogaW5pdGlhbDtcclxuICAgIHdpZHRoOiA1MCUgIWltcG9ydGFudDtcclxuICAgIHBvaW50ZXItZXZlbnRzOiBub25lO1xyXG59Ki9cbi8qaW9uLWxhYmVse1xyXG4gICAgcGFkZGluZzogMTJweDtcclxufVxyXG5cclxuaW9uLXJhZGlve1xyXG4gICAgbWFyZ2luLWxlZnQ6IDIwcHg7XHJcbn0qL1xuLmhlYWRlclRleHRDb2xvciB7XG4gIHRleHQtY29sb3I6ICNERUUyREQ7IH1cblxuLnRvb2xiYXJTdHlsZSB7XG4gIGJhY2tncm91bmQ6ICMzQjU5OTk7IH1cblxuLnJhZGlvR3JvdXBDb2wge1xuICBtYXgtd2lkdGg6IDYwcHggIWltcG9ydGFudDtcbiAgbWF4LWhlaWdodDogMzBweCAhaW1wb3J0YW50O1xuICBtYXJnaW4tdG9wOiAxOHB4OyB9XG5cbi5sYWJlbC1zdGFja2VkLnNjLWlvbi1sYWJlbC1tZC1oIHtcbiAgZm9udC1zaXplOiAxNnB4OyB9XG5cbi5yYWRpb0xhYmVsIHtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4OyB9XG5cbi5zY2hvb2xUeXBlQ29sIHtcbiAgbWF4LXdpZHRoOiAxOTVweCAhaW1wb3J0YW50OyB9XG5cbi5zYXZlQnV0dG9uIHtcbiAgZmxvYXQ6IHJpZ2h0O1xuICB3aWR0aDogMTAwcHg7XG4gIGhlaWdodDogMzBweDtcbiAgZm9udC1zaXplOiAxNHB4ICFpbXBvcnRhbnQ7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIHBhZGRpbmctbGVmdDogMTBweDsgfVxuXG4uc2F2ZUNvbnRpbnVlIHtcbiAgZmxvYXQ6IHJpZ2h0O1xuICB3aWR0aDogMjAwcHg7XG4gIGhlaWdodDogMzBweDtcbiAgZm9udC1zaXplOiAxNHB4ICFpbXBvcnRhbnQ7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIHBhZGRpbmctcmlnaHQ6IDEwcHg7IH1cblxuaW9uLXRleHRhcmVhIHtcbiAgcGFkZGluZy10b3A6IDEwcHg7IH1cblxuLmNhcmQtY29udGVudC1tZCB7XG4gIHBhZGRpbmc6IDVweCA1cHg7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgbGluZS1oZWlnaHQ6IDEuNTsgfVxuXG4udmlld19sYWJlbCB7XG4gIHdpZHRoOiAyNTBweDtcbiAgbWFyZ2luLWxlZnQ6IDEuMjUlOyB9XG5cbi50ZXh0LWRhdGEge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7IH1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/school-profile/school-profile.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/school-profile/school-profile.page.ts ***!
  \*******************************************************/
/*! exports provided: SchoolProfilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SchoolProfilePage", function() { return SchoolProfilePage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _services_school_profile_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/school-profile.service */ "./src/app/services/school-profile.service.ts");
/* harmony import */ var _services_local_store_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/local-store-service.service */ "./src/app/services/local-store-service.service.ts");
/* harmony import */ var _models_school_profile_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../models/school-profile-model */ "./src/app/models/school-profile-model.ts");
/* harmony import */ var _util_httpWrapper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../util/httpWrapper */ "./src/util/httpWrapper.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








var SchoolProfilePage = /** @class */ (function () {
    function SchoolProfilePage(nav, httpClient, activatedRoute, router, schoolProfileService, loadingController, localStoreService) {
        this.nav = nav;
        this.httpClient = httpClient;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.schoolProfileService = schoolProfileService;
        this.loadingController = loadingController;
        this.localStoreService = localStoreService;
        this.isLoading = false;
        this.schoolDetails = null;
        this.parsedschoolDetailsJson = null;
        this.spm = new _models_school_profile_model__WEBPACK_IMPORTED_MODULE_5__["SchoolProfileModel"](); //this is the getter and setter
        this.shouldHideTable = true;
        console.log(this.shouldHideTable);
    }
    SchoolProfilePage.prototype.ngOnInit = function () {
        var _this = this;
        if (this.localStoreService.isSearchSchoolFlow) {
            this.shouldHideTable = false;
            console.log("issearchflow " + this.localStoreService.isSearchSchoolFlow);
            this.present().then(function (a) { return console.log('presented'); });
            this.schoolId = this.localStoreService.schoolId;
            console.log("schoolId", this.schoolId);
            this.schoolProfileService.getSchoolDetails(this.schoolId).subscribe(function (result) {
                // console.log('Details',result);
                _this.localStoreService.sampleJson = JSON.parse(JSON.stringify(result));
                console.log(_this.localStoreService.sampleJson);
                _this.initialisationTextBox(_this.localStoreService.sampleJson.schoolProfileBasic);
                _this.dismiss().then(function (a) { return console.log('presented'); });
            });
        }
        else {
            this.shouldHideTable = true;
        }
    };
    SchoolProfilePage.prototype.initialisationTextBox = function (parsedJson) {
        this.spm.name = parsedJson.schoolName;
        this.spm.location = parsedJson.location;
        this.spm.establishedYear = parsedJson.establishedDate;
        this.spm.address = parsedJson.schoolAddress;
        this.spm.schoolCluster = parsedJson.schoolCluster;
        this.spm.block = parsedJson.block;
        this.spm.district = parsedJson.district;
        this.spm.state = parsedJson.state;
        if (parsedJson.category != null) {
            this.spm.category = parsedJson.category;
        }
        if (parsedJson.type != null) {
            this.spm.type = parsedJson.type;
        }
        this.spm.medium = parsedJson.medium;
        this.spm.diseCode = parsedJson.diseCode;
        this.spm.emailId = parsedJson.emailId;
        this.spm.studentCount = parsedJson.studentCount;
        // save data to local class
        this.localStoreService.schoolProfileBasic = this.spm;
        //this.localStoreService.
        //
    };
    SchoolProfilePage.prototype.editFunc = function () {
        this.shouldHideTable = true;
    };
    SchoolProfilePage.prototype.saveandContinueFunc = function () {
        this.router.navigate(['../school-info/student-profile']);
    };
    SchoolProfilePage.prototype.saveFunc = function () {
        this.httpClient.post('/school/SchoolProfiles/', {
            "block": this.spm.block,
            "category": this.spm.category,
            "diseCode": this.spm.diseCode,
            "district": this.spm.district,
            "emailId": this.spm.emailId,
            "establishedDate": this.spm.establishedYear,
            "location": this.spm.location,
            "medium": [],
            "region": this.spm.district,
            "schoolAddress": this.spm.address,
            "schoolCluster": this.spm.schoolCluster,
            "schoolName": this.spm.name,
            "state": this.spm.state,
            "studentCount": Number(this.spm.studentCount),
            "type": this.spm.type,
            "schoolId": this.schoolId
        }).subscribe(function (data) {
            console.log(data);
        });
        console.log(this.spm.medium);
        this.localStoreService.schoolName = this.spm.name;
        this.shouldHideTable = false;
    };
    SchoolProfilePage.prototype.updateCategoryDescriptioon = function (categoryValue) {
        switch (categoryValue) {
            case 'GLPS': {
                this.spm.category.description = 'Govt. Lower Primary School (1-5th)';
                break;
            }
            case 'GHPS': {
                this.spm.category.description = 'Govt. Higer Primary School (6-8th)';
                break;
            }
            case 'GMPS': {
                this.spm.category.description = 'Need to check with Mahak';
                break;
            }
            case 'GHS': {
                this.spm.category.description = 'Need to check with Mahak';
                break;
            }
            default: {
                this.spm.category.description = '';
                break;
            }
        }
    };
    SchoolProfilePage.prototype.present = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.isLoading = true;
                        return [4 /*yield*/, this.loadingController.create({
                                message: 'Please wait. . .',
                                duration: 5000,
                            }).then(function (a) {
                                a.present().then(function () {
                                    console.log('presented');
                                    if (!_this.isLoading) {
                                        a.dismiss().then(function () { return console.log('abort presenting'); });
                                    }
                                });
                            })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    SchoolProfilePage.prototype.dismiss = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.isLoading = false;
                        return [4 /*yield*/, this.loadingController.dismiss().then(function () { return console.log('dismissed'); })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    SchoolProfilePage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-school-profile',
            template: __webpack_require__(/*! ./school-profile.page.html */ "./src/app/school-profile/school-profile.page.html"),
            styles: [__webpack_require__(/*! ./school-profile.page.scss */ "./src/app/school-profile/school-profile.page.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _util_httpWrapper__WEBPACK_IMPORTED_MODULE_6__["HttpWrapper"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _services_school_profile_service__WEBPACK_IMPORTED_MODULE_3__["SchoolProfileService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _services_local_store_service_service__WEBPACK_IMPORTED_MODULE_4__["LocalStoreServiceService"]])
    ], SchoolProfilePage);
    return SchoolProfilePage;
}());



/***/ })

}]);
//# sourceMappingURL=school-profile-school-profile-module.js.map