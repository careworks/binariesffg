(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["other-info-other-info-module"],{

/***/ "./src/app/models/other-info-model.ts":
/*!********************************************!*\
  !*** ./src/app/models/other-info-model.ts ***!
  \********************************************/
/*! exports provided: OtherInfoModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OtherInfoModel", function() { return OtherInfoModel; });
var OtherInfoModel = /** @class */ (function () {
    function OtherInfoModel() {
    }
    return OtherInfoModel;
}());



/***/ }),

/***/ "./src/app/other-info/other-info.module.ts":
/*!*************************************************!*\
  !*** ./src/app/other-info/other-info.module.ts ***!
  \*************************************************/
/*! exports provided: OtherInfoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OtherInfoPageModule", function() { return OtherInfoPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _other_info_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./other-info.page */ "./src/app/other-info/other-info.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _other_info_page__WEBPACK_IMPORTED_MODULE_5__["OtherInfoPage"]
    }
];
var OtherInfoPageModule = /** @class */ (function () {
    function OtherInfoPageModule() {
    }
    OtherInfoPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_other_info_page__WEBPACK_IMPORTED_MODULE_5__["OtherInfoPage"]]
        })
    ], OtherInfoPageModule);
    return OtherInfoPageModule;
}());



/***/ }),

/***/ "./src/app/other-info/other-info.page.html":
/*!*************************************************!*\
  !*** ./src/app/other-info/other-info.page.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>otherInfo</ion-title>\r\n  </ion-toolbar>\r\n</ion-header-->\r\n\r\n<ion-content padding>\r\n<div [hidden]=\"shouldHideTable\">\r\n<ion-item class=\"textField\">\r\n\t<ion-button class=\"saveButton\" (click)=\"editFunc()\" block>Edit</ion-button>\r\n\t</ion-item>\r\n<ion-item class=\"otherInfoItem\">\r\n\t  <ion-label position=\"stacked\" class=\"otherlabel\">Any Other Information </ion-label>\r\n\t  <ion-textarea [ngModel] =\"otherInfo\"  name = \"otherInfo\" class=\"otherinfo\"\r\n\t    value=\"\">\r\n\t  </ion-textarea>\r\n</ion-item>\r\n</div>\r\n<div [hidden]=\"!shouldHideTable\">\r\n<ion-item class=\"textField\">\r\n\t<ion-button class=\"saveButton\" (click)=\"saveFunc()\" block>Submit</ion-button>\r\n\t</ion-item>\r\n<ion-item class=\"otherInfoItem\">\r\n\t  <ion-label position=\"stacked\" class=\"otherlabel\">Any Other Information </ion-label>\r\n\t  <ion-textarea [ngModel] =\"otherInfo\"  name = \"otherInfo\" class=\"otherinfo\"\r\n\t    value=\"\">\r\n\t  </ion-textarea>\r\n</ion-item>\r\n</div>\r\n</ion-content>\r\n\r\n"

/***/ }),

/***/ "./src/app/other-info/other-info.page.scss":
/*!*************************************************!*\
  !*** ./src/app/other-info/other-info.page.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".otherInfoItem {\n  height: 150px;\n  width: 100%; }\n\nion-textarea {\n  height: 116px;\n  width: 100%;\n  background: #c1c4cd; }\n\n.headerTextColor {\n  text-color: #DEE2DD; }\n\n.toolbarStyle {\n  background: #3B5999; }\n\nion-label {\n  padding: 12px; }\n\n.otherlabel {\n  font-size: 1.2em; }\n\n.saveButton {\n  float: right;\n  width: 100px;\n  height: 30px;\n  font-size: 14px !important;\n  font-weight: 700; }\n\n.textField {\n  --border-radius: 5px;\n  /*--box-shadow: 0px 14px 25px rgba(182, 30, 30, 0.59);*/ }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvb3RoZXItaW5mby9DOlxcdmFyc2hhXFxjd1xcY2FyZXdvcmtzdWlcXENhcmV3b3Jrcy9zcmNcXGFwcFxcb3RoZXItaW5mb1xcb3RoZXItaW5mby5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyxhQUFhO0VBQ2IsV0FBVyxFQUFBOztBQUdaO0VBQ0MsYUFBYTtFQUNiLFdBQVU7RUFDUCxtQkFBbUIsRUFBQTs7QUFHdEI7RUFDRyxtQkFBbUIsRUFBQTs7QUFHdkI7RUFDSSxtQkFBbUIsRUFBQTs7QUFFdkI7RUFDSSxhQUFhLEVBQUE7O0FBRWhCO0VBQ0EsZ0JBQWdCLEVBQUE7O0FBRWpCO0VBRUksWUFBWTtFQUNaLFlBQVk7RUFDWixZQUFZO0VBQ1osMEJBQTBCO0VBQzFCLGdCQUFnQixFQUFBOztBQUdwQjtFQUNJLG9CQUFnQjtFQUNoQix1REFBQSxFQUF3RCIsImZpbGUiOiJzcmMvYXBwL290aGVyLWluZm8vb3RoZXItaW5mby5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIub3RoZXJJbmZvSXRlbXtcclxuXHRoZWlnaHQ6IDE1MHB4O1xyXG5cdHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5pb24tdGV4dGFyZWF7XHJcblx0aGVpZ2h0OiAxMTZweDtcclxuXHR3aWR0aDoxMDAlO1xyXG4gICAgYmFja2dyb3VuZDogI2MxYzRjZDtcclxufVxyXG5cclxuIC5oZWFkZXJUZXh0Q29sb3J7XHJcbiAgICB0ZXh0LWNvbG9yOiAjREVFMkREO1xyXG4gfVxyXG5cclxuLnRvb2xiYXJTdHlsZXtcclxuICAgIGJhY2tncm91bmQ6ICMzQjU5OTk7XHJcbn1cclxuaW9uLWxhYmVse1xyXG4gICAgcGFkZGluZzogMTJweDtcclxufVxyXG4gLm90aGVybGFiZWx7XHJcblx0Zm9udC1zaXplOiAxLjJlbTtcclxufVxyXG4uc2F2ZUJ1dHRvbntcclxuICAgIFxyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgd2lkdGg6IDEwMHB4O1xyXG4gICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgZm9udC1zaXplOiAxNHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBmb250LXdlaWdodDogNzAwO1xyXG59XHJcblxyXG4udGV4dEZpZWxke1xyXG4gICAgLS1ib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICAvKi0tYm94LXNoYWRvdzogMHB4IDE0cHggMjVweCByZ2JhKDE4MiwgMzAsIDMwLCAwLjU5KTsqL1xyXG59XHJcblxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/other-info/other-info.page.ts":
/*!***********************************************!*\
  !*** ./src/app/other-info/other-info.page.ts ***!
  \***********************************************/
/*! exports provided: OtherInfoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OtherInfoPage", function() { return OtherInfoPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _services_school_profile_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/school-profile.service */ "./src/app/services/school-profile.service.ts");
/* harmony import */ var _services_local_store_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/local-store-service.service */ "./src/app/services/local-store-service.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _models_other_info_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../models/other-info-model */ "./src/app/models/other-info-model.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var OtherInfoPage = /** @class */ (function () {
    function OtherInfoPage(nav, httpClient, activatedRoute, router, schoolProfileService, loadingController, localStoreService) {
        this.nav = nav;
        this.httpClient = httpClient;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.schoolProfileService = schoolProfileService;
        this.loadingController = loadingController;
        this.localStoreService = localStoreService;
        this.parsedschoolDetailsJson = null;
        this.ot = new _models_other_info_model__WEBPACK_IMPORTED_MODULE_6__["OtherInfoModel"]();
        this.shouldHideTable = true;
        console.log(this.shouldHideTable);
    }
    OtherInfoPage.prototype.initialisationTextBox = function (data) {
        this.ot.otherInfo = data;
        console.log(this.otherInfo);
    };
    OtherInfoPage.prototype.ngOnInit = function () {
        if (this.localStoreService.isSearchSchoolFlow) {
            this.shouldHideTable = false;
            this.data = this.localStoreService.sampleJson.schoolAdvancedProfile.anyotherInfo;
            this.initialisationTextBox(this.data);
        }
        else {
            this.shouldHideTable = true;
        }
    };
    OtherInfoPage.prototype.editFunc = function () {
        this.shouldHideTable = true;
    };
    OtherInfoPage.prototype.saveFunc = function () {
        this.router.navigateByUrl('/school-dashboard');
    };
    OtherInfoPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-other-info',
            template: __webpack_require__(/*! ./other-info.page.html */ "./src/app/other-info/other-info.page.html"),
            styles: [__webpack_require__(/*! ./other-info.page.scss */ "./src/app/other-info/other-info.page.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _services_school_profile_service__WEBPACK_IMPORTED_MODULE_3__["SchoolProfileService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _services_local_store_service_service__WEBPACK_IMPORTED_MODULE_4__["LocalStoreServiceService"]])
    ], OtherInfoPage);
    return OtherInfoPage;
}());



/***/ })

}]);
//# sourceMappingURL=other-info-other-info-module.js.map