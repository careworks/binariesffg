(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["student-profile-student-profile-module"],{

/***/ "./src/app/models/student-profile-model.ts":
/*!*************************************************!*\
  !*** ./src/app/models/student-profile-model.ts ***!
  \*************************************************/
/*! exports provided: StudentProfileModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentProfileModel", function() { return StudentProfileModel; });
var StudentProfileModel = /** @class */ (function () {
    function StudentProfileModel() {
    }
    return StudentProfileModel;
}());



/***/ }),

/***/ "./src/app/student-profile/caste-select/caste-select.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/student-profile/caste-select/caste-select.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-item>\r\n    <ion-label>SC</ion-label>\r\n<ion-radio slot=\"start\" value=\"biff\" checked></ion-radio>\r\n\r\n</ion-item>\r\n<ion-item>\r\n    <ion-label>ST</ion-label>\r\n<ion-radio slot=\"start\" value=\"biff\" ></ion-radio>\r\n\r\n</ion-item>\r\n<ion-item>\r\n    <ion-label>OBC</ion-label>\r\n<ion-radio slot=\"start\" value=\"biff\" ></ion-radio>\r\n\r\n</ion-item>\r\n<ion-item>\r\n    <ion-label>XYZ</ion-label>\r\n<ion-radio slot=\"start\" value=\"biff\" ></ion-radio>\r\n\r\n</ion-item>\r\n<ion-item>\r\n<ion-label>Boys #</ion-label>\r\n<ion-input placeholder=\"Enter Input\"></ion-input>\r\n</ion-item>\r\n<ion-item>\r\n<ion-label>Girls #</ion-label>\r\n<ion-input placeholder=\"Enter Input\"></ion-input>\r\n</ion-item>\r\n"

/***/ }),

/***/ "./src/app/student-profile/caste-select/caste-select.component.scss":
/*!**************************************************************************!*\
  !*** ./src/app/student-profile/caste-select/caste-select.component.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N0dWRlbnQtcHJvZmlsZS9jYXN0ZS1zZWxlY3QvY2FzdGUtc2VsZWN0LmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/student-profile/caste-select/caste-select.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/student-profile/caste-select/caste-select.component.ts ***!
  \************************************************************************/
/*! exports provided: CasteSelectComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CasteSelectComponent", function() { return CasteSelectComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CasteSelectComponent = /** @class */ (function () {
    function CasteSelectComponent() {
        this.onDelete = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    CasteSelectComponent.prototype.ngOnInit = function () {
        this.data.id = new Date().getTime();
    };
    CasteSelectComponent.prototype.deleteClicked = function () {
        console.log("deleteClicked");
        this.onDelete.next(this.data);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], CasteSelectComponent.prototype, "data", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], CasteSelectComponent.prototype, "onDelete", void 0);
    CasteSelectComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-caste-select',
            template: __webpack_require__(/*! ./caste-select.component.html */ "./src/app/student-profile/caste-select/caste-select.component.html"),
            styles: [__webpack_require__(/*! ./caste-select.component.scss */ "./src/app/student-profile/caste-select/caste-select.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], CasteSelectComponent);
    return CasteSelectComponent;
}());



/***/ }),

/***/ "./src/app/student-profile/input-row/input-row.component.html":
/*!********************************************************************!*\
  !*** ./src/app/student-profile/input-row/input-row.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<ion-button color=\"danger\"  (click)=\"deleteClicked(item)\">Delete</ion-button>\r\n\r\n\r\n  <ion-item>\r\n    <ion-label>Section</ion-label>\r\n    <ion-input placeholder=\"Enter Input\"></ion-input>\r\n  </ion-item>\r\n  <ion-item>\r\n    <ion-label>Total Number of Boys</ion-label>\r\n    <ion-input placeholder=\"Enter Input\"></ion-input>\r\n  </ion-item>\r\n  <ion-item>\r\n    <ion-label>Total Number of Girls</ion-label>\r\n    <ion-input placeholder=\"Enter Input\"></ion-input>\r\n  </ion-item>\r\n  \r\n  <ion-button (click)=\"inClassAddRows()\" round>Add</ion-button>\r\n  <ion-list>\r\n      <div *ngFor=\"let i of inButtonInputRowValue; let ii=index\"  style=\"font-size:smaller\">\r\n  \r\n        <app-caste-select [data]=\"i\" ></app-caste-select>\r\n        <br>\r\n      </div>\r\n    </ion-list>\r\n  \r\n    "

/***/ }),

/***/ "./src/app/student-profile/input-row/input-row.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/student-profile/input-row/input-row.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N0dWRlbnQtcHJvZmlsZS9pbnB1dC1yb3cvaW5wdXQtcm93LmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/student-profile/input-row/input-row.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/student-profile/input-row/input-row.component.ts ***!
  \******************************************************************/
/*! exports provided: InputRowComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InputRowComponent", function() { return InputRowComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// IMPORT SUB COMPONENT
var InputRowComponent = /** @class */ (function () {
    function InputRowComponent() {
        this.inButtonInputRowValue = [{}];
        this.onDelete = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    InputRowComponent.prototype.inClassAddRows = function () {
        this.inButtonInputRowValue.push({});
    };
    InputRowComponent.prototype.ngOnInit = function () {
        this.data.id = new Date().getTime();
    };
    InputRowComponent.prototype.deleteClicked = function () {
        console.log("deleteClicked");
        this.onDelete.next(this.data);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], InputRowComponent.prototype, "data", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], InputRowComponent.prototype, "onDelete", void 0);
    InputRowComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-input-row',
            template: __webpack_require__(/*! ./input-row.component.html */ "./src/app/student-profile/input-row/input-row.component.html"),
            styles: [__webpack_require__(/*! ./input-row.component.scss */ "./src/app/student-profile/input-row/input-row.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], InputRowComponent);
    return InputRowComponent;
}());



/***/ }),

/***/ "./src/app/student-profile/student-profile.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/student-profile/student-profile.module.ts ***!
  \***********************************************************/
/*! exports provided: StudentProfilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentProfilePageModule", function() { return StudentProfilePageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _student_profile_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./student-profile.page */ "./src/app/student-profile/student-profile.page.ts");
/* harmony import */ var _input_row_input_row_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./input-row/input-row.component */ "./src/app/student-profile/input-row/input-row.component.ts");
/* harmony import */ var _caste_select_caste_select_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./caste-select/caste-select.component */ "./src/app/student-profile/caste-select/caste-select.component.ts");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/fesm5/ng2-smart-table.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var routes = [
    {
        path: '',
        component: _student_profile_page__WEBPACK_IMPORTED_MODULE_5__["StudentProfilePage"]
    }
];
var StudentProfilePageModule = /** @class */ (function () {
    function StudentProfilePageModule() {
    }
    StudentProfilePageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                ng2_smart_table__WEBPACK_IMPORTED_MODULE_8__["Ng2SmartTableModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_student_profile_page__WEBPACK_IMPORTED_MODULE_5__["StudentProfilePage"], _input_row_input_row_component__WEBPACK_IMPORTED_MODULE_6__["InputRowComponent"], _caste_select_caste_select_component__WEBPACK_IMPORTED_MODULE_7__["CasteSelectComponent"]]
        })
    ], StudentProfilePageModule);
    return StudentProfilePageModule;
}());



/***/ }),

/***/ "./src/app/student-profile/student-profile.page.html":
/*!***********************************************************!*\
  !*** ./src/app/student-profile/student-profile.page.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<ion-content padding>\r\n<br>\r\n<div [hidden]=\"shouldHideTable\">\r\n<br>\r\n<ion-item>\r\n<ion-button class=\"saveButton\" (click)=\"editFunc()\" block>Edit</ion-button>\r\n</ion-item>\r\n <H3>Medium: Kannada</H3>\r\n<ng2-smart-table [settings]=\"settings1\" [source]=\"data\"></ng2-smart-table>\r\n<br>\r\n<H3>Medium: English</H3>\r\n<ng2-smart-table [settings]=\"settings1\" [source]=\"data1\"></ng2-smart-table>\r\n</div>\r\n\r\n\r\n<div [hidden] = \"!shouldHideTable\">\r\n<br>\r\n<ion-item>\r\n<ion-button class=\"saveContinue\" (click)=\"saveandContinueFunc()\" block>Save and Continue</ion-button>\r\n<ion-button class=\"saveButton\" (click)=\"saveStudents()\" block>Save</ion-button>\r\n</ion-item>\r\n  <H3>Medium: Kannada</H3>\r\n<ng2-smart-table [settings]=\"settings\" [source]=\"data\" (deleteConfirm)=\"onDeleteKannadaConfirm($event)\" (editConfirm)=\"onSaveKannadaConfirm($event)\"\r\n (createConfirm)=\"onCreateKannadaConfirm($event)\"></ng2-smart-table>\r\n<br>\r\n<H3>Medium: English</H3>\r\n<ng2-smart-table [settings]=\"settings\" [source]=\"data1\" (deleteConfirm)=\"onDeleteEnglishConfirm($event)\" (editConfirm)=\"onSaveEnglishConfirm($event)\"\r\n (createConfirm)=\"onCreateEnglishConfirm($event)\"></ng2-smart-table>\r\n</div>\r\n</ion-content>\r\n\r\n"

/***/ }),

/***/ "./src/app/student-profile/student-profile.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/student-profile/student-profile.page.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/*\r\n.view {\r\ndisplay : none !important;\r\n}\r\ntable { \r\n\t\twidth: 100%; \r\n\t\tborder-collapse: collapse; \r\n\t}\r\n\t/* Zebra striping \r\n\ttr:nth-of-type(odd) { \r\n\t\tbackground: #eee; \r\n\t}\r\n\tth { \r\n\t\tbackground: #333; \r\n\t\tcolor: white; \r\n\t\tfont-weight: bold; \r\n\t}\r\n\ttd, th { \r\n\t\tpadding: 6px; \r\n\t\tborder: 1px solid #ccc; \r\n\t\ttext-align: left; \r\n\t}\r\n.textField{\r\n    --border-radius: 5px;\r\n    --box-shadow: 0px 14px 25px rgba(182, 30, 30, 0.59);\r\n}\r\n\r\nion-label{\r\n\twidth: 200px;\r\n}\r\n*/\n:host /deep/ ng2-smart-table table {\n  border-style: inset;\n  border-color: initial;\n  -webkit-border-image: initial;\n       -o-border-image: initial;\n          border-image: initial;\n  border-width: 2px;\n  display: table;\n  border-spacing: 2px;\n  border-color: grey; }\n:host /deep/ ng2-smart-table table > tbody > tr > td {\n  box-sizing: border-box;\n  border: 1px solid grey; }\n:host /deep/ ng2-smart-table thead > tr > th {\n  border: 1px solid grey; }\n:host /deep/ ng2-smart-table {\n  font-size: 16px; }\n.saveButton {\n  float: right;\n  width: 100px;\n  height: 30px;\n  font-size: 14px !important;\n  font-weight: 700; }\n.textField {\n  --border-radius: 5px;\n  /*--box-shadow: 0px 14px 25px rgba(182, 30, 30, 0.59);*/ }\n.saveContinue {\n  float: right;\n  width: 200px;\n  height: 30px;\n  font-size: 14px !important;\n  font-weight: 700;\n  padding-right: 10px; }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3R1ZGVudC1wcm9maWxlL0M6XFx2YXJzaGFcXGN3XFxjYXJld29ya3N1aVxcQ2FyZXdvcmtzL3NyY1xcYXBwXFxzdHVkZW50LXByb2ZpbGVcXHN0dWRlbnQtcHJvZmlsZS5wYWdlLnNjc3MiLCJzcmMvYXBwL3N0dWRlbnQtcHJvZmlsZS9zdHVkZW50LXByb2ZpbGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Q0M4QkM7QURFRDtFQUNJLG1CQUFtQjtFQUNuQixxQkFBcUI7RUFDckIsNkJBQXFCO09BQXJCLHdCQUFxQjtVQUFyQixxQkFBcUI7RUFDckIsaUJBQWlCO0VBRWpCLGNBQWM7RUFDZCxtQkFBbUI7RUFDbkIsa0JBQWtCLEVBQUE7QUFHdEI7RUFDSSxzQkFBc0I7RUFDdEIsc0JBQXNCLEVBQUE7QUFHMUI7RUFDSSxzQkFBc0IsRUFBQTtBQUUxQjtFQUNJLGVBQWMsRUFBQTtBQUdsQjtFQUVJLFlBQVk7RUFDWixZQUFZO0VBQ1osWUFBWTtFQUNaLDBCQUEwQjtFQUMxQixnQkFBZ0IsRUFBQTtBQUVwQjtFQUNJLG9CQUFnQjtFQUNoQix1REFBQSxFQUF3RDtBQUc1RDtFQUVBLFlBQWE7RUFDYixZQUFhO0VBQ2IsWUFBYTtFQUNiLDBCQUEyQjtFQUMzQixnQkFBZ0I7RUFDaEIsbUJBQW1CLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9zdHVkZW50LXByb2ZpbGUvc3R1ZGVudC1wcm9maWxlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbi52aWV3IHtcclxuZGlzcGxheSA6IG5vbmUgIWltcG9ydGFudDtcclxufVxyXG50YWJsZSB7IFxyXG5cdFx0d2lkdGg6IDEwMCU7IFxyXG5cdFx0Ym9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTsgXHJcblx0fVxyXG5cdC8qIFplYnJhIHN0cmlwaW5nIFxyXG5cdHRyOm50aC1vZi10eXBlKG9kZCkgeyBcclxuXHRcdGJhY2tncm91bmQ6ICNlZWU7IFxyXG5cdH1cclxuXHR0aCB7IFxyXG5cdFx0YmFja2dyb3VuZDogIzMzMzsgXHJcblx0XHRjb2xvcjogd2hpdGU7IFxyXG5cdFx0Zm9udC13ZWlnaHQ6IGJvbGQ7IFxyXG5cdH1cclxuXHR0ZCwgdGggeyBcclxuXHRcdHBhZGRpbmc6IDZweDsgXHJcblx0XHRib3JkZXI6IDFweCBzb2xpZCAjY2NjOyBcclxuXHRcdHRleHQtYWxpZ246IGxlZnQ7IFxyXG5cdH1cclxuLnRleHRGaWVsZHtcclxuICAgIC0tYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgLS1ib3gtc2hhZG93OiAwcHggMTRweCAyNXB4IHJnYmEoMTgyLCAzMCwgMzAsIDAuNTkpO1xyXG59XHJcblxyXG5pb24tbGFiZWx7XHJcblx0d2lkdGg6IDIwMHB4O1xyXG59XHJcbiovXHJcblxyXG46aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHRhYmxle1xyXG4gICAgYm9yZGVyLXN0eWxlOiBpbnNldDtcclxuICAgIGJvcmRlci1jb2xvcjogaW5pdGlhbDtcclxuICAgIGJvcmRlci1pbWFnZTogaW5pdGlhbDtcclxuICAgIGJvcmRlci13aWR0aDogMnB4O1xyXG4gICAgXHJcbiAgICBkaXNwbGF5OiB0YWJsZTtcclxuICAgIGJvcmRlci1zcGFjaW5nOiAycHg7XHJcbiAgICBib3JkZXItY29sb3I6IGdyZXk7IFxyXG5cclxufVxyXG46aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHRhYmxlID4gdGJvZHkgPiB0ciA+IHRkIHtcclxuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCBncmV5O1xyXG59XHJcblxyXG46aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHRoZWFkID4gdHIgPiB0aCAgeyBcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIGdyZXk7XHJcbn0gIFxyXG46aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHtcclxuICAgIGZvbnQtc2l6ZToxNnB4O1xyXG59XHJcblxyXG4uc2F2ZUJ1dHRvbntcclxuICAgIFxyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgd2lkdGg6IDEwMHB4O1xyXG4gICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgZm9udC1zaXplOiAxNHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBmb250LXdlaWdodDogNzAwO1xyXG59XHJcbi50ZXh0RmllbGR7XHJcbiAgICAtLWJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIC8qLS1ib3gtc2hhZG93OiAwcHggMTRweCAyNXB4IHJnYmEoMTgyLCAzMCwgMzAsIDAuNTkpOyovXHJcbn1cclxuXHJcbi5zYXZlQ29udGludWV7XHJcblxyXG5mbG9hdCA6IHJpZ2h0O1xyXG53aWR0aCA6IDIwMHB4O1xyXG5oZWlnaHQgOiAzMHB4O1xyXG5mb250LXNpemUgOiAxNHB4ICFpbXBvcnRhbnQ7XHJcbmZvbnQtd2VpZ2h0IDo3MDA7XHJcbnBhZGRpbmctcmlnaHQgOjEwcHg7XHJcbn1cclxuXHJcbiIsIi8qXHJcbi52aWV3IHtcclxuZGlzcGxheSA6IG5vbmUgIWltcG9ydGFudDtcclxufVxyXG50YWJsZSB7IFxyXG5cdFx0d2lkdGg6IDEwMCU7IFxyXG5cdFx0Ym9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTsgXHJcblx0fVxyXG5cdC8qIFplYnJhIHN0cmlwaW5nIFxyXG5cdHRyOm50aC1vZi10eXBlKG9kZCkgeyBcclxuXHRcdGJhY2tncm91bmQ6ICNlZWU7IFxyXG5cdH1cclxuXHR0aCB7IFxyXG5cdFx0YmFja2dyb3VuZDogIzMzMzsgXHJcblx0XHRjb2xvcjogd2hpdGU7IFxyXG5cdFx0Zm9udC13ZWlnaHQ6IGJvbGQ7IFxyXG5cdH1cclxuXHR0ZCwgdGggeyBcclxuXHRcdHBhZGRpbmc6IDZweDsgXHJcblx0XHRib3JkZXI6IDFweCBzb2xpZCAjY2NjOyBcclxuXHRcdHRleHQtYWxpZ246IGxlZnQ7IFxyXG5cdH1cclxuLnRleHRGaWVsZHtcclxuICAgIC0tYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgLS1ib3gtc2hhZG93OiAwcHggMTRweCAyNXB4IHJnYmEoMTgyLCAzMCwgMzAsIDAuNTkpO1xyXG59XHJcblxyXG5pb24tbGFiZWx7XHJcblx0d2lkdGg6IDIwMHB4O1xyXG59XHJcbiovXG46aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHRhYmxlIHtcbiAgYm9yZGVyLXN0eWxlOiBpbnNldDtcbiAgYm9yZGVyLWNvbG9yOiBpbml0aWFsO1xuICBib3JkZXItaW1hZ2U6IGluaXRpYWw7XG4gIGJvcmRlci13aWR0aDogMnB4O1xuICBkaXNwbGF5OiB0YWJsZTtcbiAgYm9yZGVyLXNwYWNpbmc6IDJweDtcbiAgYm9yZGVyLWNvbG9yOiBncmV5OyB9XG5cbjpob3N0IC9kZWVwLyBuZzItc21hcnQtdGFibGUgdGFibGUgPiB0Ym9keSA+IHRyID4gdGQge1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICBib3JkZXI6IDFweCBzb2xpZCBncmV5OyB9XG5cbjpob3N0IC9kZWVwLyBuZzItc21hcnQtdGFibGUgdGhlYWQgPiB0ciA+IHRoIHtcbiAgYm9yZGVyOiAxcHggc29saWQgZ3JleTsgfVxuXG46aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHtcbiAgZm9udC1zaXplOiAxNnB4OyB9XG5cbi5zYXZlQnV0dG9uIHtcbiAgZmxvYXQ6IHJpZ2h0O1xuICB3aWR0aDogMTAwcHg7XG4gIGhlaWdodDogMzBweDtcbiAgZm9udC1zaXplOiAxNHB4ICFpbXBvcnRhbnQ7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7IH1cblxuLnRleHRGaWVsZCB7XG4gIC0tYm9yZGVyLXJhZGl1czogNXB4O1xuICAvKi0tYm94LXNoYWRvdzogMHB4IDE0cHggMjVweCByZ2JhKDE4MiwgMzAsIDMwLCAwLjU5KTsqLyB9XG5cbi5zYXZlQ29udGludWUge1xuICBmbG9hdDogcmlnaHQ7XG4gIHdpZHRoOiAyMDBweDtcbiAgaGVpZ2h0OiAzMHB4O1xuICBmb250LXNpemU6IDE0cHggIWltcG9ydGFudDtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgcGFkZGluZy1yaWdodDogMTBweDsgfVxuIl19 */"

/***/ }),

/***/ "./src/app/student-profile/student-profile.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/student-profile/student-profile.page.ts ***!
  \*********************************************************/
/*! exports provided: StudentProfilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentProfilePage", function() { return StudentProfilePage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_school_profile_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/school-profile.service */ "./src/app/services/school-profile.service.ts");
/* harmony import */ var _services_local_store_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/local-store-service.service */ "./src/app/services/local-store-service.service.ts");
/* harmony import */ var _models_student_profile_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/student-profile-model */ "./src/app/models/student-profile-model.ts");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/fesm5/ng2-smart-table.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// import { NavController } from 'ionic-angular';





var StudentProfilePage = /** @class */ (function () {
    function StudentProfilePage(schoolProfileService, localStoreService, router) {
        this.schoolProfileService = schoolProfileService;
        this.localStoreService = localStoreService;
        this.router = router;
        // Testing Smart Tables 
        this.settings = {
            defaultStyle: false,
            actions: {
                position: 'right',
            },
            add: {
                addButtonContent: 'Add     ',
                createButtonContent: 'Create    ',
                cancelButtonContent: '    Cancel',
                confirmCreate: true,
            },
            edit: {
                editButtonContent: 'Edit  ',
                saveButtonContent: 'Update    ',
                cancelButtonContent: '    Cancel',
                confirmSave: true,
            },
            delete: {
                deleteButtonContent: '	Delete',
                confirmDelete: true,
            },
            columns: {
                class: {
                    title: 'Class', filter: false
                },
                sectionCount: {
                    title: 'Section count', filter: false
                },
                totalBoys: {
                    title: 'Total Boys', filter: false
                },
                totalGirls: {
                    title: 'Total Girls', filter: false
                },
                scboysNumber: {
                    title: 'SC Boys', filter: false
                }, scgirlsNumber: {
                    title: 'SC Girls', filter: false
                },
                stboysNumber: {
                    title: 'ST Boys', filter: false
                },
                stgirlsNumber: {
                    title: 'ST Girls', filter: false
                }
            },
            attr: {
                class: 'table table-bordered table-striped'
            }
        };
        this.settings1 = {
            defaultStyle: false,
            actions: false,
            hideSubHeader: true,
            columns: {
                class: {
                    title: 'Class', filter: false
                },
                sectionCount: {
                    title: 'Section count', filter: false
                },
                totalBoys: {
                    title: 'Total Boys', filter: false
                },
                totalGirls: {
                    title: 'Total Girls', filter: false
                },
                scboysNumber: {
                    title: 'SC Boys', filter: false
                }, scgirlsNumber: {
                    title: 'SC Girls', filter: false
                },
                stboysNumber: {
                    title: 'ST Boys', filter: false
                },
                stgirlsNumber: {
                    title: 'ST Girls', filter: false
                }
            },
            attr: {
                class: 'table table-bordered table-striped'
            }
        };
        //Testing Ends
        this.inputRowValues = [{}];
        this.obj = {};
        this.studentPM = new _models_student_profile_model__WEBPACK_IMPORTED_MODULE_3__["StudentProfileModel"]();
        this.parsedJson = null;
        this.parsedJson1 = null;
        this.classList = null;
        this.modified = null;
        this.mediumList = null;
        this.casteList = null;
        // this.x = 0;
        this.obj = this.schoolProfileService.getSampleJson();
        this.class_no = 1;
        this.shouldHideTable = true;
    }
    StudentProfilePage.prototype.onDeleteKannadaConfirm = function (event) {
        console.log("Delete Event In Console");
        console.log(event);
        if (window.confirm('Are you sure you want to delete?')) {
            event.confirm.resolve();
        }
        else {
            event.confirm.reject();
        }
    };
    StudentProfilePage.prototype.onCreateKannadaConfirm = function (event) {
        console.log("Create Event In Console");
        console.log(event);
        if (window.confirm('Are you sure you want to add new entry?')) {
            event.confirm.resolve();
        }
        else {
            event.confirm.reject();
        }
    };
    StudentProfilePage.prototype.onSaveKannadaConfirm = function (event) {
        console.log("Edit Event In Console");
        console.log(event);
        if (window.confirm('Are you sure you want to update?')) {
            event.confirm.resolve();
        }
        else {
            event.confirm.reject();
        }
    };
    StudentProfilePage.prototype.onDeleteEnglishConfirm = function (event) {
        console.log("Delete Event In Console");
        console.log(event);
        if (window.confirm('Are you sure you want to delete?')) {
            event.confirm.resolve();
        }
        else {
            event.confirm.reject();
        }
    };
    StudentProfilePage.prototype.onCreateEnglishConfirm = function (event) {
        console.log("Create Event In Console");
        console.log(event);
        if (window.confirm('Are you sure you want to add new entry?')) {
            event.confirm.resolve();
        }
        else {
            event.confirm.reject();
        }
    };
    StudentProfilePage.prototype.onSaveEnglishConfirm = function (event) {
        console.log("Edit Event In Console");
        console.log(event);
        if (window.confirm('Are you sure you want to update?')) {
            event.confirm.resolve();
        }
        else {
            event.confirm.reject();
        }
    };
    StudentProfilePage.prototype.ngOnInit = function () {
        this.data = [];
        this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_4__["LocalDataSource"](this.data);
        // this.modified = this.schoolProfileService.getSampleJson();
        if (this.localStoreService.isSearchSchoolFlow) {
            this.shouldHideTable = false;
            this.data = this.localStoreService.sampleJson.schoolAdvancedProfile.teacherDetails;
            this.source.load(this.data);
        }
        else {
        }
        this.shouldHideTable = true;
        this.data = this.schoolProfileService.getSampleJson().schoolProfileAdvanced.studentDetails[0].details;
        this.data1 = this.schoolProfileService.getSampleJson().schoolProfileAdvanced.studentDetails[1].details;
    };
    StudentProfilePage.prototype.deleteRow = function (rowClass) {
        // alert(row);
        /// delete part not working 
        var i;
        for (i = 0; i < this.modified.schoolProfileAdvanced.studentDetails[1].details.length; i++) {
            if (this.modified.schoolProfileAdvanced.studentDetails[1].details[i].class == rowClass) {
                delete this.modified.schoolProfileAdvanced.studentDetails[1].details[i];
            }
        }
    };
    StudentProfilePage.prototype.addRow = function () {
        this.inputRowValues.push({});
        this.x += 1;
        this.obj.schoolProfileAdvanced.studentDetails[1].details.push({
            "class": this.studentPM.class,
            "sectionCount": this.studentPM.sectionCount,
            "totalBoys": this.studentPM.totalBoys,
            "totalGirls": this.studentPM.totalGirls,
            "scboysNumber": this.studentPM.scboysNumber,
            "scgirlsNumber": this.studentPM.scgirlsNumber,
            "stboysNumber": this.studentPM.stboysNumber,
            "stgirlsNumber": this.studentPM.stgirlsNumber
        });
        console.log(this.obj.schoolProfileAdvanced.studentDetails[1].details);
        this.modified = this.obj.schoolProfileAdvanced.studentDetails[1].details;
        this.class_no += 1;
    };
    StudentProfilePage.prototype.onDelete = function (_event) {
        console.log(_event);
        this.inputRowValues = this.inputRowValues.filter(function (i) { return i.id !== _event.id; });
    };
    StudentProfilePage.prototype.editFunc = function () {
        this.shouldHideTable = true;
    };
    StudentProfilePage.prototype.saveStudents = function () {
        //POST to API
        this.shouldHideTable = false;
    };
    StudentProfilePage.prototype.saveandContinueFunc = function () {
        this.router.navigate(['../school-info/teachers-profile']);
    };
    StudentProfilePage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-student-profile',
            template: __webpack_require__(/*! ./student-profile.page.html */ "./src/app/student-profile/student-profile.page.html"),
            styles: [__webpack_require__(/*! ./student-profile.page.scss */ "./src/app/student-profile/student-profile.page.scss")]
        }),
        __metadata("design:paramtypes", [_services_school_profile_service__WEBPACK_IMPORTED_MODULE_1__["SchoolProfileService"], _services_local_store_service_service__WEBPACK_IMPORTED_MODULE_2__["LocalStoreServiceService"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], StudentProfilePage);
    return StudentProfilePage;
}());



/***/ })

}]);
//# sourceMappingURL=student-profile-student-profile-module.js.map