(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["classroom-environment-classroom-environment-module"],{

/***/ "./src/app/classroom-environment/classroom-environment.config.ts":
/*!***********************************************************************!*\
  !*** ./src/app/classroom-environment/classroom-environment.config.ts ***!
  \***********************************************************************/
/*! exports provided: ClassroomEnvironmentConfig */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClassroomEnvironmentConfig", function() { return ClassroomEnvironmentConfig; });
var ClassroomEnvironmentConfig = /** @class */ (function () {
    function ClassroomEnvironmentConfig() {
    }
    ClassroomEnvironmentConfig.computerLabDetailsConfig = {
        defaultStyle: false,
        actions: {
            position: "right",
            add: false,
            delete: false
        },
        columns: {
            name: {
                title: 'Particulars',
                editable: false,
                filter: false
            },
            available: {
                title: 'Available',
                filter: false
            },
            repair: {
                title: 'Require repair',
                filter: false
            },
            pristine: {
                title: 'New requirement',
                filter: false
            }
        }
    };
    ClassroomEnvironmentConfig.computerLabDetailsConfigReadOnly = {
        defaultStyle: false,
        actions: false,
        columns: {
            name: {
                title: 'Particulars',
                editable: false,
                filter: false
            },
            available: {
                title: 'Available',
                filter: false
            },
            repair: {
                title: 'Require repair',
                filter: false
            },
            pristine: {
                title: 'New requirement',
                filter: false
            }
        }
    };
    ClassroomEnvironmentConfig.computerLabConfig = [
        {
            "name": "Computers",
            "available": 0,
            "pristine": 0,
            "repair": 0
        },
        {
            "name": "Laptops",
            "available": 0,
            "pristine": 0,
            "repair": 0
        },
        {
            "name": "UPS",
            "available": 0,
            "pristine": 0,
            "repair": 0
        },
        {
            "name": "Computer tables",
            "available": 0,
            "pristine": 0,
            "repair": 0
        },
        {
            "name": "Computer chairs",
            "available": 0,
            "pristine": 0,
            "repair": 0
        },
        {
            "name": "Projector",
            "available": 0,
            "pristine": 0,
            "repair": 0
        },
        {
            "name": "Projector Screen",
            "available": 0,
            "pristine": 0,
            "repair": 0
        },
        {
            "name": "Wall Mounted",
            "available": 0,
            "pristine": 0,
            "repair": 0
        }
    ];
    ClassroomEnvironmentConfig.libraryRoomConfig = [
        {
            "name": "Is library arranged level wise",
            "available": 0,
            "pristine": 0,
            "repair": 0,
            "value": ""
        },
        {
            "name": "Is sufficient cupboards available",
            "available": 0,
            "pristine": 0,
            "repair": 0,
            "value": ""
        },
        {
            "name": "Is table available",
            "available": 0,
            "pristine": 0,
            "repair": 0,
            "value": ""
        },
        {
            "name": "Is sufficient chairs/bench available",
            "available": 0,
            "pristine": 0,
            "repair": 0,
            "value": ""
        }
    ];
    ClassroomEnvironmentConfig.scienceLabConfig = [
        {
            "name": "Wall Mounted Table",
            "available": 0,
            "pristine": 0,
            "repair": 0,
            "value": ""
        },
        {
            "name": "Center Table",
            "available": 0,
            "pristine": 0,
            "repair": 0,
            "value": ""
        },
        {
            "name": "Chemicals",
            "available": 0,
            "pristine": 0,
            "repair": 0,
            "value": ""
        },
        {
            "name": "Washbasin",
            "available": 0,
            "pristine": 0,
            "repair": 0,
            "value": ""
        },
        {
            "name": "Models",
            "available": 0,
            "pristine": 0,
            "repair": 0,
            "value": ""
        }
    ];
    return ClassroomEnvironmentConfig;
}());



/***/ }),

/***/ "./src/app/classroom-environment/classroom-environment.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/classroom-environment/classroom-environment.module.ts ***!
  \***********************************************************************/
/*! exports provided: ClassroomEnvironmentPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClassroomEnvironmentPageModule", function() { return ClassroomEnvironmentPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/fesm5/ng2-smart-table.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _classroom_environment_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./classroom-environment.page */ "./src/app/classroom-environment/classroom-environment.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: '',
        component: _classroom_environment_page__WEBPACK_IMPORTED_MODULE_6__["ClassroomEnvironmentPage"]
    }
];
var ClassroomEnvironmentPageModule = /** @class */ (function () {
    function ClassroomEnvironmentPageModule() {
    }
    ClassroomEnvironmentPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
                ng2_smart_table__WEBPACK_IMPORTED_MODULE_4__["Ng2SmartTableModule"]
            ],
            declarations: [_classroom_environment_page__WEBPACK_IMPORTED_MODULE_6__["ClassroomEnvironmentPage"]],
            schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["CUSTOM_ELEMENTS_SCHEMA"]]
        })
    ], ClassroomEnvironmentPageModule);
    return ClassroomEnvironmentPageModule;
}());



/***/ }),

/***/ "./src/app/classroom-environment/classroom-environment.page.html":
/*!***********************************************************************!*\
  !*** ./src/app/classroom-environment/classroom-environment.page.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content padding>\r\n    <ion-button color=\"tertiary\" (click)=\"startEditing()\" *ngIf=\"!hasStarted\">\r\n        <ion-icon name=\"arrow-forward\" padding></ion-icon>\r\n        Begin</ion-button>\r\n     <ion-button color=\"primary\" (click)=\"isEditing = !isEditing\" *ngIf=\"hasStarted && !isEditing\">\r\n       <ion-icon name=\"create\" padding></ion-icon>\r\n       Edit</ion-button>\r\n     <ion-button color=\"secondary\" (click)=\"saveClassRoomEnvironmentDetailsAndContinue()\" *ngIf=\"isEditing\" (click)=\"isEditing = !isEditing\"> \r\n      <ion-icon name=\"save\" padding></ion-icon>\r\n     Save & Continue</ion-button>\r\n     <ion-button color=\"tertiary\" (click)=\"saveClassRoomEnvironmentDetails()\" *ngIf=\"isEditing\" (click)=\"isEditing = !isEditing\"> \r\n      <ion-icon name=\"clipboard\" padding></ion-icon>\r\n     Save</ion-button>\r\n    <div *ngIf=\"hasStarted && !isEditing\">\r\n          <ion-card padding>\r\n              <ion-card-body>\r\n                  <ion-grid>\r\n                      <ion-row>\r\n                          <ion-col>\r\n                              <ion-item>\r\n                                  <ion-label>Is there a seperate room for computer Lab in the school?</ion-label>\r\n                              </ion-item>\r\n                          </ion-col>\r\n                          <ion-col>\r\n                              <ion-item>\r\n                                  <ion-label >{{classroomEnvironment.computerLab}}</ion-label>\r\n                              </ion-item>    \r\n                          </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                          <ion-col>\r\n                              <ion-item>\r\n                                  <ion-label>Is curriculum available?</ion-label>\r\n                              </ion-item>\r\n                          </ion-col>\r\n                          <ion-col>\r\n                              <ion-item>\r\n                                  <ion-label>{{classroomEnvironment.curriculum.has}}</ion-label>\r\n                              </ion-item>\r\n                          </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                          <ion-col>\r\n                              <ion-item>\r\n                                  <ion-label>Provide the details</ion-label>\r\n                              </ion-item>\r\n                          </ion-col>\r\n                          <ion-col>\r\n                              <ion-item>\r\n                                  <ion-label>{{classroomEnvironment.curriculum.remarks}}</ion-label>\r\n                              </ion-item>\r\n                          </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                          <ion-col>\r\n                              <ion-item>\r\n                                  <ion-label>Is e-learning system of teaching available?</ion-label>\r\n                              </ion-item>\r\n                          </ion-col>\r\n                          <ion-col>\r\n                              <ion-item>\r\n                                  <ion-label>{{classroomEnvironment.eLearn.has}}</ion-label>\r\n                              </ion-item>\r\n                          </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                          <ion-col>\r\n                              <ion-item>\r\n                                  <ion-label>Mention the organization who provides the system</ion-label>\r\n                              </ion-item>\r\n                          </ion-col>\r\n                          <ion-col>\r\n                              <ion-item>\r\n                                  <ion-label>{{classroomEnvironment.eLearn.remarks}}</ion-label>\r\n                              </ion-item>\r\n                          </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>ComputerLab Details</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                          </ion-row>\r\n\t\t\t\t\t   <ion-row>\r\n              <ng2-smart-table [settings]=\"computerLabDetailssettingsReadOnly\" \r\n              [source]=\"computerLabDetailsdata\"></ng2-smart-table>\r\n\t\t\t\t\t   </ion-row>\r\n                      <ion-row>\r\n                          <ion-col>\r\n                              <ion-item>\r\n                                  <ion-label>Is there a seperate room for the library?</ion-label>\r\n                              </ion-item>\r\n                          </ion-col>\r\n                          <ion-col>\r\n                              <ion-item>\r\n                                  <ion-label>{{classroomEnvironment.libraryRoom}}</ion-label>\r\n                              </ion-item>\r\n                          </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>Is there any NGO/Org managing/supporting the library</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>{{classroomEnvironment.libraryNGO.has}}</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>Mention the organization</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>{{classroomEnvironment.libraryNGO.remarks}}</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>Library Room Details</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t  <ion-row *ngFor=\"let item of classroomEnvironment.libraryDetail.details ; let index=index;\">\r\n                              <ion-col>\r\n                                  <ion-item>\r\n\t\t\t\t\t\t\t\t\t                    <ion-label>{{item.name}}</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                              <ion-col>\r\n                                <ion-item>\r\n                                    <ion-label>{{classroomEnvironment.libraryDetail.details[index].value}}</ion-label>\r\n                                </ion-item>\r\n                              </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>Is there a seperate room for Science Lab?</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>{{classroomEnvironment.scienceLab.has}}</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                      </ion-row>\r\n\t\t\t\t\t            <ion-row>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>Science Lab Details</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t  <ion-row *ngFor=\"let item of classroomEnvironment.scienceLab.details ; let index=index;\">\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                    <ion-label>{{item.name}}</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                              <ion-col>\r\n                                  <ion-item>      \r\n                                    <ion-label>{{classroomEnvironment.scienceLab.details[index].value}}</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>Is there any Org/NGO supporting Science Lab activities?</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>{{classroomEnvironment.scienceNGO.has}}</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                            <ion-col>\r\n                                <ion-item>\r\n                                    <ion-label>Mention the organization</ion-label>\r\n                                </ion-item>\r\n                            </ion-col>\r\n                            <ion-col>\r\n                                <ion-item>\r\n                                    <ion-label>{{classroomEnvironment.scienceNGO.remarks}}</ion-label>\r\n                                </ion-item>\r\n                            </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>Is there a store room dedicated for sports?</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>{{classroomEnvironment.sportsStore}}</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>Do you have sufficient sports materials?</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>{{classroomEnvironment.sportsMaterial}}</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>Are you enaging students in teaching art & craft?</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>{{classroomEnvironment.engageArtCraft}}</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>Is there any organization supporting Art & Craft?</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>{{classroomEnvironment.supportArtCraft}}</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                      </ion-row>\r\n                      <ion-row>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>Do you have sufficient learning materials?</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                              <ion-col>\r\n                                  <ion-item>\r\n                                      <ion-label>{{classroomEnvironment.learningMaterial}}</ion-label>\r\n                                  </ion-item>\r\n                              </ion-col>\r\n                      </ion-row>\r\n                  </ion-grid>\r\n              </ion-card-body>\r\n          </ion-card>\r\n       </div>\r\n     <div *ngIf=\"hasStarted && isEditing\">\r\n    <ion-item class=\"yesorNo\">\r\n      <ion-grid class=\"radioGroup\">\r\n        <ion-row>\r\n          <ion-col>\r\n            <ion-radio-group  [(ngModel)]=\"classroomEnvironment.computerLab\" name = \"radio_1\">\r\n            <ion-row>\r\n               <ion-label class=\"schoolLocatedLabel\">Is there a seperate room for computer Lab in the school? </ion-label>\r\n              <ion-col class=\"radioGroupCol\">\r\n                   <ion-label>Yes</ion-label>\r\n              </ion-col>\r\n              <ion-col class=\"radioGroupCol\">   \t\r\n                  <ion-radio value=\"Yes\"  ></ion-radio>\r\n              </ion-col>\r\n              <ion-col class=\"radioGroupCol\">\r\n                  <ion-label>No</ion-label>\r\n              </ion-col>  \r\n              <ion-col class=\"radioGroupCol\">\t\r\n                    <ion-radio value=\"No\" ></ion-radio>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-radio-group>  \r\n          </ion-col>\r\n        </ion-row>\r\n    </ion-grid>\t  \r\n  \r\n  </ion-item>\r\n  <ion-item  class = \"yesorNoTextArea\">\r\n  \r\n    <ion-grid class=\"radioGroup\">\r\n      <ion-row>\r\n        <ion-col>\r\n          <ion-radio-group  [(ngModel)]=\"classroomEnvironment.curriculum.has\" name = \"radio_1\">\r\n          <ion-row>\r\n             <ion-label class=\"schoolLocatedLabel\">Is curriculum available? </ion-label>\r\n             \r\n            <ion-col class=\"radioGroupCol\">\r\n                 <ion-label>Yes</ion-label>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">   \t\r\n                <ion-radio value=\"Yes\" (ionSelect) = \"ShowNextOption('curriculumRemarks')\" ></ion-radio>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">\r\n                <ion-label>No</ion-label>\r\n            </ion-col>  \r\n            <ion-col class=\"radioGroupCol\">\t\r\n                  <ion-radio value=\"No\" (ionSelect) = \"HideNextOption('curriculumRemarks')\"></ion-radio>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-radio-group>  \r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row class=\"visibility-hidden\" id=\"curriculumRemarks\">\r\n        <ion-col>\r\n        <ion-row>\r\n        <ion-label class=\"schoolLocatedLabel\"> \r\n          Provide the details\r\n        </ion-label>\r\n        </ion-row>\r\n        <ion-row>\r\n        <ion-textarea [(ngModel)]=\"classroomEnvironment.curriculum.remarks\" ></ion-textarea>\r\n        </ion-row>\r\n        </ion-col>\r\n      </ion-row>\r\n  </ion-grid>\t  \r\n</ion-item>\r\n\r\n<ion-item  class = \"yesorNoTextArea\">\r\n  \r\n    <ion-grid class=\"radioGroup\">\r\n      <ion-row>\r\n        <ion-col>\r\n          <ion-radio-group  [(ngModel)]=\"classroomEnvironment.eLearn.has\" name = \"radio_1\">\r\n          <ion-row>\r\n             <ion-label class=\"schoolLocatedLabel\">Is e-learning system of teaching available? </ion-label>\r\n             \r\n            <ion-col class=\"radioGroupCol\">\r\n                 <ion-label>Yes</ion-label>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">   \t\r\n                <ion-radio value=\"Yes\" (ionSelect) = \"ShowNextOption('eLearnRemarks')\"  ></ion-radio>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">\r\n                <ion-label>No</ion-label>\r\n            </ion-col>  \r\n            <ion-col class=\"radioGroupCol\">\t\r\n                  <ion-radio value=\"No\" (ionSelect) = \"HideNextOption('eLearnRemarks')\" ></ion-radio>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-radio-group>  \r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row class=\"textField\" id=\"eLearnRemarks\" class=\"visibility-hidden\">\r\n        <ion-label class=\"remarksLabel\"> \r\n          Mention the organization who provides the system\r\n        </ion-label>\r\n        <ion-input [(ngModel)]=\"classroomEnvironment.eLearn.remarks\" ></ion-input>\r\n        </ion-row>\r\n  </ion-grid>\t  \r\n</ion-item>\r\n<ion-item>\r\n    <ion-grid class=\"radioGroup\">\r\n      <ion-row>\r\n          <ion-col>\r\n            <ion-row>\r\n                <ion-label class=\"schoolLocatedLabel\">ComputerLab Details</ion-label>\r\n            </ion-row>\r\n          </ion-col>\r\n        </ion-row>\r\n    <ng2-smart-table [settings]=\"computerLabDetailssettings\" [source]=\"computerLabDetailsdata\"></ng2-smart-table>\r\n  </ion-grid>\r\n  </ion-item>\r\n<ion-item class=\"yesorNo\">\r\n    <ion-grid class=\"radioGroup\">\r\n      <ion-row>\r\n        <ion-col>\r\n          <ion-radio-group  [(ngModel)]=\"classroomEnvironment.libraryRoom\" name = \"radio_1\">\r\n          <ion-row>\r\n             <ion-label class=\"schoolLocatedLabel\">Is there a seperate room for the library? </ion-label>\r\n            <ion-col class=\"radioGroupCol\">\r\n                 <ion-label>Yes</ion-label>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">   \t\r\n                <ion-radio value=\"Yes\"  ></ion-radio>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">\r\n                <ion-label>No</ion-label>\r\n            </ion-col>  \r\n            <ion-col class=\"radioGroupCol\">\t\r\n                  <ion-radio value=\"No\" ></ion-radio>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-radio-group>  \r\n        </ion-col>\r\n      </ion-row>\r\n  </ion-grid>\t  \r\n\r\n</ion-item>\r\n<ion-item class=\"yesorNoTextArea\">\r\n    <ion-grid class=\"radioGroup\">\r\n      <ion-row>\r\n        <ion-col>\r\n          <ion-radio-group  [(ngModel)]=\"classroomEnvironment.libraryNGO.has\" name = \"radio_1\">\r\n          <ion-row>\r\n             <ion-label class=\"schoolLocatedLabel\">Is there any NGO/Org managing/supporting the library </ion-label>\r\n            <ion-col class=\"radioGroupCol\">\r\n                 <ion-label>Yes</ion-label>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">   \t\r\n                <ion-radio value=\"Yes\" (ionSelect) = \"ShowNextOption('libraryNGORemarks')\" ></ion-radio>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">\r\n                <ion-label>No</ion-label>\r\n            </ion-col>  \r\n            <ion-col class=\"radioGroupCol\">\t\r\n                  <ion-radio value=\"No\" (ionSelect) = \"HideNextOption('libraryNGORemarks')\"></ion-radio>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-radio-group>  \r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row class=\"textField\" id=\"libraryNGORemarks\" class=\"visibility-hidden\">\r\n          <ion-label class=\"remarksLabel\"> \r\n            Mention the organization\r\n          </ion-label>\r\n          <ion-input [(ngModel)]=\"classroomEnvironment.libraryNGO.remarks\" ></ion-input>\r\n          </ion-row>\r\n  </ion-grid>\t  \r\n\r\n</ion-item>\r\n<ion-item class =\"yesorNoTextArea\">\r\n  <ion-grid class=\"radioGroup\">\r\n      <ion-row>\r\n          <ion-col>\r\n            <ion-row>\r\n               <ion-label class=\"schoolLocatedLabel\"> Library Room Details</ion-label>\r\n            </ion-row>\r\n          </ion-col>\r\n        </ion-row>\r\n        <!--\r\n      <ion-row >\r\n        <ion-col>\r\n          <ion-radio-group name = \"radio_1\" class = \"yesorNo\">\r\n          <ion-row>\r\n             <ion-label class=\"schoolLocatedLabel\">Is library arranged level wise?</ion-label>\r\n            <ion-col class=\"radioGroupCol\">\r\n                 <ion-label>Yes</ion-label>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">   \t\r\n                <ion-radio value=\"Yes\"  ></ion-radio>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">\r\n                <ion-label>No</ion-label>\r\n            </ion-col>  \r\n            <ion-col class=\"radioGroupCol\">\t\r\n                  <ion-radio value=\"No\" ></ion-radio>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-radio-group>\r\n        </ion-col>\r\n      </ion-row>\r\n        <ion-row >\r\n            <ion-label class=\"remarksLabel\"> \r\n              No of books available?\r\n            </ion-label>\r\n            <ion-input ></ion-input>\r\n        </ion-row>\r\n        <ion-row>\r\n            <ion-col>\r\n              <ion-radio-group name = \"radio_1\" class = \"yesorNo\">\r\n              <ion-row>\r\n                 <ion-label class=\"schoolLocatedLabel\">Is sufficient cupboards available?</ion-label>\r\n                <ion-col class=\"radioGroupCol\">\r\n                     <ion-label>Yes</ion-label>\r\n                </ion-col>\r\n                <ion-col class=\"radioGroupCol\">   \t\r\n                    <ion-radio value=\"Yes\"  ></ion-radio>\r\n                </ion-col>\r\n                <ion-col class=\"radioGroupCol\">\r\n                    <ion-label>No</ion-label>\r\n                </ion-col>  \r\n                <ion-col class=\"radioGroupCol\">\t\r\n                      <ion-radio value=\"No\" ></ion-radio>\r\n                </ion-col>\r\n              </ion-row>\r\n            </ion-radio-group>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-row>\r\n              <ion-col>\r\n                <ion-radio-group  name = \"radio_1\" class = \"yesorNo\">\r\n                <ion-row>\r\n                   <ion-label class=\"schoolLocatedLabel\">Is Table available?</ion-label>\r\n                  <ion-col class=\"radioGroupCol\">\r\n                       <ion-label>Yes</ion-label>\r\n                  </ion-col>\r\n                  <ion-col class=\"radioGroupCol\">   \t\r\n                      <ion-radio value=\"Yes\"  ></ion-radio>\r\n                  </ion-col>\r\n                  <ion-col class=\"radioGroupCol\">\r\n                      <ion-label>No</ion-label>\r\n                  </ion-col>  \r\n                  <ion-col class=\"radioGroupCol\">\t\r\n                        <ion-radio value=\"No\" ></ion-radio>\r\n                  </ion-col>\r\n                </ion-row>\r\n              </ion-radio-group>\r\n              </ion-col>\r\n            </ion-row>\r\n            <ion-row>\r\n                <ion-col>\r\n                  <ion-radio-group name = \"radio_1\" class = \"yesorNo\">\r\n                  <ion-row>\r\n                     <ion-label class=\"schoolLocatedLabel\">Is sufficient chairs/bench available?</ion-label>\r\n                    <ion-col class=\"radioGroupCol\">\r\n                         <ion-label>Yes</ion-label>\r\n                    </ion-col>\r\n                    <ion-col class=\"radioGroupCol\">   \t\r\n                        <ion-radio value=\"Yes\"  ></ion-radio>\r\n                    </ion-col>\r\n                    <ion-col class=\"radioGroupCol\">\r\n                        <ion-label>No</ion-label>\r\n                    </ion-col>  \r\n                    <ion-col class=\"radioGroupCol\">\t\r\n                          <ion-radio value=\"No\" ></ion-radio>\r\n                    </ion-col>\r\n                  </ion-row>\r\n                </ion-radio-group>\r\n                </ion-col>\r\n              </ion-row>\r\n              -->\r\n              <ion-row *ngFor=\"let item of classroomEnvironment.libraryDetail.details ; let index=index;\">\r\n                  <ion-col>\r\n                    <ion-radio-group  [(ngModel)]=\"classroomEnvironment.libraryDetail.details[index].value\" name = \"radio_1\" class = \"yesorNo\">\r\n                    <ion-row>\r\n                       <ion-label class=\"schoolLocatedLabel\"> {{item.name}}</ion-label>\r\n                      <ion-col class=\"radioGroupCol\">\r\n                           <ion-label>Yes</ion-label>\r\n                      </ion-col>\r\n                      <ion-col class=\"radioGroupCol\">   \t\r\n                          <ion-radio value=\"Yes\"  ></ion-radio>\r\n                      </ion-col>\r\n                      <ion-col class=\"radioGroupCol\">\r\n                          <ion-label>No</ion-label>\r\n                      </ion-col>  \r\n                      <ion-col class=\"radioGroupCol\">\t\r\n                            <ion-radio value=\"No\" ></ion-radio>\r\n                      </ion-col>\r\n                    </ion-row>\r\n                  </ion-radio-group>\r\n                  </ion-col>\r\n                </ion-row>       \r\n  </ion-grid>\t\r\n</ion-item>\r\n<ion-item  class = \"yesorNoTextArea\">\r\n  \r\n    <ion-grid class=\"radioGroup\">\r\n      <ion-row>\r\n        <ion-col>\r\n          <ion-radio-group  [(ngModel)]=\"classroomEnvironment.scienceLab.has\" name = \"radio_1\">\r\n          <ion-row>\r\n             <ion-label class=\"schoolLocatedLabel\">Is there a seperate room for Science Lab? </ion-label>\r\n             \r\n            <ion-col class=\"radioGroupCol\">\r\n                 <ion-label>Yes</ion-label>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">   \t\r\n                <ion-radio value=\"Yes\" (ionSelect) = \"ShowNextOption('scienceLabDetails')\" ></ion-radio>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">\r\n                <ion-label>No</ion-label>\r\n            </ion-col>  \r\n            <ion-col class=\"radioGroupCol\">\t\r\n                  <ion-radio value=\"No\" (ionSelect) = \"HideNextOption('scienceLabDetails')\" > </ion-radio>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-radio-group>  \r\n        </ion-col>\r\n      </ion-row>\r\n          <ion-grid class=\"radioGroup\" id=\"scienceLabDetails\" class=\"visibility-hidden\">\r\n            <!--\r\n              <ion-row >\r\n                <ion-col>\r\n                  <ion-radio-group  name = \"radio_1\" class = \"yesorNo\">\r\n                  <ion-row>\r\n                     <ion-label class=\"schoolLocatedLabel\">wall mounted table?</ion-label>\r\n                    <ion-col class=\"radioGroupCol\">\r\n                         <ion-label>Yes</ion-label>\r\n                    </ion-col>\r\n                    <ion-col class=\"radioGroupCol\">   \t\r\n                        <ion-radio value=\"Yes\"  ></ion-radio>\r\n                    </ion-col>\r\n                    <ion-col class=\"radioGroupCol\">\r\n                        <ion-label>No</ion-label>\r\n                    </ion-col>  \r\n                    <ion-col class=\"radioGroupCol\">\t\r\n                          <ion-radio value=\"No\" ></ion-radio>\r\n                    </ion-col>\r\n                  </ion-row>\r\n                </ion-radio-group>\r\n                </ion-col>\r\n              </ion-row>\r\n              <ion-row >\r\n                  <ion-col>\r\n                    <ion-radio-group  name = \"radio_1\" class = \"yesorNo\">\r\n                    <ion-row>\r\n                       <ion-label class=\"schoolLocatedLabel\">Center Table</ion-label>\r\n                      <ion-col class=\"radioGroupCol\">\r\n                           <ion-label>Yes</ion-label>\r\n                      </ion-col>\r\n                      <ion-col class=\"radioGroupCol\">   \t\r\n                          <ion-radio value=\"Yes\"  ></ion-radio>\r\n                      </ion-col>\r\n                      <ion-col class=\"radioGroupCol\">\r\n                          <ion-label>No</ion-label>\r\n                      </ion-col>  \r\n                      <ion-col class=\"radioGroupCol\">\t\r\n                            <ion-radio value=\"No\" ></ion-radio>\r\n                      </ion-col>\r\n                    </ion-row>\r\n                  </ion-radio-group>\r\n                  </ion-col>\r\n                </ion-row>\r\n                <ion-row >\r\n                          <ion-col>\r\n                            <ion-radio-group  name = \"radio_1\" class = \"yesorNo\">\r\n                            <ion-row>\r\n                               <ion-label class=\"schoolLocatedLabel\">Chemicals</ion-label>\r\n                              <ion-col class=\"radioGroupCol\">\r\n                                   <ion-label>Yes</ion-label>\r\n                              </ion-col>\r\n                              <ion-col class=\"radioGroupCol\">   \t\r\n                                  <ion-radio value=\"Yes\"  ></ion-radio>\r\n                              </ion-col>\r\n                              <ion-col class=\"radioGroupCol\">\r\n                                  <ion-label>No</ion-label>\r\n                              </ion-col>  \r\n                              <ion-col class=\"radioGroupCol\">\t\r\n                                    <ion-radio value=\"No\" ></ion-radio>\r\n                              </ion-col>\r\n                            </ion-row>\r\n                          </ion-radio-group>\r\n                          </ion-col>\r\n                        </ion-row>\r\n                  <ion-row>\r\n                      <ion-col>\r\n                        <ion-radio-group name = \"radio_1\" class = \"yesorNo\">\r\n                        <ion-row>\r\n                           <ion-label class=\"schoolLocatedLabel\">Washbasin?</ion-label>\r\n                          <ion-col class=\"radioGroupCol\">\r\n                               <ion-label>Yes</ion-label>\r\n                          </ion-col>\r\n                          <ion-col class=\"radioGroupCol\">   \t\r\n                              <ion-radio value=\"Yes\"  ></ion-radio>\r\n                          </ion-col>\r\n                          <ion-col class=\"radioGroupCol\">\r\n                              <ion-label>No</ion-label>\r\n                          </ion-col>  \r\n                          <ion-col class=\"radioGroupCol\">\t\r\n                                <ion-radio value=\"No\" ></ion-radio>\r\n                          </ion-col>\r\n                        </ion-row>\r\n                      </ion-radio-group>\r\n                      </ion-col>\r\n                    </ion-row>\r\n                    <ion-row>\r\n                        <ion-col>\r\n                          <ion-radio-group  name = \"radio_1\" class = \"yesorNo\">\r\n                          <ion-row>\r\n                             <ion-label class=\"schoolLocatedLabel\">Models</ion-label>\r\n                            <ion-col class=\"radioGroupCol\">\r\n                                 <ion-label>Yes</ion-label>\r\n                            </ion-col>\r\n                            <ion-col class=\"radioGroupCol\">   \t\r\n                                <ion-radio value=\"Yes\"  ></ion-radio>\r\n                            </ion-col>\r\n                            <ion-col class=\"radioGroupCol\">\r\n                                <ion-label>No</ion-label>\r\n                            </ion-col>  \r\n                            <ion-col class=\"radioGroupCol\">\t\r\n                                  <ion-radio value=\"No\" ></ion-radio>\r\n                            </ion-col>\r\n                          </ion-row>\r\n                        </ion-radio-group>\r\n                        </ion-col>\r\n                      </ion-row>\r\n                      <ion-row >\r\n                          <ion-col>\r\n                            <ion-radio-group  name = \"radio_1\" class = \"yesorNo\">\r\n                            <ion-row>\r\n                               <ion-label class=\"schoolLocatedLabel\">Any other?</ion-label>\r\n                              <ion-col class=\"radioGroupCol\">\r\n                                   <ion-label>Yes</ion-label>\r\n                              </ion-col>\r\n                              <ion-col class=\"radioGroupCol\">   \t\r\n                                  <ion-radio value=\"Yes\"  ></ion-radio>\r\n                              </ion-col>\r\n                              <ion-col class=\"radioGroupCol\">\r\n                                  <ion-label>No</ion-label>\r\n                              </ion-col>  \r\n                              <ion-col class=\"radioGroupCol\">\t\r\n                                    <ion-radio value=\"No\" ></ion-radio>\r\n                              </ion-col>\r\n                            </ion-row>\r\n                          </ion-radio-group>\r\n                          </ion-col>\r\n                        </ion-row>\r\n                      -->\r\n                      <ion-row *ngFor=\"let item of classroomEnvironment.scienceLab.details ; let index=index;\" >\r\n                          <ion-col>\r\n                            <ion-radio-group  [(ngModel)]=\"classroomEnvironment.scienceLab.details[index].value\" name = \"radio_1\" class = \"yesorNo\">\r\n                            <ion-row>\r\n                               <ion-label class=\"schoolLocatedLabel\"> {{item.name}}</ion-label>\r\n                              <ion-col class=\"radioGroupCol\">\r\n                                   <ion-label>Yes</ion-label>\r\n                              </ion-col>\r\n                              <ion-col class=\"radioGroupCol\">   \t\r\n                                  <ion-radio value=\"Yes\"  ></ion-radio>\r\n                              </ion-col>\r\n                              <ion-col class=\"radioGroupCol\">\r\n                                  <ion-label>No</ion-label>\r\n                              </ion-col>  \r\n                              <ion-col class=\"radioGroupCol\">\t\r\n                                    <ion-radio value=\"No\" ></ion-radio>\r\n                              </ion-col>\r\n                            </ion-row>\r\n                          </ion-radio-group>\r\n                          </ion-col>\r\n                        </ion-row>\r\n          </ion-grid>\t\r\n  </ion-grid>\t  \r\n</ion-item>\r\n<ion-item  class = \"yesorNoTextArea\">\r\n  \r\n    <ion-grid class=\"radioGroup\">\r\n      <ion-row>\r\n        <ion-col>\r\n          <ion-radio-group  [(ngModel)]=\"classroomEnvironment.scienceNGO.has\" name = \"radio_1\">\r\n          <ion-row>\r\n             <ion-label class=\"schoolLocatedLabel\">Is there any Org/NGO supporting Science Lab activities? </ion-label> \r\n             \r\n            <ion-col class=\"radioGroupCol\">\r\n                 <ion-label>Yes</ion-label>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">   \t\r\n                <ion-radio value=\"Yes\" (ionSelect) = \"ShowNextOption('scienceNGORemarks')\" ></ion-radio>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">\r\n                <ion-label>No</ion-label>\r\n            </ion-col>  \r\n            <ion-col class=\"radioGroupCol\">\t\r\n                  <ion-radio value=\"No\" (ionSelect) = \"HideNextOption('scienceNGORemarks')\"></ion-radio>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-radio-group>  \r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row class=\"textField\" id=\"scienceNGORemarks\" class=\"visibility-hidden\">\r\n        <ion-label class=\"remarksLabel\"> \r\n          Mention the organization \r\n        </ion-label>\r\n        <ion-input [(ngModel)]=\"classroomEnvironment.scienceNGO.remarks\" ></ion-input>\r\n        </ion-row>\r\n  </ion-grid>\t  \r\n</ion-item>\r\n<ion-item class=\"yesorNo\">\r\n    <ion-grid class=\"radioGroup\">\r\n      <ion-row>\r\n        <ion-col>\r\n          <ion-radio-group  [(ngModel)]=\"classroomEnvironment.sportsStore\" name = \"radio_1\">\r\n          <ion-row>\r\n             <ion-label class=\"schoolLocatedLabel\">Is there a store room dedicated for sports? </ion-label>\r\n            <ion-col class=\"radioGroupCol\">\r\n                 <ion-label>Yes</ion-label>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">   \t\r\n                <ion-radio value=\"Yes\"  ></ion-radio>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">\r\n                <ion-label>No</ion-label>\r\n            </ion-col>  \r\n            <ion-col class=\"radioGroupCol\">\t\r\n                  <ion-radio value=\"No\" ></ion-radio>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-radio-group>  \r\n        </ion-col>\r\n      </ion-row>\r\n  </ion-grid>\t  \r\n\r\n</ion-item>\r\n<ion-item class=\"yesorNo\">\r\n    <ion-grid class=\"radioGroup\">\r\n      <ion-row>\r\n        <ion-col>\r\n          <ion-radio-group  [(ngModel)]=\"classroomEnvironment.playGround\" name = \"radio_1\">\r\n          <ion-row>\r\n             <ion-label class=\"schoolLocatedLabel\">Do you have a playground? </ion-label>\r\n            <ion-col class=\"radioGroupCol\">\r\n                 <ion-label>Yes</ion-label>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">   \t\r\n                <ion-radio value=\"Yes\"  ></ion-radio>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">\r\n                <ion-label>No</ion-label>\r\n            </ion-col>  \r\n            <ion-col class=\"radioGroupCol\">\t\r\n                  <ion-radio value=\"No\" ></ion-radio>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-radio-group>  \r\n        </ion-col>\r\n      </ion-row>\r\n  </ion-grid>\t  \r\n\r\n</ion-item>\r\n<ion-item class=\"yesorNo\">\r\n    <ion-grid class=\"radioGroup\">\r\n      <ion-row>\r\n        <ion-col>\r\n          <ion-radio-group  [(ngModel)]=\"classroomEnvironment.sportsMaterial\" name = \"radio_1\">\r\n          <ion-row>\r\n             <ion-label class=\"schoolLocatedLabel\">Do you have sufficient sports materials? </ion-label>\r\n            <ion-col class=\"radioGroupCol\">\r\n                 <ion-label>Yes</ion-label>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">   \t\r\n                <ion-radio value=\"Yes\"  ></ion-radio>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">\r\n                <ion-label>No</ion-label>\r\n            </ion-col>  \r\n            <ion-col class=\"radioGroupCol\">\t\r\n                  <ion-radio value=\"No\" ></ion-radio>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-radio-group>  \r\n        </ion-col>\r\n      </ion-row>\r\n  </ion-grid>\t  \r\n\r\n</ion-item>\r\n<ion-item class=\"yesorNo\">\r\n    <ion-grid class=\"radioGroup\">\r\n      <ion-row>\r\n        <ion-col>\r\n          <ion-radio-group  [(ngModel)]=\"classroomEnvironment.engageArtCraft\" name = \"radio_1\">\r\n          <ion-row>\r\n             <ion-label class=\"schoolLocatedLabel\">Are you enaging students in teaching art & craft? </ion-label>\r\n            <ion-col class=\"radioGroupCol\">\r\n                 <ion-label>Yes</ion-label>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">   \t\r\n                <ion-radio value=\"Yes\"  ></ion-radio>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">\r\n                <ion-label>No</ion-label>\r\n            </ion-col>  \r\n            <ion-col class=\"radioGroupCol\">\t\r\n                  <ion-radio value=\"No\" ></ion-radio>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-radio-group>  \r\n        </ion-col>\r\n      </ion-row>\r\n  </ion-grid>\t  \r\n\r\n</ion-item>\r\n<ion-item class=\"yesorNo\">\r\n    <ion-grid class=\"radioGroup\">\r\n      <ion-row>\r\n        <ion-col>\r\n          <ion-radio-group  [(ngModel)]=\"classroomEnvironment.supportArtCraft\" name = \"radio_1\">\r\n          <ion-row>\r\n             <ion-label class=\"schoolLocatedLabel\">Is there any organization supporting Art & Craft? </ion-label>\r\n            <ion-col class=\"radioGroupCol\">\r\n                 <ion-label>Yes</ion-label>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">   \t\r\n                <ion-radio value=\"Yes\"  ></ion-radio>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">\r\n                <ion-label>No</ion-label>\r\n            </ion-col>  \r\n            <ion-col class=\"radioGroupCol\">\t\r\n                  <ion-radio value=\"No\" ></ion-radio>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-radio-group>  \r\n        </ion-col>\r\n      </ion-row>\r\n  </ion-grid>\t  \r\n\r\n</ion-item>\r\n<ion-item class=\"yesorNo\">\r\n    <ion-grid class=\"radioGroup\">\r\n      <ion-row>\r\n        <ion-col>\r\n          <ion-radio-group  [(ngModel)]=\"classroomEnvironment.learningMaterial\" name = \"radio_1\">\r\n          <ion-row>\r\n             <ion-label class=\"schoolLocatedLabel\">Do you have sufficient learning materials? </ion-label>\r\n            <ion-col class=\"radioGroupCol\">\r\n                 <ion-label>Yes</ion-label>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">   \t\r\n                <ion-radio value=\"Yes\"  ></ion-radio>\r\n            </ion-col>\r\n            <ion-col class=\"radioGroupCol\">\r\n                <ion-label>No</ion-label>\r\n            </ion-col>  \r\n            <ion-col class=\"radioGroupCol\">\t\r\n                  <ion-radio value=\"No\" ></ion-radio>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-radio-group>  \r\n        </ion-col>\r\n      </ion-row>\r\n  </ion-grid>\t  \r\n</ion-item>\r\n     </div>\r\n\r\n     <ion-button color=\"primary\" (click)=\"isEditing = !isEditing\" *ngIf=\"hasStarted && !isEditing\">\r\n        <ion-icon name=\"create\" padding></ion-icon>\r\n        Edit</ion-button>\r\n    <ion-button color=\"secondary\" (click)=\"saveClassRoomEnvironmentDetailsAndContinue()\" *ngIf=\"isEditing\" (click)=\"isEditing = !isEditing\"> \r\n      <ion-icon name=\"save\" padding></ion-icon>\r\n     Save & Continue</ion-button>\r\n     <ion-button color=\"tertiary\" (click)=\"saveClassRoomEnvironmentDetails()\" *ngIf=\"isEditing\" (click)=\"isEditing = !isEditing\"> \r\n      <ion-icon name=\"clipboard\" padding></ion-icon>\r\n     Save</ion-button>    \r\n  </ion-content>\r\n  "

/***/ }),

/***/ "./src/app/classroom-environment/classroom-environment.page.scss":
/*!***********************************************************************!*\
  !*** ./src/app/classroom-environment/classroom-environment.page.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".textField {\n  --border-radius: 5px;\n  /*--box-shadow: 0px 14px 25px rgba(182, 30, 30, 0.59);*/ }\n\n.textField ion-label {\n  width: 250px; }\n\n.yesorNo ion-label {\n  width: 370px !important; }\n\n.yesorNoTextArea .schoolLocatedLabel {\n  width: 370px !important;\n  max-width: 370px !important; }\n\n.yesorNoTextArea .remarksLabel {\n  width: 370px !important;\n  max-width: 370px !important;\n  margin-top: 25px;\n  margin-left: -5px;\n  margin-right: 20px; }\n\n.yesorNoTextArea ion-input {\n  --padding-bottom: 0px; }\n\n.yesorNoMultiple ion-label {\n  width: 100px; }\n\n.dateLabel {\n  max-width: 200px; }\n\n.radioGroup {\n  width: 100%; }\n\nion-radio {\n  margin-top: 0px; }\n\n.schoolLocatedLabel {\n  margin-top: 25px;\n  margin-left: -10px;\n  margin-right: 20px; }\n\n.textareaLabel {\n  max-width: 400px; }\n\n/*.addressItem{\r\n\theight: 150px;\r\n\twidth: 100%;\r\n}*/\n\n/*ion-textarea{\r\n\theight: 116px;\r\n\twidth:100%;\r\n    background: #c1c4cd;\r\n}\r\n\r\n.item-input.sc-ion-label-md-h, .item-input .sc-ion-label-md-h {\r\n    -ms-flex: initial;\r\n    flex: initial;\r\n    width: 50% !important;\r\n    pointer-events: none;\r\n}*/\n\n/*ion-label{\r\n    padding: 12px;\r\n}\r\n\r\nion-radio{\r\n    margin-left: 20px;\r\n}*/\n\n.headerTextColor {\n  text-color: #DEE2DD; }\n\n.toolbarStyle {\n  background: #3B5999; }\n\n.radioGroupCol {\n  max-width: 60px !important;\n  max-height: 30px !important;\n  margin-top: 18px; }\n\n.visibility-hidden {\n  display: none; }\n\n.label-stacked.sc-ion-label-md-h {\n  font-size: 16px; }\n\n.radioLabel {\n  margin-right: 10px; }\n\n.schoolTypeCol {\n  max-width: 195px !important; }\n\n.saveButton {\n  float: right;\n  width: 100px;\n  height: 30px;\n  font-size: 14px !important;\n  font-weight: 700; }\n\nion-textarea {\n  padding-top: 10px; }\n\n:host /deep/ ng2-smart-table table {\n  border-style: inset;\n  border-color: initial;\n  -webkit-border-image: initial;\n       -o-border-image: initial;\n          border-image: initial;\n  border-width: 2px;\n  display: table;\n  border-spacing: 2px;\n  border-color: grey; }\n\n:host /deep/ ng2-smart-table table > tbody > tr > td {\n  box-sizing: border-box;\n  border: 1px solid grey; }\n\n:host /deep/ ng2-smart-table thead > tr > th {\n  border: 1px solid grey; }\n\n:host /deep/ ng2-smart-table {\n  font-size: 16px; }\n\n:host /deep/ ng2-smart-table .ng2-smart-filters {\n  display: none; }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2xhc3Nyb29tLWVudmlyb25tZW50L0M6XFx2YXJzaGFcXGN3XFxjYXJld29ya3N1aVxcQ2FyZXdvcmtzL3NyY1xcYXBwXFxjbGFzc3Jvb20tZW52aXJvbm1lbnRcXGNsYXNzcm9vbS1lbnZpcm9ubWVudC5wYWdlLnNjc3MiLCJzcmMvYXBwL2NsYXNzcm9vbS1lbnZpcm9ubWVudC9jbGFzc3Jvb20tZW52aXJvbm1lbnQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksb0JBQWdCO0VBQ2hCLHVEQUFBLEVBQXdEOztBQUU1RDtFQUNJLFlBQVksRUFBQTs7QUFFaEI7RUFDSSx1QkFBdUIsRUFBQTs7QUFFM0I7RUFFSSx1QkFBdUI7RUFDdkIsMkJBQTJCLEVBQUE7O0FBRS9CO0VBRUksdUJBQXVCO0VBQ3ZCLDJCQUEyQjtFQUMzQixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGtCQUFrQixFQUFBOztBQUV0QjtFQUVJLHFCQUFpQixFQUFBOztBQUVyQjtFQUNJLFlBQVksRUFBQTs7QUFFaEI7RUFDRyxnQkFBZ0IsRUFBQTs7QUFFbkI7RUFDQyxXQUFXLEVBQUE7O0FBR1o7RUFDSSxlQUFlLEVBQUE7O0FBR25CO0VBQ0ksZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixrQkFBa0IsRUFBQTs7QUFFdEI7RUFFSSxnQkFBZ0IsRUFBQTs7QUFFcEI7OztFQ0hFOztBRFFGOzs7Ozs7Ozs7OztFQ0lFOztBRFNGOzs7Ozs7RUNGRTs7QURVRDtFQUNHLG1CQUFtQixFQUFBOztBQUd2QjtFQUNJLG1CQUFtQixFQUFBOztBQUd2QjtFQUNJLDBCQUEwQjtFQUMxQiwyQkFBMkI7RUFDM0IsZ0JBQWdCLEVBQUE7O0FBRXBCO0VBRUksYUFBYSxFQUFBOztBQUVqQjtFQUNHLGVBQWUsRUFBQTs7QUFHbEI7RUFDSSxrQkFBa0IsRUFBQTs7QUFHdEI7RUFDSSwyQkFBMkIsRUFBQTs7QUFHL0I7RUFFSSxZQUFZO0VBQ1osWUFBWTtFQUNaLFlBQVk7RUFDWiwwQkFBMEI7RUFDMUIsZ0JBQWdCLEVBQUE7O0FBR3BCO0VBQ0ksaUJBQWlCLEVBQUE7O0FBRXJCO0VBQ0ksbUJBQW1CO0VBQ25CLHFCQUFxQjtFQUNyQiw2QkFBcUI7T0FBckIsd0JBQXFCO1VBQXJCLHFCQUFxQjtFQUNyQixpQkFBaUI7RUFFakIsY0FBYztFQUNkLG1CQUFtQjtFQUNuQixrQkFBa0IsRUFBQTs7QUFHdEI7RUFDSSxzQkFBc0I7RUFDdEIsc0JBQXNCLEVBQUE7O0FBRzFCO0VBQ0ksc0JBQXNCLEVBQUE7O0FBRTFCO0VBQ0ksZUFBYyxFQUFBOztBQUVsQjtFQUVJLGFBQWEsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2NsYXNzcm9vbS1lbnZpcm9ubWVudC9jbGFzc3Jvb20tZW52aXJvbm1lbnQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRleHRGaWVsZHtcclxuICAgIC0tYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgLyotLWJveC1zaGFkb3c6IDBweCAxNHB4IDI1cHggcmdiYSgxODIsIDMwLCAzMCwgMC41OSk7Ki9cclxufVxyXG4udGV4dEZpZWxkIGlvbi1sYWJlbHtcclxuICAgIHdpZHRoOiAyNTBweDtcclxufVxyXG4ueWVzb3JObyBpb24tbGFiZWx7XHJcbiAgICB3aWR0aDogMzcwcHggIWltcG9ydGFudDtcclxufVxyXG4ueWVzb3JOb1RleHRBcmVhIC5zY2hvb2xMb2NhdGVkTGFiZWxcclxue1xyXG4gICAgd2lkdGg6IDM3MHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBtYXgtd2lkdGg6IDM3MHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuLnllc29yTm9UZXh0QXJlYSAucmVtYXJrc0xhYmVsXHJcbntcclxuICAgIHdpZHRoOiAzNzBweCAhaW1wb3J0YW50O1xyXG4gICAgbWF4LXdpZHRoOiAzNzBweCAhaW1wb3J0YW50O1xyXG4gICAgbWFyZ2luLXRvcDogMjVweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAtNXB4O1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xyXG59XHJcbi55ZXNvck5vVGV4dEFyZWEgaW9uLWlucHV0XHJcbntcclxuICAgIC0tcGFkZGluZy1ib3R0b206IDBweDtcclxufVxyXG4ueWVzb3JOb011bHRpcGxlIGlvbi1sYWJlbHtcclxuICAgIHdpZHRoOiAxMDBweDtcclxufVxyXG4uZGF0ZUxhYmVse1xyXG4gICBtYXgtd2lkdGg6IDIwMHB4OyBcclxufVxyXG4ucmFkaW9Hcm91cHtcclxuXHR3aWR0aDogMTAwJTtcclxufVxyXG5cclxuaW9uLXJhZGlve1xyXG4gICAgbWFyZ2luLXRvcDogMHB4O1xyXG59XHJcblxyXG4uc2Nob29sTG9jYXRlZExhYmVse1xyXG4gICAgbWFyZ2luLXRvcDogMjVweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAtMTBweDtcclxuICAgIG1hcmdpbi1yaWdodDogMjBweDtcclxufVxyXG4udGV4dGFyZWFMYWJlbFxyXG57XHJcbiAgICBtYXgtd2lkdGg6IDQwMHB4O1xyXG59XHJcbi8qLmFkZHJlc3NJdGVte1xyXG5cdGhlaWdodDogMTUwcHg7XHJcblx0d2lkdGg6IDEwMCU7XHJcbn0qL1xyXG5cclxuLyppb24tdGV4dGFyZWF7XHJcblx0aGVpZ2h0OiAxMTZweDtcclxuXHR3aWR0aDoxMDAlO1xyXG4gICAgYmFja2dyb3VuZDogI2MxYzRjZDtcclxufVxyXG5cclxuLml0ZW0taW5wdXQuc2MtaW9uLWxhYmVsLW1kLWgsIC5pdGVtLWlucHV0IC5zYy1pb24tbGFiZWwtbWQtaCB7XHJcbiAgICAtbXMtZmxleDogaW5pdGlhbDtcclxuICAgIGZsZXg6IGluaXRpYWw7XHJcbiAgICB3aWR0aDogNTAlICFpbXBvcnRhbnQ7XHJcbiAgICBwb2ludGVyLWV2ZW50czogbm9uZTtcclxufSovXHJcblxyXG4vKmlvbi1sYWJlbHtcclxuICAgIHBhZGRpbmc6IDEycHg7XHJcbn1cclxuXHJcbmlvbi1yYWRpb3tcclxuICAgIG1hcmdpbi1sZWZ0OiAyMHB4O1xyXG59Ki9cclxuXHJcbiAuaGVhZGVyVGV4dENvbG9ye1xyXG4gICAgdGV4dC1jb2xvcjogI0RFRTJERDtcclxuIH1cclxuXHJcbi50b29sYmFyU3R5bGV7XHJcbiAgICBiYWNrZ3JvdW5kOiAjM0I1OTk5O1xyXG59XHJcblxyXG4ucmFkaW9Hcm91cENvbHtcclxuICAgIG1heC13aWR0aDogNjBweCAhaW1wb3J0YW50O1xyXG4gICAgbWF4LWhlaWdodDogMzBweCAhaW1wb3J0YW50O1xyXG4gICAgbWFyZ2luLXRvcDogMThweDtcclxufVxyXG4udmlzaWJpbGl0eS1oaWRkZW5cclxue1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxufVxyXG4ubGFiZWwtc3RhY2tlZC5zYy1pb24tbGFiZWwtbWQtaCB7XHJcbiAgIGZvbnQtc2l6ZTogMTZweDtcclxufVxyXG5cclxuLnJhZGlvTGFiZWx7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbn1cclxuXHJcbi5zY2hvb2xUeXBlQ29se1xyXG4gICAgbWF4LXdpZHRoOiAxOTVweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uc2F2ZUJ1dHRvbntcclxuICAgIFxyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgd2lkdGg6IDEwMHB4O1xyXG4gICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgZm9udC1zaXplOiAxNHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBmb250LXdlaWdodDogNzAwO1xyXG59XHJcblxyXG5pb24tdGV4dGFyZWF7XHJcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcclxufVxyXG46aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHRhYmxle1xyXG4gICAgYm9yZGVyLXN0eWxlOiBpbnNldDtcclxuICAgIGJvcmRlci1jb2xvcjogaW5pdGlhbDtcclxuICAgIGJvcmRlci1pbWFnZTogaW5pdGlhbDtcclxuICAgIGJvcmRlci13aWR0aDogMnB4O1xyXG4gICAgXHJcbiAgICBkaXNwbGF5OiB0YWJsZTtcclxuICAgIGJvcmRlci1zcGFjaW5nOiAycHg7XHJcbiAgICBib3JkZXItY29sb3I6IGdyZXk7IFxyXG5cclxufVxyXG46aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHRhYmxlID4gdGJvZHkgPiB0ciA+IHRkIHtcclxuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCBncmV5O1xyXG59XHJcblxyXG46aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHRoZWFkID4gdHIgPiB0aCAgeyBcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIGdyZXk7XHJcbn0gIFxyXG46aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHtcclxuICAgIGZvbnQtc2l6ZToxNnB4O1xyXG59XHJcbjpob3N0IC9kZWVwLyBuZzItc21hcnQtdGFibGUgLm5nMi1zbWFydC1maWx0ZXJzXHJcbntcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbn1cclxuIiwiLnRleHRGaWVsZCB7XG4gIC0tYm9yZGVyLXJhZGl1czogNXB4O1xuICAvKi0tYm94LXNoYWRvdzogMHB4IDE0cHggMjVweCByZ2JhKDE4MiwgMzAsIDMwLCAwLjU5KTsqLyB9XG5cbi50ZXh0RmllbGQgaW9uLWxhYmVsIHtcbiAgd2lkdGg6IDI1MHB4OyB9XG5cbi55ZXNvck5vIGlvbi1sYWJlbCB7XG4gIHdpZHRoOiAzNzBweCAhaW1wb3J0YW50OyB9XG5cbi55ZXNvck5vVGV4dEFyZWEgLnNjaG9vbExvY2F0ZWRMYWJlbCB7XG4gIHdpZHRoOiAzNzBweCAhaW1wb3J0YW50O1xuICBtYXgtd2lkdGg6IDM3MHB4ICFpbXBvcnRhbnQ7IH1cblxuLnllc29yTm9UZXh0QXJlYSAucmVtYXJrc0xhYmVsIHtcbiAgd2lkdGg6IDM3MHB4ICFpbXBvcnRhbnQ7XG4gIG1heC13aWR0aDogMzcwcHggIWltcG9ydGFudDtcbiAgbWFyZ2luLXRvcDogMjVweDtcbiAgbWFyZ2luLWxlZnQ6IC01cHg7XG4gIG1hcmdpbi1yaWdodDogMjBweDsgfVxuXG4ueWVzb3JOb1RleHRBcmVhIGlvbi1pbnB1dCB7XG4gIC0tcGFkZGluZy1ib3R0b206IDBweDsgfVxuXG4ueWVzb3JOb011bHRpcGxlIGlvbi1sYWJlbCB7XG4gIHdpZHRoOiAxMDBweDsgfVxuXG4uZGF0ZUxhYmVsIHtcbiAgbWF4LXdpZHRoOiAyMDBweDsgfVxuXG4ucmFkaW9Hcm91cCB7XG4gIHdpZHRoOiAxMDAlOyB9XG5cbmlvbi1yYWRpbyB7XG4gIG1hcmdpbi10b3A6IDBweDsgfVxuXG4uc2Nob29sTG9jYXRlZExhYmVsIHtcbiAgbWFyZ2luLXRvcDogMjVweDtcbiAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xuICBtYXJnaW4tcmlnaHQ6IDIwcHg7IH1cblxuLnRleHRhcmVhTGFiZWwge1xuICBtYXgtd2lkdGg6IDQwMHB4OyB9XG5cbi8qLmFkZHJlc3NJdGVte1xyXG5cdGhlaWdodDogMTUwcHg7XHJcblx0d2lkdGg6IDEwMCU7XHJcbn0qL1xuLyppb24tdGV4dGFyZWF7XHJcblx0aGVpZ2h0OiAxMTZweDtcclxuXHR3aWR0aDoxMDAlO1xyXG4gICAgYmFja2dyb3VuZDogI2MxYzRjZDtcclxufVxyXG5cclxuLml0ZW0taW5wdXQuc2MtaW9uLWxhYmVsLW1kLWgsIC5pdGVtLWlucHV0IC5zYy1pb24tbGFiZWwtbWQtaCB7XHJcbiAgICAtbXMtZmxleDogaW5pdGlhbDtcclxuICAgIGZsZXg6IGluaXRpYWw7XHJcbiAgICB3aWR0aDogNTAlICFpbXBvcnRhbnQ7XHJcbiAgICBwb2ludGVyLWV2ZW50czogbm9uZTtcclxufSovXG4vKmlvbi1sYWJlbHtcclxuICAgIHBhZGRpbmc6IDEycHg7XHJcbn1cclxuXHJcbmlvbi1yYWRpb3tcclxuICAgIG1hcmdpbi1sZWZ0OiAyMHB4O1xyXG59Ki9cbi5oZWFkZXJUZXh0Q29sb3Ige1xuICB0ZXh0LWNvbG9yOiAjREVFMkREOyB9XG5cbi50b29sYmFyU3R5bGUge1xuICBiYWNrZ3JvdW5kOiAjM0I1OTk5OyB9XG5cbi5yYWRpb0dyb3VwQ29sIHtcbiAgbWF4LXdpZHRoOiA2MHB4ICFpbXBvcnRhbnQ7XG4gIG1heC1oZWlnaHQ6IDMwcHggIWltcG9ydGFudDtcbiAgbWFyZ2luLXRvcDogMThweDsgfVxuXG4udmlzaWJpbGl0eS1oaWRkZW4ge1xuICBkaXNwbGF5OiBub25lOyB9XG5cbi5sYWJlbC1zdGFja2VkLnNjLWlvbi1sYWJlbC1tZC1oIHtcbiAgZm9udC1zaXplOiAxNnB4OyB9XG5cbi5yYWRpb0xhYmVsIHtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4OyB9XG5cbi5zY2hvb2xUeXBlQ29sIHtcbiAgbWF4LXdpZHRoOiAxOTVweCAhaW1wb3J0YW50OyB9XG5cbi5zYXZlQnV0dG9uIHtcbiAgZmxvYXQ6IHJpZ2h0O1xuICB3aWR0aDogMTAwcHg7XG4gIGhlaWdodDogMzBweDtcbiAgZm9udC1zaXplOiAxNHB4ICFpbXBvcnRhbnQ7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7IH1cblxuaW9uLXRleHRhcmVhIHtcbiAgcGFkZGluZy10b3A6IDEwcHg7IH1cblxuOmhvc3QgL2RlZXAvIG5nMi1zbWFydC10YWJsZSB0YWJsZSB7XG4gIGJvcmRlci1zdHlsZTogaW5zZXQ7XG4gIGJvcmRlci1jb2xvcjogaW5pdGlhbDtcbiAgYm9yZGVyLWltYWdlOiBpbml0aWFsO1xuICBib3JkZXItd2lkdGg6IDJweDtcbiAgZGlzcGxheTogdGFibGU7XG4gIGJvcmRlci1zcGFjaW5nOiAycHg7XG4gIGJvcmRlci1jb2xvcjogZ3JleTsgfVxuXG46aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHRhYmxlID4gdGJvZHkgPiB0ciA+IHRkIHtcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgYm9yZGVyOiAxcHggc29saWQgZ3JleTsgfVxuXG46aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHRoZWFkID4gdHIgPiB0aCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkIGdyZXk7IH1cblxuOmhvc3QgL2RlZXAvIG5nMi1zbWFydC10YWJsZSB7XG4gIGZvbnQtc2l6ZTogMTZweDsgfVxuXG46aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIC5uZzItc21hcnQtZmlsdGVycyB7XG4gIGRpc3BsYXk6IG5vbmU7IH1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/classroom-environment/classroom-environment.page.ts":
/*!*********************************************************************!*\
  !*** ./src/app/classroom-environment/classroom-environment.page.ts ***!
  \*********************************************************************/
/*! exports provided: ClassroomEnvironmentPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClassroomEnvironmentPage", function() { return ClassroomEnvironmentPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_need_assessment_classroom_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../models/need-assessment/classroom-environment */ "./src/app/models/need-assessment/classroom-environment.ts");
/* harmony import */ var _services_need_assessment_classroom_environment_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/need-assessment/classroom-environment.service */ "./src/app/services/need-assessment/classroom-environment.service.ts");
/* harmony import */ var _services_local_store_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/local-store-service.service */ "./src/app/services/local-store-service.service.ts");
/* harmony import */ var _classroom_environment_config__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./classroom-environment.config */ "./src/app/classroom-environment/classroom-environment.config.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var ClassroomEnvironmentPage = /** @class */ (function () {
    function ClassroomEnvironmentPage(classRoomEnvironmentService, router, localStoreService, _elementRef) {
        this.classRoomEnvironmentService = classRoomEnvironmentService;
        this.router = router;
        this.localStoreService = localStoreService;
        this._elementRef = _elementRef;
        this.demo = true;
        this.computerLabDetailssettings = _classroom_environment_config__WEBPACK_IMPORTED_MODULE_4__["ClassroomEnvironmentConfig"].computerLabDetailsConfig;
        this.computerLabDetailssettingsReadOnly = _classroom_environment_config__WEBPACK_IMPORTED_MODULE_4__["ClassroomEnvironmentConfig"].computerLabDetailsConfigReadOnly;
        this.classroomEnvironment = new _models_need_assessment_classroom_environment__WEBPACK_IMPORTED_MODULE_1__["classroomEnvironmentModel"]();
    }
    ClassroomEnvironmentPage.prototype.ngOnInit = function () {
        var _this = this;
        /**
         * While fetching the status API,
         * if need assessment has started, set hasStarted to true
         * else, hasStarted to false.
         *
         * isEdited is already handled.
         */
        this.isEditing = false;
        this.hasStarted = true;
        var schoolId = "1"; // ideally schoolId should be fetched from the local storage
        this.classRoomEnvironmentService.getClassRoomEnvironmentDetailsOfSchool(schoolId)
            .subscribe(function (result) {
            console.log("ClassRoomEnvironment - GET", result);
            //let data = this.classRoomEnvironmentService.getSampleJson();
            _this.initializeFormElements(result);
        }, function (err) {
            console.log("Some error occurred", err);
        });
    };
    ClassroomEnvironmentPage.prototype.getEmptyDetails = function () {
        var elem = new _models_need_assessment_classroom_environment__WEBPACK_IMPORTED_MODULE_1__["commonModel"]();
        elem.has = "";
        elem.remarks = "";
        elem.details = [];
        return elem;
    };
    ClassroomEnvironmentPage.prototype.getEmptyComputerLabDetails = function () {
        var elem = new _models_need_assessment_classroom_environment__WEBPACK_IMPORTED_MODULE_1__["csLabDetailsModel"]();
        elem.has = "Yes";
        elem.details = _classroom_environment_config__WEBPACK_IMPORTED_MODULE_4__["ClassroomEnvironmentConfig"].computerLabConfig;
        return elem;
    };
    ClassroomEnvironmentPage.prototype.getEmptyLibraryRoomDetails = function () {
        var elem = new _models_need_assessment_classroom_environment__WEBPACK_IMPORTED_MODULE_1__["yesOrNoModel"]();
        elem.has = "Yes";
        elem.details = _classroom_environment_config__WEBPACK_IMPORTED_MODULE_4__["ClassroomEnvironmentConfig"].libraryRoomConfig;
        return elem;
    };
    ClassroomEnvironmentPage.prototype.getEmptyScienceLabDetails = function () {
        var elem = new _models_need_assessment_classroom_environment__WEBPACK_IMPORTED_MODULE_1__["yesOrNoModel"]();
        elem.has = "";
        elem.details = _classroom_environment_config__WEBPACK_IMPORTED_MODULE_4__["ClassroomEnvironmentConfig"].scienceLabConfig;
        return elem;
    };
    ClassroomEnvironmentPage.prototype.initializeFormElements = function (dataJSON) {
        this.classroomEnvironment = dataJSON;
        if (this.classroomEnvironment.computerLab == null || this.classroomEnvironment.computerLab == undefined) {
            this.classroomEnvironment.computerLab = "";
        }
        if (this.classroomEnvironment.curriculum == null || this.classroomEnvironment.curriculum == undefined) {
            this.classroomEnvironment.curriculum = this.getEmptyDetails();
        }
        if (this.classroomEnvironment.eLearn == null || this.classroomEnvironment.eLearn == undefined) {
            this.classroomEnvironment.eLearn = this.getEmptyDetails();
        }
        if (this.classroomEnvironment.csLabDetail == null || this.classroomEnvironment.csLabDetail == undefined || this.classroomEnvironment.csLabDetail.details == undefined || this.classroomEnvironment.csLabDetail.details.length == 0) {
            this.classroomEnvironment.csLabDetail = this.getEmptyComputerLabDetails();
        }
        if (this.classroomEnvironment.libraryRoom == null || this.classroomEnvironment.libraryRoom == undefined) {
            this.classroomEnvironment.libraryRoom = "";
        }
        if (this.classroomEnvironment.libraryNGO == null || this.classroomEnvironment.libraryNGO == undefined) {
            this.classroomEnvironment.libraryNGO = this.getEmptyDetails();
        }
        if (this.classroomEnvironment.libraryDetail == null || this.classroomEnvironment.libraryDetail == undefined || this.classroomEnvironment.libraryDetail.details == undefined || this.classroomEnvironment.libraryDetail.details.length == 0) {
            this.classroomEnvironment.libraryDetail = this.getEmptyLibraryRoomDetails();
        }
        if (this.classroomEnvironment.scienceLab == null || this.classroomEnvironment.scienceLab == undefined || this.classroomEnvironment.scienceLab.details == undefined || this.classroomEnvironment.scienceLab.details.length == 0) {
            this.classroomEnvironment.scienceLab = this.getEmptyScienceLabDetails();
        }
        if (this.classroomEnvironment.scienceNGO == null || this.classroomEnvironment.scienceNGO == undefined) {
            this.classroomEnvironment.scienceNGO = this.getEmptyDetails();
        }
        if (this.classroomEnvironment.sportsStore == null || this.classroomEnvironment.sportsStore == undefined) {
            this.classroomEnvironment.sportsStore = "";
        }
        if (this.classroomEnvironment.playGround == null || this.classroomEnvironment.playGround == undefined) {
            this.classroomEnvironment.playGround = "";
        }
        if (this.classroomEnvironment.sportsMaterial == null || this.classroomEnvironment.sportsMaterial == undefined) {
            this.classroomEnvironment.sportsMaterial = "";
        }
        if (this.classroomEnvironment.engageArtCraft == null || this.classroomEnvironment.engageArtCraft == undefined) {
            this.classroomEnvironment.engageArtCraft = "";
        }
        if (this.classroomEnvironment.supportArtCraft == null || this.classroomEnvironment.supportArtCraft == undefined) {
            this.classroomEnvironment.supportArtCraft = "";
        }
        if (this.classroomEnvironment.learningMaterial == null || this.classroomEnvironment.learningMaterial == undefined) {
            this.classroomEnvironment.learningMaterial = "";
        }
        this.localStoreService.classRoomEnvironment = this.classroomEnvironment;
        this.computerLabDetailsdata = this.classroomEnvironment.csLabDetail.details;
    };
    ClassroomEnvironmentPage.prototype.ShowNextOption = function (elementId) {
        var elem = document.getElementById(elementId);
        if (elem.classList.contains('visibility-hidden')) {
            elem.classList.remove('visibility-hidden');
        }
    };
    ClassroomEnvironmentPage.prototype.HideNextOption = function (elementId) {
        var elem = document.getElementById(elementId);
        if (!elem.classList.contains('visibility-hidden')) {
            elem.classList.add('visibility-hidden');
        }
    };
    ClassroomEnvironmentPage.prototype.validateClassRoomEnvironmentDetails = function () {
        if (this.classroomEnvironment.curriculum.has == "No") {
            this.classroomEnvironment.curriculum.remarks = "";
        }
        if (this.classroomEnvironment.eLearn.has == "No") {
            this.classroomEnvironment.eLearn.remarks = "";
        }
        if (this.classroomEnvironment.libraryNGO.has == "No") {
            this.classroomEnvironment.libraryNGO.remarks = "";
        }
        if (this.classroomEnvironment.scienceLab.has == "No") {
            for (var _i = 0, _a = this.classroomEnvironment.scienceLab.details; _i < _a.length; _i++) {
                var value = _a[_i];
                value.value = "";
            }
        }
        if (this.classroomEnvironment.scienceNGO.has == "No") {
            this.classroomEnvironment.scienceNGO.remarks = "";
        }
    };
    ClassroomEnvironmentPage.prototype.saveClassRoomEnvironmentDetails = function () {
        this.validateClassRoomEnvironmentDetails();
        this.localStoreService.classRoomEnvironment = this.classroomEnvironment;
        console.log(this.classroomEnvironment);
        this.classRoomEnvironmentService.saveClassRoomEnvironmentDetails(this.classroomEnvironment).subscribe(function (res) {
            console.log("ClassroomEnvSave, success");
        }, function (err) {
            console.log(err);
        });
    };
    ClassroomEnvironmentPage.prototype.saveClassRoomEnvironmentDetailsAndContinue = function () {
        var _this = this;
        this.validateClassRoomEnvironmentDetails();
        this.localStoreService.classRoomEnvironment = this.classroomEnvironment;
        console.log(this.classroomEnvironment);
        this.classRoomEnvironmentService.saveClassRoomEnvironmentDetails(this.classroomEnvironment).subscribe(function (res) {
            _this.router.navigate(['../need-assessment/school-environment']);
            console.log("ClassroomEnvSave, success");
            _this.isEditing = false;
        }, function (err) {
            console.log(err);
            _this.isEditing = true;
        });
    };
    ClassroomEnvironmentPage.prototype.startEditing = function () {
        this.isEditing = true;
        this.hasStarted = true;
    };
    ClassroomEnvironmentPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-classroom-environment',
            template: __webpack_require__(/*! ./classroom-environment.page.html */ "./src/app/classroom-environment/classroom-environment.page.html"),
            styles: [__webpack_require__(/*! ./classroom-environment.page.scss */ "./src/app/classroom-environment/classroom-environment.page.scss")]
        }),
        __metadata("design:paramtypes", [_services_need_assessment_classroom_environment_service__WEBPACK_IMPORTED_MODULE_2__["ClassroomEnvironmentService"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _services_local_store_service_service__WEBPACK_IMPORTED_MODULE_3__["LocalStoreServiceService"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]])
    ], ClassroomEnvironmentPage);
    return ClassroomEnvironmentPage;
}());



/***/ }),

/***/ "./src/app/models/need-assessment/classroom-environment.ts":
/*!*****************************************************************!*\
  !*** ./src/app/models/need-assessment/classroom-environment.ts ***!
  \*****************************************************************/
/*! exports provided: classroomEnvironmentModel, csLabDetailsModel, commonModel, yesOrNoModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "classroomEnvironmentModel", function() { return classroomEnvironmentModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "csLabDetailsModel", function() { return csLabDetailsModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "commonModel", function() { return commonModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "yesOrNoModel", function() { return yesOrNoModel; });
var classroomEnvironmentModel = /** @class */ (function () {
    function classroomEnvironmentModel() {
    }
    return classroomEnvironmentModel;
}());

var csLabDetailsModel = /** @class */ (function () {
    function csLabDetailsModel() {
    }
    return csLabDetailsModel;
}());

var commonModel = /** @class */ (function () {
    function commonModel() {
    }
    return commonModel;
}());

var yesOrNoModel = /** @class */ (function () {
    function yesOrNoModel() {
    }
    return yesOrNoModel;
}());

var csLabdetailsModel = /** @class */ (function () {
    function csLabdetailsModel() {
    }
    return csLabdetailsModel;
}());
var detailsModel = /** @class */ (function () {
    function detailsModel() {
    }
    return detailsModel;
}());
/*
class curriculumModel
{
    has:string;
    remarks:string;
    details : [];
}
class eLearnModel
{
    has:string;
    remarks:string;
}
class csLabModel
{
    particulars : string;
    available : number;
    repair : number;
    new : number;
}
class libraryNGOModel
{
    has : string;
    remarks : string;
}
class libraryDetailsModel
{
    particulars : string;
    has : string;
}
class scienceLabModel
{
    has: string;
    remarks : particularsOfScienceLab [];
}
class particularsOfScienceLab
{
    particulars : string;
    has: string;
}
class scienceNGOModel
{
    has :string;
    remarks : string;
}
*/ 


/***/ }),

/***/ "./src/app/services/local-store-service.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/services/local-store-service.service.ts ***!
  \*********************************************************/
/*! exports provided: LocalStoreServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocalStoreServiceService", function() { return LocalStoreServiceService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LocalStoreServiceService = /** @class */ (function () {
    function LocalStoreServiceService() {
        this._isSearchSchoolFlow = true;
    }
    Object.defineProperty(LocalStoreServiceService.prototype, "sampleJson", {
        get: function () {
            var tmp = this._jsonData;
            return tmp;
        },
        set: function (jdata) {
            this._jsonData = jdata;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "isSearchSchoolFlow", {
        get: function () {
            var tmp = this._isSearchSchoolFlow;
            return tmp;
        },
        set: function (searchSchoolFlow) {
            this._isSearchSchoolFlow = searchSchoolFlow;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "schoolName", {
        get: function () {
            var tmp = this._schoolName;
            return tmp;
        },
        set: function (schoolProfilePage) {
            this._schoolName = schoolProfilePage;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "schoolId", {
        get: function () {
            var id = this._schoolId;
            return id;
        },
        set: function (id) {
            this._schoolId = id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "schoolProfileBasic", {
        get: function () {
            var tmp = this._schoolProfileBasic;
            return tmp;
        },
        set: function (schoolProfile) {
            this._schoolProfileBasic = schoolProfile;
            console.log(this._schoolProfileBasic.name);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "eduDeptFunction", {
        get: function () {
            var tmp = this._eduDeptFunction;
            return tmp;
        },
        set: function (eduDept) {
            this._eduDeptFunction = eduDept;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "teacherDetails", {
        get: function () {
            var tmp = this._teacherDetails;
            return tmp;
        },
        set: function (teacherDetail) {
            this._teacherDetails = teacherDetail;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "stakeholderInfo", {
        get: function () {
            var tmp = this._stakeholderInfo;
            return tmp;
        },
        set: function (stakeholderInformation) {
            this._stakeholderInfo = stakeholderInformation;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "otherInfo", {
        get: function () {
            var tmp = this._otherInfo;
            return tmp;
        },
        set: function (otherInformation) {
            this._otherInfo = otherInformation;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "healthSanitation", {
        get: function () {
            var tmp = this._healthSanitation;
            return tmp;
        },
        set: function (healthSanitation) {
            this._healthSanitation = healthSanitation;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "classRoomEnvironment", {
        get: function () {
            var tmp = this._classRoomEnvironment;
            return tmp;
        },
        set: function (classRoomEnvironment) {
            this._classRoomEnvironment = classRoomEnvironment;
        },
        enumerable: true,
        configurable: true
    });
    LocalStoreServiceService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], LocalStoreServiceService);
    return LocalStoreServiceService;
}());



/***/ }),

/***/ "./src/app/services/need-assessment/classroom-environment.service.ts":
/*!***************************************************************************!*\
  !*** ./src/app/services/need-assessment/classroom-environment.service.ts ***!
  \***************************************************************************/
/*! exports provided: ClassroomEnvironmentService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClassroomEnvironmentService", function() { return ClassroomEnvironmentService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _util_httpWrapper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../util/httpWrapper */ "./src/util/httpWrapper.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ClassroomEnvironmentService = /** @class */ (function () {
    function ClassroomEnvironmentService(httpClient, alertController) {
        this.httpClient = httpClient;
        this.alertController = alertController;
        this.getClassRoomEnvironmentBySchoolIdUrl = '/getClassroomDetails';
        this.saveClassRoomEnvironmentUrl = '/saveClassroomDetails';
    }
    ClassroomEnvironmentService.prototype.getClassRoomEnvironmentDetailsOfSchool = function (schoolId) {
        return this.httpClient.get(this.getClassRoomEnvironmentBySchoolIdUrl + "?schoolId=" + schoolId);
    };
    ClassroomEnvironmentService.prototype.saveClassRoomEnvironmentDetails = function (classroomEnvironment) {
        var _this = this;
        return this.httpClient.post(this.saveClassRoomEnvironmentUrl, classroomEnvironment)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(function (e) {
            var status = e.status;
            if (status === 401) {
                _this.showAlert('You are not authorized to perform this action');
            }
            else {
                _this.showAlert("Error in saving data,Please try again");
            }
            throw new Error(e);
        }));
    };
    ClassroomEnvironmentService.prototype.getSampleJson = function () {
        var data = {
            "schoolId": 1,
            "computerLab": "Yes",
            "libraryRoom": "Yes",
            "sportsStore": "Yes",
            "playGround": "Yes",
            "sportsMaterial": "Yes",
            "engageArtCraft": "Yes",
            "supportArtCraft": "No",
            "learningMaterial": "Yes",
            "curriculum": {
                "has": "Yes",
                "remarks": "The school observes that each student is assigned a mentor who can help him be motivated",
                "details": []
            },
            "csLabDetail": {
                "has": "Yes",
                "details": [
                    {
                        "name": "Computers",
                        "available": 2,
                        "pristine": 1,
                        "repair": 2,
                        "value": ""
                    },
                    {
                        "name": "Laptops",
                        "available": 2,
                        "pristine": 1,
                        "repair": 2,
                        "value": ""
                    },
                    {
                        "name": "UPS",
                        "available": 2,
                        "pristine": 1,
                        "repair": 2,
                        "value": ""
                    },
                    {
                        "name": "Computer tables",
                        "available": 2,
                        "pristine": 1,
                        "repair": 2,
                        "value": ""
                    },
                    {
                        "name": "Computer chairs",
                        "available": 2,
                        "pristine": 1,
                        "repair": 2,
                        "value": ""
                    },
                    {
                        "name": "Projector",
                        "available": 2,
                        "pristine": 1,
                        "repair": 2,
                        "value": ""
                    },
                    {
                        "name": "Projector Screen",
                        "available": 2,
                        "pristine": 1,
                        "repair": 2,
                        "value": ""
                    },
                    {
                        "name": "Wall Mounted",
                        "available": 2,
                        "pristine": 1,
                        "repair": 2,
                        "value": ""
                    }
                ],
                "remarks": ""
            },
            "eLearn": {
                "has": "Yes",
                "remarks": "JP Morgan",
                "details": []
            },
            "libraryNGO": {
                "has": "Yes",
                "remarks": "No comments",
                "details": []
            },
            "scienceNGO": {
                "has": "Yes",
                "remarks": "JP Morgan",
                "details": []
            },
            "libraryDetail": {
                "has": "Yes",
                "details": [
                    {
                        "name": "Is library arranged level wise",
                        "available": 0,
                        "pristine": 0,
                        "repair": 0,
                        "value": "Yes"
                    },
                    {
                        "name": "Is sufficient cupboards available",
                        "available": 0,
                        "pristine": 0,
                        "repair": 0,
                        "value": "Yes"
                    },
                    {
                        "name": "Is table available",
                        "available": 0,
                        "pristine": 0,
                        "repair": 0,
                        "value": "Yes"
                    },
                    {
                        "name": "Is sufficient chairs/bench available",
                        "available": 0,
                        "pristine": 0,
                        "repair": 0,
                        "value": "Yes"
                    }
                ],
                "remarks": ""
            },
            "scienceLab": {
                "has": "Yes",
                "details": [
                    {
                        "name": "Wall Mounted Table",
                        "available": 0,
                        "pristine": 0,
                        "repair": 0,
                        "value": "Yes"
                    },
                    {
                        "name": "Center Table",
                        "available": 0,
                        "pristine": 0,
                        "repair": 0,
                        "value": "Yes"
                    },
                    {
                        "name": "Chemicals",
                        "available": 0,
                        "pristine": 0,
                        "repair": 0,
                        "value": "Yes"
                    },
                    {
                        "name": "Washbasin",
                        "available": 0,
                        "pristine": 0,
                        "repair": 0,
                        "value": "Yes"
                    },
                    {
                        "name": "Models",
                        "available": 0,
                        "pristine": 0,
                        "repair": 0,
                        "value": "Yes"
                    }
                ],
                remarks: ""
            }
        };
        return data;
    };
    ClassroomEnvironmentService.prototype.showAlert = function (msg) {
        var alert = this.alertController.create({
            message: msg,
            buttons: ['OK']
        });
        alert.then(function (alert) { return alert.present(); });
    };
    ClassroomEnvironmentService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_util_httpWrapper__WEBPACK_IMPORTED_MODULE_1__["HttpWrapper"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]])
    ], ClassroomEnvironmentService);
    return ClassroomEnvironmentService;
}());



/***/ })

}]);
//# sourceMappingURL=classroom-environment-classroom-environment-module.js.map