(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["need-assessment-need-assessment-module"],{

/***/ "./src/app/need-assessment/need-assessment.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/need-assessment/need-assessment.module.ts ***!
  \***********************************************************/
/*! exports provided: NeedAssessmentPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NeedAssessmentPageModule", function() { return NeedAssessmentPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _need_assessment_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./need-assessment.page */ "./src/app/need-assessment/need-assessment.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _need_assessment_page__WEBPACK_IMPORTED_MODULE_5__["NeedAssessmentPage"],
        children: [
            {
                path: 'health-sanitation',
                loadChildren: '../health-sanitation/health-sanitation.module#HealthSanitationPageModule'
            },
            {
                path: 'classroom-environment',
                loadChildren: '../classroom-environment/classroom-environment.module#ClassroomEnvironmentPageModule'
            },
            {
                path: 'school-environment',
                loadChildren: '../school-environment/school-environment.module#SchoolEnvironmentPageModule'
            },
            {
                path: 'need-assessment-status',
                loadChildren: '../need-assessment-status/need-assessment-status.module#NeedAssessmentStatusPageModule'
            }
        ]
    },
    {
        path: '',
        redirectTo: '/need-assessment',
        pathMatch: 'full'
    }
];
var NeedAssessmentPageModule = /** @class */ (function () {
    function NeedAssessmentPageModule() {
    }
    NeedAssessmentPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_need_assessment_page__WEBPACK_IMPORTED_MODULE_5__["NeedAssessmentPage"]]
        })
    ], NeedAssessmentPageModule);
    return NeedAssessmentPageModule;
}());



/***/ }),

/***/ "./src/app/need-assessment/need-assessment.page.html":
/*!***********************************************************!*\
  !*** ./src/app/need-assessment/need-assessment.page.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<!-- <ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>\r\n\t\tNeed Assessment\r\n\t</ion-title>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header> -->\r\n<ion-header>\r\n\t<ion-toolbar>\r\n\t  <ion-buttons slot=\"start\">\r\n\t\t<ion-menu-button></ion-menu-button>\r\n\t  </ion-buttons>\r\n\t  <ion-title>\r\n\t\tNeed Assessment - {{schoolName}}\r\n\t  </ion-title>\r\n\t</ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content padding>\r\n\t<ion-tabs tab=\"school-profile\">\r\n\t\t<ion-tab-bar slot=\"top\" color=\"primary\">\r\n\t    \r\n\t\t    <ion-tab-button tab=\"health-sanitation\">\r\n\t\t      <ion-icon name=\"medkit\"></ion-icon>\r\n\t\t      <ion-label>Health and Sanitation</ion-label>\r\n\t\t    </ion-tab-button>\r\n\r\n\t\t    <ion-tab-button tab=\"classroom-environment\">\r\n\t\t      <ion-icon name=\"contacts\"></ion-icon>\r\n\t\t      <ion-label>Classroom Environment</ion-label>\r\n\t\t    </ion-tab-button>\r\n\r\n\t\t    <ion-tab-button tab=\"school-environment\">\r\n\t\t      <ion-icon name=\"school\"></ion-icon>\r\n\t\t      <ion-label>School Environment</ion-label>\r\n\t\t\t</ion-tab-button>\r\n\r\n\t\t\t<ion-tab-button tab=\"need-assessment-status\">\r\n\t\t\t\t<ion-icon name=\"albums\"></ion-icon>\r\n\t\t\t\t<ion-label>Need Assessment Status</ion-label>\r\n\t\t\t  </ion-tab-button>\r\n\t\t</ion-tab-bar>\r\n\t</ion-tabs>\r\n\t<!-- <ion-tabs>\r\n\t\t<ion-tab-bar slot=\"bottom\">\r\n\t\t    <ion-tab-button tab=\"submit\" color=\"secondary\">\r\n\t\t      <ion-icon name=\"save\"></ion-icon>\r\n\t\t      <ion-label>Submit</ion-label>\r\n\t\t    </ion-tab-button>\r\n\t\t</ion-tab-bar> \r\n\t</ion-tabs>\t -->\t   \t\r\n</ion-content>\r\n\r\n"

/***/ }),

/***/ "./src/app/need-assessment/need-assessment.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/need-assessment/need-assessment.page.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-tab-bar {\n  overflow-x: scroll;\n  -webkit-box-pack: left;\n          justify-content: left;\n  /*height: 80px;*/\n  margin-bottom: -1px; }\n\n::-webkit-scrollbar,\n*::-webkit-scrollbar {\n  display: none; }\n\n.tab-selected {\n  background: #3B5999; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbmVlZC1hc3Nlc3NtZW50L0M6XFx2YXJzaGFcXGN3XFxjYXJld29ya3N1aVxcQ2FyZXdvcmtzL3NyY1xcYXBwXFxuZWVkLWFzc2Vzc21lbnRcXG5lZWQtYXNzZXNzbWVudC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyxrQkFBa0I7RUFDbEIsc0JBQXFCO1VBQXJCLHFCQUFxQjtFQUNyQixnQkFBQTtFQUNBLG1CQUFtQixFQUFBOztBQUdwQjs7RUFFRSxhQUFhLEVBQUE7O0FBR2Y7RUFDQyxtQkFBbUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL25lZWQtYXNzZXNzbWVudC9uZWVkLWFzc2Vzc21lbnQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXRhYi1iYXIge1xyXG5cdG92ZXJmbG93LXg6IHNjcm9sbDtcclxuXHRqdXN0aWZ5LWNvbnRlbnQ6IGxlZnQ7XHJcblx0LypoZWlnaHQ6IDgwcHg7Ki9cclxuXHRtYXJnaW4tYm90dG9tOiAtMXB4O1xyXG59XHJcblxyXG46Oi13ZWJraXQtc2Nyb2xsYmFyLFxyXG4qOjotd2Via2l0LXNjcm9sbGJhciB7XHJcbiAgZGlzcGxheTogbm9uZTtcclxufVxyXG5cclxuLnRhYi1zZWxlY3RlZCB7XHJcblx0YmFja2dyb3VuZDogIzNCNTk5OTtcclxufSBcclxuIl19 */"

/***/ }),

/***/ "./src/app/need-assessment/need-assessment.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/need-assessment/need-assessment.page.ts ***!
  \*********************************************************/
/*! exports provided: NeedAssessmentPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NeedAssessmentPage", function() { return NeedAssessmentPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../app.config */ "./src/app/app.config.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NeedAssessmentPage = /** @class */ (function () {
    function NeedAssessmentPage() {
        this.schoolName = _app_config__WEBPACK_IMPORTED_MODULE_1__["AppConfig"].schoolName;
    }
    NeedAssessmentPage.prototype.ngOnInit = function () {
    };
    NeedAssessmentPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-need-assessment',
            template: __webpack_require__(/*! ./need-assessment.page.html */ "./src/app/need-assessment/need-assessment.page.html"),
            styles: [__webpack_require__(/*! ./need-assessment.page.scss */ "./src/app/need-assessment/need-assessment.page.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], NeedAssessmentPage);
    return NeedAssessmentPage;
}());



/***/ })

}]);
//# sourceMappingURL=need-assessment-need-assessment-module.js.map