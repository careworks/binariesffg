(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["stakeholders-profile-stakeholders-profile-module"],{

/***/ "./src/app/models/stakeholders-profile-model.ts":
/*!******************************************************!*\
  !*** ./src/app/models/stakeholders-profile-model.ts ***!
  \******************************************************/
/*! exports provided: StakeholdersProfileModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StakeholdersProfileModel", function() { return StakeholdersProfileModel; });
var StakeholdersProfileModel = /** @class */ (function () {
    function StakeholdersProfileModel() {
    }
    return StakeholdersProfileModel;
}());



/***/ }),

/***/ "./src/app/stakeholders-profile/stakeholders-profile.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/stakeholders-profile/stakeholders-profile.module.ts ***!
  \*********************************************************************/
/*! exports provided: StakeholdersProfilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StakeholdersProfilePageModule", function() { return StakeholdersProfilePageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _stakeholders_profile_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./stakeholders-profile.page */ "./src/app/stakeholders-profile/stakeholders-profile.page.ts");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/fesm5/ng2-smart-table.js");
/* harmony import */ var _ag_grid_view_edit_mode_ag_grid_view_edit_mode_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../ag-grid-view-edit-mode/ag-grid-view-edit-mode.module */ "./src/app/ag-grid-view-edit-mode/ag-grid-view-edit-mode.module.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var routes = [
    {
        path: '',
        component: _stakeholders_profile_page__WEBPACK_IMPORTED_MODULE_5__["StakeholdersProfilePage"]
    }
];
var StakeholdersProfilePageModule = /** @class */ (function () {
    function StakeholdersProfilePageModule() {
    }
    StakeholdersProfilePageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                ng2_smart_table__WEBPACK_IMPORTED_MODULE_6__["Ng2SmartTableModule"],
                _ag_grid_view_edit_mode_ag_grid_view_edit_mode_module__WEBPACK_IMPORTED_MODULE_7__["AgGridViewEditModeModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatProgressSpinnerModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_stakeholders_profile_page__WEBPACK_IMPORTED_MODULE_5__["StakeholdersProfilePage"]]
        })
    ], StakeholdersProfilePageModule);
    return StakeholdersProfilePageModule;
}());



/***/ }),

/***/ "./src/app/stakeholders-profile/stakeholders-profile.page.html":
/*!*********************************************************************!*\
  !*** ./src/app/stakeholders-profile/stakeholders-profile.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <ion-content padding>\r\n<br><br>\r\n<div [hidden]=\"shouldHideTable\">\r\n<br>\r\n<ion-item class=\"textField\">\r\n\t<ion-button class=\"saveButton\" (click)=\"editFunc()\" block>Edit</ion-button>\r\n\t</ion-item>\r\n<ng2-smart-table [settings]=\"settings1\" [source]=\"source\"></ng2-smart-table>\r\n</div>\r\n<div [hidden]=\"!shouldHideTable\">\r\n<ion-item>\r\n<ion-button class=\"saveContinue\" (click)=\"saveandContinueFunc()\" block>Save and Continue</ion-button>\r\n<ion-button class=\"saveButton\" (click)=\"saveStake()\" block>Save</ion-button>\r\n</ion-item>\r\n<ng2-smart-table [settings]=\"settings\" [source]=\"source\" (deleteConfirm)=\"onDeleteConfirm($event)\" (editConfirm)=\"onSaveConfirm($event)\"\r\n (createConfirm)=\"onCreateConfirm($event)\"></ng2-smart-table>\r\n</div>\r\n</ion-content>\r\n -->\r\n\r\n <ion-card>\r\n\t<div *ngIf=\"!rowData\" style=\"height:400px; display: flex; justify-content: center; align-items: center\">\r\n\t\t<mat-spinner [diameter]=\"50\"></mat-spinner>\r\n\t</div>\r\n\t<div *ngIf=\"rowData\">\r\n\t\t<app-ag-grid-view-edit-mode [columnDefs]=\"columnDefs\" [editGridColumnDefs]=\"editGridColumnDefs\"\r\n\t\t[saveCallback] = \"saveCallback\"\r\n\t\t[saveandContinueCallback]=\"saveandContinueCallback\"\r\n\t\t[rowData]=\"rowData\"></app-ag-grid-view-edit-mode>  \r\n\t</div>\r\n</ion-card>\r\n"

/***/ }),

/***/ "./src/app/stakeholders-profile/stakeholders-profile.page.scss":
/*!*********************************************************************!*\
  !*** ./src/app/stakeholders-profile/stakeholders-profile.page.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/*\r\n.view {\r\ndisplay : none !important;\r\n}\r\ntable { \r\n\t\twidth: 100%; \r\n\t\tborder-collapse: collapse; \r\n\t}\r\n\r\n\ttr:nth-of-type(odd) { \r\n\t\tbackground: #eee; \r\n\t}\r\n\tth { \r\n\t\tbackground: #333; \r\n\t\tcolor: white; \r\n\t\tfont-weight: bold; \r\n\t}\r\n\ttd, th { \r\n\t\tpadding: 6px; \r\n\t\tborder: 1px solid #ccc; \r\n\t\ttext-align: left; \r\n\t}\r\n\r\n\r\n*/\n:host /deep/ ng2-smart-table table {\n  border-style: inset;\n  border-color: initial;\n  -webkit-border-image: initial;\n       -o-border-image: initial;\n          border-image: initial;\n  border-width: 2px;\n  display: table;\n  border-spacing: 2px;\n  border-color: grey; }\n:host /deep/ ng2-smart-table table > tbody > tr > td {\n  box-sizing: border-box;\n  border: 1px solid grey; }\n:host /deep/ ng2-smart-table thead > tr > th {\n  border: 1px solid grey; }\n:host /deep/ ng2-smart-table {\n  font-size: 16px; }\n.saveButton {\n  float: right;\n  width: 100px;\n  height: 30px;\n  font-size: 14px !important;\n  font-weight: 700; }\n.saveContinue {\n  float: right;\n  width: 200px;\n  height: 30px;\n  font-size: 14px !important;\n  font-weight: 700;\n  padding-right: 10px; }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3Rha2Vob2xkZXJzLXByb2ZpbGUvQzpcXHZhcnNoYVxcY3dcXGNhcmV3b3Jrc3VpXFxDYXJld29ya3Mvc3JjXFxhcHBcXHN0YWtlaG9sZGVycy1wcm9maWxlXFxzdGFrZWhvbGRlcnMtcHJvZmlsZS5wYWdlLnNjc3MiLCJzcmMvYXBwL3N0YWtlaG9sZGVycy1wcm9maWxlL3N0YWtlaG9sZGVycy1wcm9maWxlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0NDd0JDO0FEQ0Q7RUFDSSxtQkFBbUI7RUFDbkIscUJBQXFCO0VBQ3JCLDZCQUFxQjtPQUFyQix3QkFBcUI7VUFBckIscUJBQXFCO0VBQ3JCLGlCQUFpQjtFQUVqQixjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLGtCQUFrQixFQUFBO0FBR3RCO0VBQ0ksc0JBQXNCO0VBQ3RCLHNCQUFzQixFQUFBO0FBRzFCO0VBQ0ksc0JBQXNCLEVBQUE7QUFFMUI7RUFDSSxlQUFjLEVBQUE7QUFJbEI7RUFFSSxZQUFZO0VBQ1osWUFBWTtFQUNaLFlBQVk7RUFDWiwwQkFBMEI7RUFDMUIsZ0JBQWdCLEVBQUE7QUFFcEI7RUFFQSxZQUFhO0VBQ2IsWUFBYTtFQUNiLFlBQWE7RUFDYiwwQkFBMkI7RUFDM0IsZ0JBQWdCO0VBQ2hCLG1CQUFtQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvc3Rha2Vob2xkZXJzLXByb2ZpbGUvc3Rha2Vob2xkZXJzLXByb2ZpbGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuLnZpZXcge1xyXG5kaXNwbGF5IDogbm9uZSAhaW1wb3J0YW50O1xyXG59XHJcbnRhYmxlIHsgXHJcblx0XHR3aWR0aDogMTAwJTsgXHJcblx0XHRib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlOyBcclxuXHR9XHJcblxyXG5cdHRyOm50aC1vZi10eXBlKG9kZCkgeyBcclxuXHRcdGJhY2tncm91bmQ6ICNlZWU7IFxyXG5cdH1cclxuXHR0aCB7IFxyXG5cdFx0YmFja2dyb3VuZDogIzMzMzsgXHJcblx0XHRjb2xvcjogd2hpdGU7IFxyXG5cdFx0Zm9udC13ZWlnaHQ6IGJvbGQ7IFxyXG5cdH1cclxuXHR0ZCwgdGggeyBcclxuXHRcdHBhZGRpbmc6IDZweDsgXHJcblx0XHRib3JkZXI6IDFweCBzb2xpZCAjY2NjOyBcclxuXHRcdHRleHQtYWxpZ246IGxlZnQ7IFxyXG5cdH1cclxuXHJcblxyXG4qL1xyXG46aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHRhYmxle1xyXG4gICAgYm9yZGVyLXN0eWxlOiBpbnNldDtcclxuICAgIGJvcmRlci1jb2xvcjogaW5pdGlhbDtcclxuICAgIGJvcmRlci1pbWFnZTogaW5pdGlhbDtcclxuICAgIGJvcmRlci13aWR0aDogMnB4O1xyXG4gICAgXHJcbiAgICBkaXNwbGF5OiB0YWJsZTtcclxuICAgIGJvcmRlci1zcGFjaW5nOiAycHg7XHJcbiAgICBib3JkZXItY29sb3I6IGdyZXk7IFxyXG5cclxufVxyXG46aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHRhYmxlID4gdGJvZHkgPiB0ciA+IHRkIHtcclxuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCBncmV5O1xyXG59XHJcblxyXG46aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHRoZWFkID4gdHIgPiB0aCAgeyBcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIGdyZXk7XHJcbn0gIFxyXG46aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHtcclxuICAgIGZvbnQtc2l6ZToxNnB4O1xyXG59XHJcblxyXG5cclxuLnNhdmVCdXR0b257XHJcbiAgICBcclxuICAgIGZsb2F0OiByaWdodDtcclxuICAgIHdpZHRoOiAxMDBweDtcclxuICAgIGhlaWdodDogMzBweDtcclxuICAgIGZvbnQtc2l6ZTogMTRweCAhaW1wb3J0YW50O1xyXG4gICAgZm9udC13ZWlnaHQ6IDcwMDtcclxufVxyXG4uc2F2ZUNvbnRpbnVle1xyXG5cclxuZmxvYXQgOiByaWdodDtcclxud2lkdGggOiAyMDBweDtcclxuaGVpZ2h0IDogMzBweDtcclxuZm9udC1zaXplIDogMTRweCAhaW1wb3J0YW50O1xyXG5mb250LXdlaWdodCA6NzAwO1xyXG5wYWRkaW5nLXJpZ2h0IDoxMHB4O1xyXG59XHJcblxyXG5cclxuXHJcblxyXG5cclxuIiwiLypcclxuLnZpZXcge1xyXG5kaXNwbGF5IDogbm9uZSAhaW1wb3J0YW50O1xyXG59XHJcbnRhYmxlIHsgXHJcblx0XHR3aWR0aDogMTAwJTsgXHJcblx0XHRib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlOyBcclxuXHR9XHJcblxyXG5cdHRyOm50aC1vZi10eXBlKG9kZCkgeyBcclxuXHRcdGJhY2tncm91bmQ6ICNlZWU7IFxyXG5cdH1cclxuXHR0aCB7IFxyXG5cdFx0YmFja2dyb3VuZDogIzMzMzsgXHJcblx0XHRjb2xvcjogd2hpdGU7IFxyXG5cdFx0Zm9udC13ZWlnaHQ6IGJvbGQ7IFxyXG5cdH1cclxuXHR0ZCwgdGggeyBcclxuXHRcdHBhZGRpbmc6IDZweDsgXHJcblx0XHRib3JkZXI6IDFweCBzb2xpZCAjY2NjOyBcclxuXHRcdHRleHQtYWxpZ246IGxlZnQ7IFxyXG5cdH1cclxuXHJcblxyXG4qL1xuOmhvc3QgL2RlZXAvIG5nMi1zbWFydC10YWJsZSB0YWJsZSB7XG4gIGJvcmRlci1zdHlsZTogaW5zZXQ7XG4gIGJvcmRlci1jb2xvcjogaW5pdGlhbDtcbiAgYm9yZGVyLWltYWdlOiBpbml0aWFsO1xuICBib3JkZXItd2lkdGg6IDJweDtcbiAgZGlzcGxheTogdGFibGU7XG4gIGJvcmRlci1zcGFjaW5nOiAycHg7XG4gIGJvcmRlci1jb2xvcjogZ3JleTsgfVxuXG46aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHRhYmxlID4gdGJvZHkgPiB0ciA+IHRkIHtcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgYm9yZGVyOiAxcHggc29saWQgZ3JleTsgfVxuXG46aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHRoZWFkID4gdHIgPiB0aCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkIGdyZXk7IH1cblxuOmhvc3QgL2RlZXAvIG5nMi1zbWFydC10YWJsZSB7XG4gIGZvbnQtc2l6ZTogMTZweDsgfVxuXG4uc2F2ZUJ1dHRvbiB7XG4gIGZsb2F0OiByaWdodDtcbiAgd2lkdGg6IDEwMHB4O1xuICBoZWlnaHQ6IDMwcHg7XG4gIGZvbnQtc2l6ZTogMTRweCAhaW1wb3J0YW50O1xuICBmb250LXdlaWdodDogNzAwOyB9XG5cbi5zYXZlQ29udGludWUge1xuICBmbG9hdDogcmlnaHQ7XG4gIHdpZHRoOiAyMDBweDtcbiAgaGVpZ2h0OiAzMHB4O1xuICBmb250LXNpemU6IDE0cHggIWltcG9ydGFudDtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgcGFkZGluZy1yaWdodDogMTBweDsgfVxuIl19 */"

/***/ }),

/***/ "./src/app/stakeholders-profile/stakeholders-profile.page.ts":
/*!*******************************************************************!*\
  !*** ./src/app/stakeholders-profile/stakeholders-profile.page.ts ***!
  \*******************************************************************/
/*! exports provided: StakeholdersProfilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StakeholdersProfilePage", function() { return StakeholdersProfilePage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_stakeholders_profile_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../models/stakeholders-profile-model */ "./src/app/models/stakeholders-profile-model.ts");
/* harmony import */ var _services_school_profile_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/school-profile.service */ "./src/app/services/school-profile.service.ts");
/* harmony import */ var _services_local_store_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/local-store-service.service */ "./src/app/services/local-store-service.service.ts");
/* harmony import */ var _util_httpWrapper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../util/httpWrapper */ "./src/util/httpWrapper.ts");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/fesm5/ng2-smart-table.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var StakeholdersProfilePage = /** @class */ (function () {
    function StakeholdersProfilePage(httpClient, schoolProfileService, localStoreService, router) {
        this.httpClient = httpClient;
        this.schoolProfileService = schoolProfileService;
        this.localStoreService = localStoreService;
        this.router = router;
        // Testing Smart Tables 
        this.settings = {
            defaultStyle: false,
            actions: {
                position: 'right',
            },
            add: {
                addButtonContent: 'Add     ',
                createButtonContent: 'Create    ',
                cancelButtonContent: '    Cancel',
                confirmCreate: true,
            },
            edit: {
                editButtonContent: 'Edit  ',
                saveButtonContent: 'Update    ',
                cancelButtonContent: '    Cancel',
                confirmSave: true,
            },
            delete: {
                deleteButtonContent: '	Delete',
                confirmDelete: true,
            },
            columns: {
                stakeHolderName: {
                    title: 'Stakeholder', filter: false
                },
                memberCount: {
                    title: 'Total Members', filter: false
                },
                formationYear: {
                    title: 'Year Of Formation', filter: false
                },
                spocName: {
                    title: 'SPOC Name', filter: false
                },
                contactNumber: {
                    title: 'Contact Number', filter: false
                }
            },
            attr: {
                class: 'table table-bordered table-striped'
            }
        };
        this.settings1 = {
            defaultStyle: false,
            actions: false,
            hideSubHeader: true,
            columns: {
                stakeHolderName: {
                    title: 'Stakeholder', filter: false
                },
                memberCount: {
                    title: 'Total Members', filter: false
                },
                formationYear: {
                    title: 'Year Of Formation', filter: false
                },
                spocName: {
                    title: 'SPOC Name', filter: false
                },
                contactNumber: {
                    title: 'Contact Number', filter: false
                }
            },
            attr: {
                class: 'table table-bordered table-striped'
            }
        };
        this.parsedschoolDetailsJson = null;
        this.size = null;
        this.sdmcTotal = null;
        this.sdmcYear = null;
        this.sdmcSpoc = null;
        this.sdmcContact = null;
        this.alumniTotal = null;
        this.alumniYear = null;
        this.alumniSpoc = null;
        this.alumniContact = null;
        this.schoolCabinetTotal = null;
        this.schoolCabinetYear = null;
        this.schoolCabinetSpoc = null;
        this.schoolCabinetContact = null;
        this.parentsTeachersTotal = null;
        this.parentsTeachersYear = null;
        this.parentsTeachersSpoc = null;
        this.parentsTeachersContact = null;
        this.pocsoTotal = null;
        this.pocsoYear = null;
        this.pocsoSpoc = null;
        this.pocsoContact = null;
        this.stakeholdersList = [];
        this.st = new _models_stakeholders_profile_model__WEBPACK_IMPORTED_MODULE_1__["StakeholdersProfileModel"]();
        this.stakeholders = null;
        this.shouldHideTable = true;
    }
    StakeholdersProfilePage.prototype.onDeleteConfirm = function (event) {
        console.log("Delete Event In Console");
        console.log(event);
        if (window.confirm('Are you sure you want to delete?')) {
            event.confirm.resolve();
        }
        else {
            event.confirm.reject();
        }
    };
    StakeholdersProfilePage.prototype.onCreateConfirm = function (event) {
        console.log("Create Event In Console");
        console.log(event);
        if (window.confirm('Are you sure you want to add new entry?')) {
            event.confirm.resolve();
        }
        else {
            event.confirm.reject();
        }
    };
    StakeholdersProfilePage.prototype.onSaveConfirm = function (event) {
        console.log("Edit Event In Console");
        console.log(event);
        if (window.confirm('Are you sure you want to update?')) {
            event.confirm.resolve();
        }
        else {
            event.confirm.reject();
        }
    };
    StakeholdersProfilePage.prototype.getEditGridColumnDefs = function () {
        this.editGridColumnDefs = [
            { headerName: '', checkboxSelection: true, pinned: 'left', width: 50 },
            { headerName: 'Stakeholder', field: 'stakeHolderName', tooltipField: 'stakeHolderName', editable: true },
            { headerName: 'Total Members', field: 'memberCount', tooltipField: 'memberCount', editable: true },
            { headerName: 'Year Of Formation', field: 'formationYear', tooltipField: 'formationYear', editable: true },
            { headerName: 'SPOC Name', field: 'spocName', tooltipField: 'spocName', editable: true },
            { headerName: 'Contact Number', field: 'contactNumber', tooltipField: 'spocName', editable: true }
        ];
    };
    StakeholdersProfilePage.prototype.getColumnDefs = function () {
        this.columnDefs = [
            { headerName: 'Stakeholder', field: 'stakeHolderName', tooltipField: 'stakeHolderName' },
            { headerName: 'Total Members', field: 'memberCount', tooltipField: 'memberCount' },
            { headerName: 'Year Of Formation', field: 'formationYear', tooltipField: 'formationYear' },
            { headerName: 'SPOC Name', field: 'spocName', tooltipField: 'spocName' },
            { headerName: 'Contact Number', field: 'contactNumber', tooltipField: 'spocName' }
        ];
    };
    StakeholdersProfilePage.prototype.saveCallback = function (data) {
        console.log("stakeholder save callback", data);
    };
    StakeholdersProfilePage.prototype.saveandContinueCallback = function (data) {
        console.log("stakeholder save and continue callback", data);
    };
    StakeholdersProfilePage.prototype.getRowData = function () {
        //this.rowData = this.httpClient.get('https://api.myjson.com/bins/15psn9');
        this.rowData = [
            { id: '1', activity: 'act1', stakeholder: 'test', resource: 'Money', responsible: 'CWF', deadline: '29-04-2019' },
            { id: '2', activity: 'act1', stakeholder: 'test', resource: 'Money', responsible: 'CWF', deadline: '29-04-2019' },
            { id: '3', activity: 'act1', stakeholder: 'test', resource: 'Money', responsible: 'CWF', deadline: '29-04-2019' }
        ];
    };
    StakeholdersProfilePage.prototype.ngOnInit = function () {
        var _this = this;
        this.getRowData();
        this.getColumnDefs();
        this.getEditGridColumnDefs();
        // Testing Code for Smart Table
        this.data = [];
        this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_5__["LocalDataSource"](this.data);
        if (this.localStoreService.isSearchSchoolFlow) {
            this.shouldHideTable = false;
            //  this.initialisationTextBox();
            this.data = this.localStoreService.sampleJson.schoolAdvancedProfile.stakeholderInfo;
            this.source.load(this.data);
            console.log(this.data);
            this.school_Id = this.localStoreService.sampleJson.schoolProfileBasic.schoolId;
        }
        else {
            this.shouldHideTable = true;
            this.httpClient.get("/school/SchoolProfilesByName/?name=" + this.localStoreService.schoolName).subscribe(function (res) {
                console.log(res[0]);
                _this.schoolByName = JSON.parse(JSON.stringify(res[0]));
            });
            this.source.load(this.data);
        }
    };
    StakeholdersProfilePage.prototype.saveStake = function () {
        var _this = this;
        this.source.getAll().then(function (res) {
            console.log(JSON.stringify(res));
            _this.httpClient.put("/school/stakeHolders/?schoolId=" + _this.schoolByName.school_id, JSON.stringify(res))
                .subscribe(function (data) {
                console.log(data);
            });
        });
        this.shouldHideTable = false;
    };
    StakeholdersProfilePage.prototype.saveandContinueFunc = function () {
        var _this = this;
        this.source.getAll().then(function (res) {
            console.log(JSON.stringify(res));
            _this.httpClient.put("/school/stakeHolders/?schoolId=" + _this.schoolByName.school_id, JSON.stringify(res))
                .subscribe(function (data) {
                console.log(data);
            });
        });
        // Move To Next Tab
        this.router.navigate(['../school-info/other-info']);
    };
    StakeholdersProfilePage.prototype.initialisationTextBox = function () {
        this.parsedschoolDetailsJson = this.localStoreService.sampleJson.schoolAdvancedProfile.stakeholderInfo;
        this.size = this.parsedschoolDetailsJson.length;
        console.log(this.parsedschoolDetailsJson[0]);
        // Can iterate over the list at later stage
        this.sdmcTotal = parseInt(this.parsedschoolDetailsJson[0].memberCount);
        this.sdmcYear = this.parsedschoolDetailsJson[0].formationYear.substr(0, 4);
        this.sdmcSpoc = this.parsedschoolDetailsJson[0].spocName;
        this.sdmcContact = this.parsedschoolDetailsJson[0].contactNumber;
        this.alumniTotal = parseInt(this.parsedschoolDetailsJson[1].memberCount);
        this.alumniYear = this.parsedschoolDetailsJson[1].formationYear.substr(0, 4);
        this.alumniSpoc = this.parsedschoolDetailsJson[1].spocName;
        this.alumniContact = this.parsedschoolDetailsJson[1].contactNumber;
        //Mapping to Model Fields
        this.st.sdmcTotal = this.sdmcTotal;
        this.st.sdmcYear = this.sdmcYear;
        this.st.sdmcSpoc = this.sdmcSpoc;
        this.st.sdmcContact = this.sdmcContact;
        this.st.alumniTotal = this.alumniTotal;
        this.st.alumniSpoc = this.alumniSpoc;
        this.st.alumniContact = this.alumniContact;
        this.st.alumniYear = this.alumniYear;
        this.st.schoolCabinetTotal = this.schoolCabinetTotal;
        this.st.schoolCabinetSpoc = this.schoolCabinetSpoc;
        this.st.schoolCabinetYear = this.schoolCabinetYear;
        this.st.schoolCabinetContact = this.schoolCabinetContact;
        this.st.parentsTeachersTotal = this.parentsTeachersYear;
        this.st.parentsTeachersSpoc = this.parentsTeachersSpoc;
        this.st.parentsTeachersContact = this.parentsTeachersContact;
        this.st.pocsoTotal = this.pocsoTotal;
        this.st.pocsoYear = this.pocsoYear;
        this.st.pocsoSpoc = this.pocsoSpoc;
        this.st.pocsoContact = this.pocsoContact;
    };
    StakeholdersProfilePage.prototype.getStakeholdersList = function () {
        return this.stakeholdersList;
    };
    StakeholdersProfilePage.prototype.addStakeholder = function (stakeholder) {
        this.stakeholdersList.push(stakeholder);
    };
    StakeholdersProfilePage.prototype.editFunc = function () {
        this.shouldHideTable = true;
    };
    StakeholdersProfilePage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-stakeholders-profile',
            template: __webpack_require__(/*! ./stakeholders-profile.page.html */ "./src/app/stakeholders-profile/stakeholders-profile.page.html"),
            styles: [__webpack_require__(/*! ./stakeholders-profile.page.scss */ "./src/app/stakeholders-profile/stakeholders-profile.page.scss")]
        }),
        __metadata("design:paramtypes", [_util_httpWrapper__WEBPACK_IMPORTED_MODULE_4__["HttpWrapper"], _services_school_profile_service__WEBPACK_IMPORTED_MODULE_2__["SchoolProfileService"], _services_local_store_service_service__WEBPACK_IMPORTED_MODULE_3__["LocalStoreServiceService"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]])
    ], StakeholdersProfilePage);
    return StakeholdersProfilePage;
}());



/***/ })

}]);
//# sourceMappingURL=stakeholders-profile-stakeholders-profile-module.js.map