(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["action-plan-action-plan-module"],{

/***/ "./src/app/action-plan-tab-content/action-plan-tab-content.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/action-plan-tab-content/action-plan-tab-content.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-card>\r\n    <mat-button-toggle-group [(ngModel)]=\"isViewMode\" (change)=\"onModeChanged()\" appearance=\"legacy\" style=\"margin-top: 5px;margin-left:8px\">\r\n        <mat-button-toggle [value]=\"true\" > View </mat-button-toggle>\r\n        <mat-button-toggle [value]=\"false\"> Edit </mat-button-toggle>       \r\n    </mat-button-toggle-group>\r\n  \r\n    <ion-card [hidden]=\"isViewMode\">\r\n          <ion-button color=\"light\"(click)=\"deleteSelectedRows()\" [disabled]=\"!rowsSelected()\" style=\"height: 20px\">Delete</ion-button> \r\n          <ion-button color=\"light\"(click)=\"insertNewRow()\" style=\"height: 20px\">Insert New Row</ion-button> \r\n          <ag-grid-angular\r\n          #agGridEdit\r\n          style=\"height: 400px;\" \r\n          class=\"ag-theme-balham\"\r\n          [components]=\"components\"\r\n          [columnDefs]=\"editGridColumnDefs\"\r\n          [gridOptions]=\"gridOptions\"\r\n          suppressRowClickSelection\r\n          singleClickEdit\r\n          rowSelection=\"multiple\"\r\n          (gridReady)=\"onEditGridReady($event)\">\r\n          </ag-grid-angular>\r\n          <div style=\"text-align: center\">\r\n            <ion-button color=\"primary\" style=\"display: inline-block\" (click)=\"submitDetails()\">Submit</ion-button>      \r\n            <ion-button color=\"light\" style=\"display: inline-block\" (click)=\"cancel()\">Cancel</ion-button>          \r\n          </div>\r\n    </ion-card>\r\n    \r\n    <ion-card [hidden]=\"!isViewMode\">\r\n        <ion-card-content>\r\n            <ag-grid-angular\r\n            #agGrid\r\n            style=\"height: 400px;\" \r\n            class=\"ag-theme-balham\"\r\n            [components]=\"components\"\r\n            [columnDefs]=\"columnDefs\"          \r\n            suppressRowClickSelection\r\n            rowSelection=\"multiple\"\r\n            (gridReady)=\"onGridReady($event)\"\r\n            >\r\n            </ag-grid-angular>\r\n          </ion-card-content>\r\n          </ion-card>\r\n</ion-card>\r\n\r\n"

/***/ }),

/***/ "./src/app/action-plan-tab-content/action-plan-tab-content.component.scss":
/*!********************************************************************************!*\
  !*** ./src/app/action-plan-tab-content/action-plan-tab-content.component.scss ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FjdGlvbi1wbGFuLXRhYi1jb250ZW50L2FjdGlvbi1wbGFuLXRhYi1jb250ZW50LmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/action-plan-tab-content/action-plan-tab-content.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/action-plan-tab-content/action-plan-tab-content.component.ts ***!
  \******************************************************************************/
/*! exports provided: ActionPlanTabContentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActionPlanTabContentComponent", function() { return ActionPlanTabContentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _action_plan_actionplan_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../action-plan/actionplan.service */ "./src/app/action-plan/actionplan.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ActionPlanTabContentComponent = /** @class */ (function () {
    function ActionPlanTabContentComponent(svc) {
        this.svc = svc;
        this.components = { datePicker: getDatePicker(), multiSelectPicker: getMultiSelectPicker() };
    }
    ActionPlanTabContentComponent.prototype.ngOnInit = function () {
        this.isViewMode = true;
        this.gridOptions = {};
        this.getActivities();
        this.getResources();
        this.getStakeHolders();
        this.getColumnDefs();
        this.getEditGridColumnDefs();
        // this.svc.getActionTabService().subscribe(data => {
        //   this.actionTabData = data;
        // });
    };
    ActionPlanTabContentComponent.prototype.getEditGridColumnDefs = function () {
        this.editGridColumnDefs = [
            { headerName: '', checkboxSelection: true, pinned: 'left', width: 50 },
            { headerName: 'Activity', field: 'activity', tooltipField: 'activity', editable: true, cellEditor: 'agSelectCellEditor',
                cellEditorParams: { values: this.activitiesList.map(function (x) { return x.activity_detail; }) } },
            { headerName: 'Stakeholder', field: 'stakeholder', tooltipField: 'stakeholder', editable: true, cellEditor: 'multiSelectPicker',
                cellEditorParams: { values: this.stakeHoldersList.map(function (x) { return x.name; }) } },
            { headerName: 'Resource', field: 'resource', tooltipField: 'resource', editable: true, cellEditor: 'multiSelectPicker',
                cellEditorParams: { values: this.resourcesList.map(function (x) { return x.name; }) } },
            { headerName: 'Responsible', field: 'responsible', tooltipField: 'responsible', editable: true },
            { headerName: 'Deadline', field: 'deadline', tooltipField: 'deadline', editable: true, cellEditor: 'datePicker' }
        ];
    };
    ActionPlanTabContentComponent.prototype.getEditGridRowData = function () {
        return [
            { id: '1', activity: 'Science Lab', stakeholder: 'test', resource: 'Money', responsible: 'CWF', deadline: '29-04-2019' },
            { id: '2', activity: 'Toilet Building', stakeholder: 'test', resource: 'Money', responsible: 'CWF', deadline: '29-04-2019' },
            { id: '3', activity: 'Dental Camp', stakeholder: 'test', resource: 'Money', responsible: 'CWF', deadline: '29-04-2019' }
        ];
    };
    ActionPlanTabContentComponent.prototype.onEditGridReady = function (params) {
        this.editGridApi = params.api;
        this.editGridColumnApi = params.columnApi;
        var editGridData = this.getEditGridRowData();
        this.editGridApi.setRowData(editGridData);
        this.editGridApi.sizeColumnsToFit();
    };
    ActionPlanTabContentComponent.prototype.getColumnDefs = function () {
        this.columnDefs = [
            { headerName: 'Activity', field: 'activity', tooltipField: 'activity' },
            { headerName: 'Stakeholder', field: 'stakeholder', tooltipField: 'stakeholder' },
            { headerName: 'Resource', field: 'resource', tooltipField: 'resource' },
            { headerName: 'Responsible', field: 'responsible', tooltipField: 'responsible' },
            { headerName: 'Deadline', field: 'deadline', tooltipField: 'deadline' }
        ];
    };
    ActionPlanTabContentComponent.prototype.getRowData = function () {
        return [
            { id: '1', activity: 'act1', stakeholder: 'test', resource: 'Money', responsible: 'CWF', deadline: '29-04-2019' },
            { id: '2', activity: 'act1', stakeholder: 'test', resource: 'Money', responsible: 'CWF', deadline: '29-04-2019' },
            { id: '3', activity: 'act1', stakeholder: 'test', resource: 'Money', responsible: 'CWF', deadline: '29-04-2019' }
        ];
    };
    ActionPlanTabContentComponent.prototype.onGridReady = function (params) {
        this.api = params.api;
        this.columnApi = params.columnApi;
        this.api.sizeColumnsToFit();
        var rowData = this.getRowData();
        this.api.setRowData(rowData);
    };
    ActionPlanTabContentComponent.prototype.getActivities = function () {
        this.activitiesList = this.svc.getActivitiesForCategory(this.id);
    };
    ActionPlanTabContentComponent.prototype.getResources = function () {
        this.resourcesList = this.svc.getResourcesForCategory(this.id);
    };
    ActionPlanTabContentComponent.prototype.getStakeHolders = function () {
        this.stakeHoldersList = this.svc.getStakeHoldersForCategory(this.id);
    };
    ActionPlanTabContentComponent.prototype.rowsSelected = function () {
        return this.editGridApi && this.editGridApi.getSelectedRows().length > 0;
    };
    ActionPlanTabContentComponent.prototype.deleteSelectedRows = function () {
        var selectRows = this.editGridApi.getSelectedRows();
        this.editGridApi.updateRowData({ remove: selectRows });
    };
    ActionPlanTabContentComponent.prototype.insertNewRow = function () {
        var tempData = { id: '4', activity: 'act1', stakeholder: 'test', resource: 'Money', responsible: 'CWF', deadline: '29-04-2019' };
        this.editGridApi.updateRowData({ add: [tempData] });
    };
    ActionPlanTabContentComponent.prototype.submitDetails = function () {
    };
    ActionPlanTabContentComponent.prototype.cancel = function () {
        this.isViewMode = true;
    };
    ActionPlanTabContentComponent.prototype.onModeChanged = function () {
        if (this.isViewMode) {
            var data = this.getRowData();
            this.api.setRowData(data);
            this.api.sizeColumnsToFit();
        }
        else {
            var editData = this.getEditGridRowData();
            this.editGridApi.setRowData(editData);
            this.editGridApi.sizeColumnsToFit();
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ActionPlanTabContentComponent.prototype, "id", void 0);
    ActionPlanTabContentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-action-plan-tab-content',
            template: __webpack_require__(/*! ./action-plan-tab-content.component.html */ "./src/app/action-plan-tab-content/action-plan-tab-content.component.html"),
            styles: [__webpack_require__(/*! ./action-plan-tab-content.component.scss */ "./src/app/action-plan-tab-content/action-plan-tab-content.component.scss")]
        }),
        __metadata("design:paramtypes", [_action_plan_actionplan_service__WEBPACK_IMPORTED_MODULE_1__["ActionplanService"]])
    ], ActionPlanTabContentComponent);
    return ActionPlanTabContentComponent;
}());

function getDatePicker() {
    function Datepicker() { }
    Datepicker.prototype.init = function (params) {
        this.eInput = document.createElement("input");
        console.log('params', params);
        this.eInput.value = params.value;
        $(this.eInput).datepicker({ dateFormat: "dd/mm/yy" });
    };
    Datepicker.prototype.getGui = function () {
        return this.eInput;
    };
    Datepicker.prototype.afterGuiAttached = function () {
        this.eInput.focus();
        this.eInput.select();
    };
    Datepicker.prototype.getValue = function () {
        console.log("getValue", this.eInput.value);
        return this.eInput.value;
    };
    Datepicker.prototype.destroy = function () { };
    Datepicker.prototype.isPopup = function () {
        return false;
    };
    return Datepicker;
}
function getMultiSelectPicker() {
    function MultiSelectPicker() { }
    MultiSelectPicker.prototype.init = function (params) {
        var _this = this;
        console.log('params', params);
        this.eSelect = document.createElement("select");
        $(this.eSelect).height('120px');
        $(this.eSelect).width('250px');
        params.values.forEach(function (element) {
            var eOption = document.createElement("option");
            eOption.text = element;
            eOption.value = element;
            _this.eSelect.appendChild(eOption);
        });
        this.eSelect.setAttribute('multiple', '');
    };
    MultiSelectPicker.prototype.getGui = function () {
        return this.eSelect;
    };
    MultiSelectPicker.prototype.afterGuiAttached = function () {
        this.eSelect.focus();
    };
    MultiSelectPicker.prototype.getValue = function () {
        var selectedValues = $(this.eSelect).val();
        console.log("getValue", selectedValues);
        return selectedValues;
    };
    MultiSelectPicker.prototype.destroy = function () { };
    MultiSelectPicker.prototype.isPopup = function () {
        return true;
    };
    return MultiSelectPicker;
}


/***/ }),

/***/ "./src/app/action-plan/action-plan.module.ts":
/*!***************************************************!*\
  !*** ./src/app/action-plan/action-plan.module.ts ***!
  \***************************************************/
/*! exports provided: ActionPlanPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActionPlanPageModule", function() { return ActionPlanPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/tabs */ "./node_modules/@angular/material/esm5/tabs.es5.js");
/* harmony import */ var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/slide-toggle */ "./node_modules/@angular/material/esm5/slide-toggle.es5.js");
/* harmony import */ var _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/button-toggle */ "./node_modules/@angular/material/esm5/button-toggle.es5.js");
/* harmony import */ var ag_grid_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ag-grid-angular */ "./node_modules/ag-grid-angular/main.js");
/* harmony import */ var ag_grid_angular__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(ag_grid_angular__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _action_plan_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./action-plan.page */ "./src/app/action-plan/action-plan.page.ts");
/* harmony import */ var _action_plan_tab_content_action_plan_tab_content_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../action-plan-tab-content/action-plan-tab-content.component */ "./src/app/action-plan-tab-content/action-plan-tab-content.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var routes = [
    {
        path: '',
        component: _action_plan_page__WEBPACK_IMPORTED_MODULE_9__["ActionPlanPage"]
    }
];
var ActionPlanPageModule = /** @class */ (function () {
    function ActionPlanPageModule() {
    }
    ActionPlanPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_material_tabs__WEBPACK_IMPORTED_MODULE_5__["MatTabsModule"],
                _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_6__["MatSlideToggleModule"],
                _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_7__["MatButtonToggleModule"],
                ag_grid_angular__WEBPACK_IMPORTED_MODULE_8__["AgGridModule"].withComponents([]),
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)
            ],
            declarations: [_action_plan_page__WEBPACK_IMPORTED_MODULE_9__["ActionPlanPage"], _action_plan_tab_content_action_plan_tab_content_component__WEBPACK_IMPORTED_MODULE_10__["ActionPlanTabContentComponent"]]
        })
    ], ActionPlanPageModule);
    return ActionPlanPageModule;
}());



/***/ }),

/***/ "./src/app/action-plan/action-plan.page.html":
/*!***************************************************!*\
  !*** ./src/app/action-plan/action-plan.page.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-menu-button></ion-menu-button>\r\n    </ion-buttons>\r\n    <ion-title>\r\n      Action Plan - {{schoolName}}\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content padding>\r\n    <mat-tab-group  [disableRipple]=\"true\">\r\n       \r\n            <mat-tab label={{env.categoryName}} *ngFor=\"let env of environmentData\">\r\n                <app-action-plan-tab-content id={{env.categoryId}}></app-action-plan-tab-content>\r\n            </mat-tab>\r\n      </mat-tab-group>\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/action-plan/action-plan.page.scss":
/*!***************************************************!*\
  !*** ./src/app/action-plan/action-plan.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FjdGlvbi1wbGFuL2FjdGlvbi1wbGFuLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/action-plan/action-plan.page.ts":
/*!*************************************************!*\
  !*** ./src/app/action-plan/action-plan.page.ts ***!
  \*************************************************/
/*! exports provided: ActionPlanPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActionPlanPage", function() { return ActionPlanPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _actionplan_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./actionplan.service */ "./src/app/action-plan/actionplan.service.ts");
/* harmony import */ var _app_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../app.config */ "./src/app/app.config.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ActionPlanPage = /** @class */ (function () {
    function ActionPlanPage(svc) {
        this.svc = svc;
        this.schoolName = _app_config__WEBPACK_IMPORTED_MODULE_2__["AppConfig"].schoolName;
    }
    ActionPlanPage.prototype.ngOnInit = function () {
        this.environmentData = this.svc.getEnvironments();
        console.log('this.environmentData', this.environmentData);
    };
    ActionPlanPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-action-plan',
            template: __webpack_require__(/*! ./action-plan.page.html */ "./src/app/action-plan/action-plan.page.html"),
            styles: [__webpack_require__(/*! ./action-plan.page.scss */ "./src/app/action-plan/action-plan.page.scss")]
        }),
        __metadata("design:paramtypes", [_actionplan_service__WEBPACK_IMPORTED_MODULE_1__["ActionplanService"]])
    ], ActionPlanPage);
    return ActionPlanPage;
}());



/***/ }),

/***/ "./src/app/action-plan/actionplan.service.ts":
/*!***************************************************!*\
  !*** ./src/app/action-plan/actionplan.service.ts ***!
  \***************************************************/
/*! exports provided: ActionplanService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActionplanService", function() { return ActionplanService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
        'Content-Type': 'application/json',
        'Authorization': 'my-auth-token'
    })
};
var ActionplanService = /** @class */ (function () {
    function ActionplanService(http) {
        this.http = http;
        this.apiRootUrl = 'http://careworksvc-env-1.nazwrxkp7x.ap-east-1.elasticbeanstalk.com:8080';
        this.getRowDataUrl = this.apiRootUrl + "/actionplan/getActionMasterDetails/";
        this.saveRowDataUrl = this.apiRootUrl + "/actionplan/saveActionMaster/";
        this.dummyData = [
            { categoryId: 1, categoryName: 'School Environment' },
            { categoryId: 2, categoryName: 'ClassRoom Environment' },
            { categoryId: 3, categoryName: 'Health & Sanitation' },
            { categoryId: 4, categoryName: 'Stakeholder Involvement' },
            { categoryId: 5, categoryName: 'Other Activities' }
        ];
        this.rowData = [
            { id: '1', activity: 'act1', stakeholder: 'test', resource: 'Money', responsible: 'CWF', deadline: '29-04-2019' },
            { id: '2', activity: 'act1', stakeholder: 'test', resource: 'Money', responsible: 'CWF', deadline: '29-04-2019' },
            { id: '3', activity: 'act1', stakeholder: 'test', resource: 'Money', responsible: 'CWF', deadline: '29-04-2019' }
        ];
        this.activityList = [
            { activityId: 0, activity_detail: 'Act1' },
            { activityId: 1, activity_detail: 'Act2' },
            { activityId: 2, activity_detail: 'Act3' },
            { activityId: 3, activity_detail: 'Act4' },
            { activityId: 4, activity_detail: 'Act5' }
        ];
        this.stakeHoldersList = [
            { id: 0, name: 'HM' },
            { id: 1, name: 'SDMC' },
            { id: 2, name: 'CWF' },
            { id: 3, name: 'CWF1' },
            { id: 4, name: 'CWF2' },
            { id: 4, name: 'CWF3' }
        ];
        this.resourcesList = [
            { id: 0, name: 'Money' },
            { id: 1, name: 'Kind' },
            { id: 2, name: 'Human Resource' }
        ];
    }
    ActionplanService.prototype.getEnvironments = function () {
        return this.dummyData;
    };
    ActionplanService.prototype.getRowData = function (categoryId) {
        var categorySpecificUrl = this.getRowDataUrl + categoryId + "&" + "0";
        this.http.get(categorySpecificUrl).subscribe(function (res) {
            console.log("action plan service getRowData", res);
            console.log("action plan service getRowData", res['activityResponseList']);
            if (res && res['activityResponseList']) {
                return res['activityResponseList'];
            }
            return [];
        });
    };
    ActionplanService.prototype.saveRowData = function (categoryId, rowData) {
        var data = JSON.stringify(rowData);
        console.log('data to be posted', data);
        this.http.put(this.saveRowDataUrl, data[0], httpOptions).subscribe(function (res) {
            console.log("action plan service getRowData", res);
        });
    };
    ActionplanService.prototype.getActivitiesForCategory = function (categoryId) {
        return this.activityList;
    };
    ActionplanService.prototype.getStakeHoldersForCategory = function (categoryId) {
        return this.stakeHoldersList;
    };
    ActionplanService.prototype.getResourcesForCategory = function (categoryId) {
        return this.resourcesList;
    };
    ActionplanService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ActionplanService);
    return ActionplanService;
}());



/***/ })

}]);
//# sourceMappingURL=action-plan-action-plan-module.js.map