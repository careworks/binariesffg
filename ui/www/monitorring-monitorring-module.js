(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["monitorring-monitorring-module"],{

/***/ "./src/app/monitorring/monitorring.module.ts":
/*!***************************************************!*\
  !*** ./src/app/monitorring/monitorring.module.ts ***!
  \***************************************************/
/*! exports provided: MonitorringPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MonitorringPageModule", function() { return MonitorringPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _monitorring_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./monitorring.page */ "./src/app/monitorring/monitorring.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _monitorring_page__WEBPACK_IMPORTED_MODULE_5__["MonitorringPage"]
    }
];
var MonitorringPageModule = /** @class */ (function () {
    function MonitorringPageModule() {
    }
    MonitorringPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_monitorring_page__WEBPACK_IMPORTED_MODULE_5__["MonitorringPage"]]
        })
    ], MonitorringPageModule);
    return MonitorringPageModule;
}());



/***/ }),

/***/ "./src/app/monitorring/monitorring.page.html":
/*!***************************************************!*\
  !*** ./src/app/monitorring/monitorring.page.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>monitorring</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content padding>\r\n\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/monitorring/monitorring.page.scss":
/*!***************************************************!*\
  !*** ./src/app/monitorring/monitorring.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21vbml0b3JyaW5nL21vbml0b3JyaW5nLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/monitorring/monitorring.page.ts":
/*!*************************************************!*\
  !*** ./src/app/monitorring/monitorring.page.ts ***!
  \*************************************************/
/*! exports provided: MonitorringPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MonitorringPage", function() { return MonitorringPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MonitorringPage = /** @class */ (function () {
    function MonitorringPage() {
    }
    MonitorringPage.prototype.ngOnInit = function () {
    };
    MonitorringPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-monitorring',
            template: __webpack_require__(/*! ./monitorring.page.html */ "./src/app/monitorring/monitorring.page.html"),
            styles: [__webpack_require__(/*! ./monitorring.page.scss */ "./src/app/monitorring/monitorring.page.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], MonitorringPage);
    return MonitorringPage;
}());



/***/ })

}]);
//# sourceMappingURL=monitorring-monitorring-module.js.map