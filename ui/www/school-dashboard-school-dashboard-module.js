(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["school-dashboard-school-dashboard-module"],{

/***/ "./src/app/models/status/school-status.model.ts":
/*!******************************************************!*\
  !*** ./src/app/models/status/school-status.model.ts ***!
  \******************************************************/
/*! exports provided: SchoolStatusModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SchoolStatusModel", function() { return SchoolStatusModel; });
var SchoolStatusModel = /** @class */ (function () {
    //NOT_CREATED, CREATED, IN_PROGRESS, PENDING_APPROVAL, REJECTED, APPROVED, COMPLETED;
    function SchoolStatusModel() {
        this.schoolId = "";
        this.schoolProfile = "";
        this.needAssessment = "";
        this.actionPlan = "";
        this.monitoring = "";
    }
    SchoolStatusModel.prototype.getInitialStatus = function (_schoolId) {
        var status = new SchoolStatusModel();
        status.schoolId = _schoolId;
        status.schoolProfile = "CREATED";
        status.needAssessment = "NOT_CREATED";
        status.actionPlan = "NOT_CREATED";
        status.monitoring = "NOT_CREATED";
        return status;
    };
    return SchoolStatusModel;
}());



/***/ }),

/***/ "./src/app/school-dashboard/school-dashboard.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/school-dashboard/school-dashboard.module.ts ***!
  \*************************************************************/
/*! exports provided: SchoolDashboardPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SchoolDashboardPageModule", function() { return SchoolDashboardPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _school_dashboard_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./school-dashboard.page */ "./src/app/school-dashboard/school-dashboard.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _school_dashboard_page__WEBPACK_IMPORTED_MODULE_5__["SchoolDashboardPage"],
        children: [
            {
                path: 'need-assessment', loadChildren: '../need-assessment/need-assessment.module#NeedAssessmentPageModule'
            },
            {
                path: 'action-plan', loadChildren: '../action-plan/action-plan.module#ActionPlanPageModule'
            },
            {
                path: 'monitorring', loadChildren: '../monitorring/monitorring.module#MonitorringPageModule'
            }
        ]
    },
    {
        path: '',
        redirectTo: '/need-assessment',
        pathMatch: 'full'
    }
];
var SchoolDashboardPageModule = /** @class */ (function () {
    function SchoolDashboardPageModule() {
    }
    SchoolDashboardPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_school_dashboard_page__WEBPACK_IMPORTED_MODULE_5__["SchoolDashboardPage"]]
        })
    ], SchoolDashboardPageModule);
    return SchoolDashboardPageModule;
}());



/***/ }),

/***/ "./src/app/school-dashboard/school-dashboard.page.html":
/*!*************************************************************!*\
  !*** ./src/app/school-dashboard/school-dashboard.page.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n    <ion-toolbar>\r\n      <ion-buttons slot=\"start\">\r\n        <ion-menu-button></ion-menu-button>\r\n      </ion-buttons>\r\n      <ion-title>\r\n        School Dashboard - {{schoolName}}\r\n      </ion-title>\r\n    </ion-toolbar>\r\n  </ion-header>\r\n\r\n<ion-content padding>\r\n  <ion-grid>\r\n    <ion-row>\r\n      <ion-col>\r\n        <ion-card width=\"80\" (click)=\"launchSchoolInfo()\">\r\n            <ion-card-header>\r\n                <ion-card-title><h3><b>School Information</b></h3></ion-card-title>\r\n                <ion-card-subtitle>{{schoolStatus.schoolProfile}}</ion-card-subtitle>\r\n            </ion-card-header>\r\n            <img src=\"../../assets/children_1.jpg\" width=\"80\" height=\"180\" />\r\n          </ion-card>\r\n        </ion-col>\r\n        <ion-col>\r\n            <ion-card width=\"80\" (click)=\"launchNeedAssessment()\">\r\n                <ion-card-header>\r\n                    <ion-card-title><h3><b>Need Assessment</b></h3></ion-card-title>\r\n                    <ion-card-subtitle>{{schoolStatus.needAssessment}}</ion-card-subtitle>\r\n                </ion-card-header>\r\n                <img src=\"../../assets/assessment.jpg\" width=\"80\" height=\"180\" />\r\n              </ion-card>\r\n            </ion-col>\r\n            <ion-col>\r\n                <ion-card width=\"80\" (click)=\"launchActionPlan()\">\r\n                    <ion-card-header>\r\n                        <ion-card-title><h3><b>Action Plan</b></h3></ion-card-title>\r\n                        <ion-card-subtitle>{{schoolStatus.actionPlan}}</ion-card-subtitle>\r\n                    </ion-card-header>\r\n                    <img src=\"../../assets/actionplan.jpg\" width=\"80\" height=\"180\" />\r\n                  </ion-card>\r\n                </ion-col>\r\n                <ion-col>\r\n                    <ion-card width=\"80\">\r\n                        <ion-card-header>\r\n                            <ion-card-title><h3><b>Status Monitoring</b></h3></ion-card-title>\r\n                            <ion-card-subtitle>{{schoolStatus.monitoring}}</ion-card-subtitle>\r\n                          </ion-card-header>\r\n                        <img src=\"../../assets/monitoring.jpg\" width=\"80\" height=\"180\" />\r\n                      </ion-card>\r\n                    </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n\r\n<div style=\"text-align:center;\">\r\n  <ion-button color=\"tertiary\" *ngIf=\"exitSchool\">Exit Self-Sufficient School</ion-button>\r\n</div>\r\n    \r\n\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/school-dashboard/school-dashboard.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/school-dashboard/school-dashboard.page.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NjaG9vbC1kYXNoYm9hcmQvc2Nob29sLWRhc2hib2FyZC5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/school-dashboard/school-dashboard.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/school-dashboard/school-dashboard.page.ts ***!
  \***********************************************************/
/*! exports provided: SchoolDashboardPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SchoolDashboardPage", function() { return SchoolDashboardPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_school_status_status_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/school-status/status.service */ "./src/app/services/school-status/status.service.ts");
/* harmony import */ var _models_status_school_status_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/status/school-status.model */ "./src/app/models/status/school-status.model.ts");
/* harmony import */ var _services_local_store_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/local-store-service.service */ "./src/app/services/local-store-service.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SchoolDashboardPage = /** @class */ (function () {
    function SchoolDashboardPage(router, statusService, localStoreService) {
        this.router = router;
        this.statusService = statusService;
        this.localStoreService = localStoreService;
        this.schoolStatus = new _models_status_school_status_model__WEBPACK_IMPORTED_MODULE_3__["SchoolStatusModel"]();
    }
    SchoolDashboardPage.prototype.ngOnInit = function () {
        var _this = this;
        this.schoolId = this.localStoreService.schoolId;
        this.schoolName = this.localStoreService.schoolName;
        this.statusService.getSchoolStatus(this.schoolId)
            .subscribe(function (result) {
            console.log("Status", result);
            if (result == null) {
                //Insert a new record if the status does not exist
                _this.saveSchoolStatusDetails();
            }
            _this.intitializeSchoolStatus(result);
            _this.validateExitSchool();
        }, function (err) {
            console.log(err);
        });
    };
    SchoolDashboardPage.prototype.saveSchoolStatusDetails = function () {
        var _this = this;
        var data = this.schoolStatus.getInitialStatus(this.schoolId);
        // this.localStoreService.schoolStatus = this.schoolStatus;
        this.statusService.saveSchoolStatus(data).subscribe(function (res) {
            console.log("schoolStatusSave, success");
            _this.intitializeSchoolStatus(res);
            _this.validateExitSchool();
        }, function (err) {
            console.log(err);
        });
    };
    SchoolDashboardPage.prototype.intitializeSchoolStatus = function (statusJson) {
        this.schoolStatus = statusJson;
    };
    SchoolDashboardPage.prototype.validateExitSchool = function () {
        if (this.schoolStatus.schoolProfile == "COMPLETED" && this.schoolStatus.needAssessment == "COMPLETED"
            && this.schoolStatus.actionPlan == "COMPLETED" && this.schoolStatus.monitoring == "COMPLETED") {
            this.exitSchool = true;
        }
    };
    SchoolDashboardPage.prototype.launchSchoolInfo = function () {
        this.router.navigateByUrl('/school-info');
    };
    SchoolDashboardPage.prototype.launchNeedAssessment = function () {
        this.router.navigateByUrl('/need-assessment/health-sanitation');
    };
    SchoolDashboardPage.prototype.launchActionPlan = function () {
        this.router.navigateByUrl('/actionplan');
    };
    SchoolDashboardPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-school-dashboard',
            template: __webpack_require__(/*! ./school-dashboard.page.html */ "./src/app/school-dashboard/school-dashboard.page.html"),
            styles: [__webpack_require__(/*! ./school-dashboard.page.scss */ "./src/app/school-dashboard/school-dashboard.page.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _services_school_status_status_service__WEBPACK_IMPORTED_MODULE_2__["StatusService"],
            _services_local_store_service_service__WEBPACK_IMPORTED_MODULE_4__["LocalStoreServiceService"]])
    ], SchoolDashboardPage);
    return SchoolDashboardPage;
}());



/***/ }),

/***/ "./src/app/services/local-store-service.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/services/local-store-service.service.ts ***!
  \*********************************************************/
/*! exports provided: LocalStoreServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocalStoreServiceService", function() { return LocalStoreServiceService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LocalStoreServiceService = /** @class */ (function () {
    function LocalStoreServiceService() {
        this._isSearchSchoolFlow = true;
    }
    Object.defineProperty(LocalStoreServiceService.prototype, "sampleJson", {
        get: function () {
            var tmp = this._jsonData;
            return tmp;
        },
        set: function (jdata) {
            this._jsonData = jdata;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "isSearchSchoolFlow", {
        get: function () {
            var tmp = this._isSearchSchoolFlow;
            return tmp;
        },
        set: function (searchSchoolFlow) {
            this._isSearchSchoolFlow = searchSchoolFlow;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "schoolName", {
        get: function () {
            var tmp = this._schoolName;
            return tmp;
        },
        set: function (schoolProfilePage) {
            this._schoolName = schoolProfilePage;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "schoolId", {
        get: function () {
            var id = this._schoolId;
            return id;
        },
        set: function (id) {
            this._schoolId = id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "schoolProfileBasic", {
        get: function () {
            var tmp = this._schoolProfileBasic;
            return tmp;
        },
        set: function (schoolProfile) {
            this._schoolProfileBasic = schoolProfile;
            console.log(this._schoolProfileBasic.name);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "eduDeptFunction", {
        get: function () {
            var tmp = this._eduDeptFunction;
            return tmp;
        },
        set: function (eduDept) {
            this._eduDeptFunction = eduDept;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "teacherDetails", {
        get: function () {
            var tmp = this._teacherDetails;
            return tmp;
        },
        set: function (teacherDetail) {
            this._teacherDetails = teacherDetail;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "stakeholderInfo", {
        get: function () {
            var tmp = this._stakeholderInfo;
            return tmp;
        },
        set: function (stakeholderInformation) {
            this._stakeholderInfo = stakeholderInformation;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "otherInfo", {
        get: function () {
            var tmp = this._otherInfo;
            return tmp;
        },
        set: function (otherInformation) {
            this._otherInfo = otherInformation;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "healthSanitation", {
        get: function () {
            var tmp = this._healthSanitation;
            return tmp;
        },
        set: function (healthSanitation) {
            this._healthSanitation = healthSanitation;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LocalStoreServiceService.prototype, "classRoomEnvironment", {
        get: function () {
            var tmp = this._classRoomEnvironment;
            return tmp;
        },
        set: function (classRoomEnvironment) {
            this._classRoomEnvironment = classRoomEnvironment;
        },
        enumerable: true,
        configurable: true
    });
    LocalStoreServiceService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], LocalStoreServiceService);
    return LocalStoreServiceService;
}());



/***/ }),

/***/ "./src/app/services/school-status/status.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/services/school-status/status.service.ts ***!
  \**********************************************************/
/*! exports provided: StatusService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatusService", function() { return StatusService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _util_httpWrapper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../util/httpWrapper */ "./src/util/httpWrapper.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var StatusService = /** @class */ (function () {
    function StatusService(httpClient, alertController, router) {
        this.httpClient = httpClient;
        this.alertController = alertController;
        this.router = router;
        this.getSchoolStatusUrl = '/status/getSchoolStatus/';
        this.saveSchoolStatusUrl = '/status/saveSchoolStatus';
    }
    StatusService.prototype.getSchoolStatus = function (schoolId) {
        var _this = this;
        return this.httpClient.get(this.getSchoolStatusUrl + schoolId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(function (e) {
            var status = e.status;
            if (status === 401) {
                _this.showAlert('You are not authorized to perform this action');
            }
            else {
                _this.showAlert("Error in fetching school status data. Please try again.");
            }
            throw new Error(e);
        }));
        ;
    };
    StatusService.prototype.saveSchoolStatus = function (schoolStatusDetails) {
        return this.httpClient.post(this.saveSchoolStatusUrl, schoolStatusDetails);
    };
    StatusService.prototype.fetchSampleGetJson = function () {
        var json = {
            "schoolId": 1,
            "schoolProfile": "CREATED",
            "needAssessment": "NOT_CREATED",
            "actionPlan": "NOT_CREATED",
            "monitoring": "NOT_CREATED"
        };
        return json;
    };
    StatusService.prototype.fetchSampleSaveJson = function () {
        var json = {
            "schoolId": 1,
            "schoolProfile": "CREATED",
            "needAssessment": "NOT_CREATED",
            "actionPlan": "NOT_CREATED",
            "monitoring": "NOT_CREATED"
        };
        return json;
    };
    StatusService.prototype.showAlert = function (msg) {
        var _this = this;
        var alert = this.alertController.create({
            message: msg,
            buttons: [
                {
                    text: 'OK',
                    handler: function () {
                        _this.router.navigateByUrl('/home');
                    }
                }
            ]
        });
        alert.then(function (alert) { return alert.present(); });
    };
    StatusService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_util_httpWrapper__WEBPACK_IMPORTED_MODULE_1__["HttpWrapper"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], StatusService);
    return StatusService;
}());



/***/ })

}]);
//# sourceMappingURL=school-dashboard-school-dashboard-module.js.map