(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-page-home-page-module"],{

/***/ "./src/app/home-page/home-page.module.ts":
/*!***********************************************!*\
  !*** ./src/app/home-page/home-page.module.ts ***!
  \***********************************************/
/*! exports provided: HomePagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePagePageModule", function() { return HomePagePageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _home_page_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home-page.page */ "./src/app/home-page/home-page.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _home_page_page__WEBPACK_IMPORTED_MODULE_5__["HomePagePage"]
    }
];
var HomePagePageModule = /** @class */ (function () {
    function HomePagePageModule() {
    }
    HomePagePageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_home_page_page__WEBPACK_IMPORTED_MODULE_5__["HomePagePage"]]
        })
    ], HomePagePageModule);
    return HomePagePageModule;
}());



/***/ }),

/***/ "./src/app/home-page/home-page.page.html":
/*!***********************************************!*\
  !*** ./src/app/home-page/home-page.page.html ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-toolbar>\r\n      <!-- <img src=\"assets/icon/cwf_logo.png\" width=\"50px\" style=\"display:inline-block\" height=\"40px\"/> -->\r\n      <ion-buttons slot=\"secondary\">\r\n          <ion-button>\r\n            <ion-icon slot=\"icon-only\" name=\"log-out\"></ion-icon>\r\n          </ion-button>\r\n        </ion-buttons>\r\n    <ion-title class=\"titleicon\" style=\"display:inline-block\">Careworks</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content padding>\r\n    <ion-slides pager=\"true\" [options]=\"slideOpts\">\r\n        <ion-slide>\r\n          <h1>Slide 1</h1>\r\n        </ion-slide>\r\n        <ion-slide>\r\n          <h1>Slide 2</h1>\r\n        </ion-slide>\r\n        <ion-slide>\r\n          <h1>Slide 3</h1>\r\n        </ion-slide>\r\n      </ion-slides>\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/home-page/home-page.page.scss":
/*!***********************************************!*\
  !*** ./src/app/home-page/home-page.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".titleicon div {\n  display: inline-block;\n  margin-bottom: 10px; }\n\nion-slides {\n  margin-top: 30%;\n  background: #e6e6e6;\n  height: 30%; }\n\nion-slides img {\n    padding-top: 20px;\n    padding-bottom: 20px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS1wYWdlL0M6XFx2YXJzaGFcXGN3XFxjYXJld29ya3N1aVxcQ2FyZXdvcmtzL3NyY1xcYXBwXFxob21lLXBhZ2VcXGhvbWUtcGFnZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxxQkFBb0I7RUFDcEIsbUJBQWtCLEVBQUE7O0FBR3RCO0VBQ0ksZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixXQUFXLEVBQUE7O0FBSGY7SUFNUSxpQkFBaUI7SUFDakIsb0JBQW9CLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9ob21lLXBhZ2UvaG9tZS1wYWdlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi50aXRsZWljb24gZGl2e1xyXG4gICAgZGlzcGxheTppbmxpbmUtYmxvY2s7XHJcbiAgICBtYXJnaW4tYm90dG9tOjEwcHg7XHJcbn1cclxuXHJcbmlvbi1zbGlkZXMge1xyXG4gICAgbWFyZ2luLXRvcDogMzAlO1xyXG4gICAgYmFja2dyb3VuZDogI2U2ZTZlNjtcclxuICAgIGhlaWdodDogMzAlO1xyXG4gXHJcbiAgICBpbWcge1xyXG4gICAgICAgIHBhZGRpbmctdG9wOiAyMHB4O1xyXG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAyMHB4O1xyXG4gICAgfVxyXG59Il19 */"

/***/ }),

/***/ "./src/app/home-page/home-page.page.ts":
/*!*********************************************!*\
  !*** ./src/app/home-page/home-page.page.ts ***!
  \*********************************************/
/*! exports provided: HomePagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePagePage", function() { return HomePagePage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HomePagePage = /** @class */ (function () {
    // slideOpts = {
    //   initialSlide: 1,
    //   speed: 100,
    //   zoom: false,
    //   slidesPerView: 1.5,
    //   spaceBetween: 20,
    //   centeredSlides: true
    // };
    function HomePagePage() {
        this.slideOpts = {
            //initialSlide: 1,
            speed: 400,
            autoplay: true,
            loop: true
        };
    }
    HomePagePage.prototype.ngOnInit = function () {
    };
    HomePagePage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home-page',
            template: __webpack_require__(/*! ./home-page.page.html */ "./src/app/home-page/home-page.page.html"),
            styles: [__webpack_require__(/*! ./home-page.page.scss */ "./src/app/home-page/home-page.page.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], HomePagePage);
    return HomePagePage;
}());



/***/ })

}]);
//# sourceMappingURL=home-page-home-page-module.js.map