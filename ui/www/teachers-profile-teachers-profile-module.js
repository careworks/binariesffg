(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["teachers-profile-teachers-profile-module"],{

/***/ "./src/app/models/teachers-profile-model.ts":
/*!**************************************************!*\
  !*** ./src/app/models/teachers-profile-model.ts ***!
  \**************************************************/
/*! exports provided: TeachersProfileModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeachersProfileModel", function() { return TeachersProfileModel; });
var TeachersProfileModel = /** @class */ (function () {
    function TeachersProfileModel() {
    }
    return TeachersProfileModel;
}());



/***/ }),

/***/ "./src/app/teachers-profile/teachers-profile.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/teachers-profile/teachers-profile.module.ts ***!
  \*************************************************************/
/*! exports provided: TeachersProfilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeachersProfilePageModule", function() { return TeachersProfilePageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _teachers_profile_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./teachers-profile.page */ "./src/app/teachers-profile/teachers-profile.page.ts");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/fesm5/ng2-smart-table.js");
/* harmony import */ var _ag_grid_view_edit_mode_ag_grid_view_edit_mode_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../ag-grid-view-edit-mode/ag-grid-view-edit-mode.module */ "./src/app/ag-grid-view-edit-mode/ag-grid-view-edit-mode.module.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var routes = [
    {
        path: '',
        component: _teachers_profile_page__WEBPACK_IMPORTED_MODULE_5__["TeachersProfilePage"]
    }
];
var TeachersProfilePageModule = /** @class */ (function () {
    function TeachersProfilePageModule() {
    }
    TeachersProfilePageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                ng2_smart_table__WEBPACK_IMPORTED_MODULE_6__["Ng2SmartTableModule"],
                _ag_grid_view_edit_mode_ag_grid_view_edit_mode_module__WEBPACK_IMPORTED_MODULE_7__["AgGridViewEditModeModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatProgressSpinnerModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_teachers_profile_page__WEBPACK_IMPORTED_MODULE_5__["TeachersProfilePage"]]
        })
    ], TeachersProfilePageModule);
    return TeachersProfilePageModule;
}());



/***/ }),

/***/ "./src/app/teachers-profile/teachers-profile.page.html":
/*!*************************************************************!*\
  !*** ./src/app/teachers-profile/teachers-profile.page.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- \r\n<ion-content padding>\r\n<br><br>\r\n<div [hidden]=\"shouldHideTable\">\r\n<br>\r\n<ion-item class=\"textField\">\r\n\t<ion-button class=\"saveButton\" (click)=\"editFunc()\" block>Edit</ion-button>\r\n\t</ion-item>\r\n<ng2-smart-table [settings]=\"settings1\" [source]=\"source\"></ng2-smart-table>\r\n</div>\r\n<div [hidden]=\"!shouldHideTable\">\r\n<ion-item>\r\n<ion-button class=\"saveContinue\" (click)=\"saveandContinueFunc()\" block>Save and Continue</ion-button>\r\n<ion-button class=\"saveButton\" (click)=\"saveTeachers()\" block>Save</ion-button>\r\n</ion-item>\r\n<ng2-smart-table [settings]=\"settings\" [source]=\"source\" (deleteConfirm)=\"onDeleteConfirm($event)\" (editConfirm)=\"onSaveConfirm($event)\"\r\n (createConfirm)=\"onCreateConfirm($event)\"></ng2-smart-table>\r\n</div>\r\n</ion-content>\r\n -->\r\n\r\n <ion-card>\r\n\t<div *ngIf=\"!rowData\" style=\"height:400px; display: flex; justify-content: center; align-items: center\">\r\n\t\t<mat-spinner [diameter]=\"50\"></mat-spinner>\r\n\t</div>\r\n\t<div *ngIf=\"rowData\">\r\n\t\t<app-ag-grid-view-edit-mode [columnDefs]=\"columnDefs\" [editGridColumnDefs]=\"editGridColumnDefs\"\r\n\t\t[saveCallback] = \"saveCallback\"\r\n\t\t[saveandContinueCallback]=\"saveandContinueCallback\"\r\n\t\t[rowData]=\"rowData\"></app-ag-grid-view-edit-mode>  \r\n\t</div>\r\n</ion-card>\r\n"

/***/ }),

/***/ "./src/app/teachers-profile/teachers-profile.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/teachers-profile/teachers-profile.page.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/*\r\n.view {\r\ndisplay : none !important;\r\n}\r\ntable { \r\n\t\twidth: 100%; \r\n\t\tborder-collapse: collapse; \r\n\t}\r\n\t/* Zebra striping \r\n\ttr:nth-of-type(odd) { \r\n\t\tbackground: #eee; \r\n\t}\r\n\tth { \r\n\t\tbackground: #333; \r\n\t\tcolor: white; \r\n\t\tfont-weight: bold; \r\n\t}\r\n\ttd, th { \r\n\t\tpadding: 6px; \r\n\t\tborder: 1px solid #ccc; \r\n\t\ttext-align: left; \r\n\t}\r\n.second-header{\r\nbackground: #eee;\r\ncolor : black;\r\n}\r\n\r\n*/\n:host /deep/ ng2-smart-table table {\n  border-style: inset;\n  border-color: initial;\n  -webkit-border-image: initial;\n       -o-border-image: initial;\n          border-image: initial;\n  border-width: 2px;\n  display: table;\n  border-spacing: 2px;\n  border-color: grey; }\n:host /deep/ ng2-smart-table table > tbody > tr > td {\n  box-sizing: border-box;\n  border: 1px solid grey; }\n:host /deep/ ng2-smart-table thead > tr > th {\n  border: 1px solid grey; }\n:host /deep/ ng2-smart-table {\n  font-size: 16px; }\n.saveButton {\n  float: right;\n  width: 100px;\n  height: 30px;\n  font-size: 14px !important;\n  font-weight: 700; }\n.saveContinue {\n  float: right;\n  width: 200px;\n  height: 30px;\n  font-size: 14px !important;\n  font-weight: 700;\n  padding-right: 10px; }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGVhY2hlcnMtcHJvZmlsZS9DOlxcdmFyc2hhXFxjd1xcY2FyZXdvcmtzdWlcXENhcmV3b3Jrcy9zcmNcXGFwcFxcdGVhY2hlcnMtcHJvZmlsZVxcdGVhY2hlcnMtcHJvZmlsZS5wYWdlLnNjc3MiLCJzcmMvYXBwL3RlYWNoZXJzLXByb2ZpbGUvdGVhY2hlcnMtcHJvZmlsZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztDQzJCQztBREVEO0VBQ0ksbUJBQW1CO0VBQ25CLHFCQUFxQjtFQUNyQiw2QkFBcUI7T0FBckIsd0JBQXFCO1VBQXJCLHFCQUFxQjtFQUNyQixpQkFBaUI7RUFFakIsY0FBYztFQUNkLG1CQUFtQjtFQUNuQixrQkFBa0IsRUFBQTtBQUd0QjtFQUNJLHNCQUFzQjtFQUN0QixzQkFBc0IsRUFBQTtBQUcxQjtFQUNJLHNCQUFzQixFQUFBO0FBRTFCO0VBQ0ksZUFBYyxFQUFBO0FBRWxCO0VBRUksWUFBWTtFQUNaLFlBQVk7RUFDWixZQUFZO0VBQ1osMEJBQTBCO0VBQzFCLGdCQUFnQixFQUFBO0FBRXBCO0VBRUEsWUFBYTtFQUNiLFlBQWE7RUFDYixZQUFhO0VBQ2IsMEJBQTJCO0VBQzNCLGdCQUFnQjtFQUNoQixtQkFBbUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3RlYWNoZXJzLXByb2ZpbGUvdGVhY2hlcnMtcHJvZmlsZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG4udmlldyB7XHJcbmRpc3BsYXkgOiBub25lICFpbXBvcnRhbnQ7XHJcbn1cclxudGFibGUgeyBcclxuXHRcdHdpZHRoOiAxMDAlOyBcclxuXHRcdGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7IFxyXG5cdH1cclxuXHQvKiBaZWJyYSBzdHJpcGluZyBcclxuXHR0cjpudGgtb2YtdHlwZShvZGQpIHsgXHJcblx0XHRiYWNrZ3JvdW5kOiAjZWVlOyBcclxuXHR9XHJcblx0dGggeyBcclxuXHRcdGJhY2tncm91bmQ6ICMzMzM7IFxyXG5cdFx0Y29sb3I6IHdoaXRlOyBcclxuXHRcdGZvbnQtd2VpZ2h0OiBib2xkOyBcclxuXHR9XHJcblx0dGQsIHRoIHsgXHJcblx0XHRwYWRkaW5nOiA2cHg7IFxyXG5cdFx0Ym9yZGVyOiAxcHggc29saWQgI2NjYzsgXHJcblx0XHR0ZXh0LWFsaWduOiBsZWZ0OyBcclxuXHR9XHJcbi5zZWNvbmQtaGVhZGVye1xyXG5iYWNrZ3JvdW5kOiAjZWVlO1xyXG5jb2xvciA6IGJsYWNrO1xyXG59XHJcblxyXG4qL1xyXG5cclxuOmhvc3QgL2RlZXAvIG5nMi1zbWFydC10YWJsZSB0YWJsZXtcclxuICAgIGJvcmRlci1zdHlsZTogaW5zZXQ7XHJcbiAgICBib3JkZXItY29sb3I6IGluaXRpYWw7XHJcbiAgICBib3JkZXItaW1hZ2U6IGluaXRpYWw7XHJcbiAgICBib3JkZXItd2lkdGg6IDJweDtcclxuICAgIFxyXG4gICAgZGlzcGxheTogdGFibGU7XHJcbiAgICBib3JkZXItc3BhY2luZzogMnB4O1xyXG4gICAgYm9yZGVyLWNvbG9yOiBncmV5OyBcclxuXHJcbn1cclxuOmhvc3QgL2RlZXAvIG5nMi1zbWFydC10YWJsZSB0YWJsZSA+IHRib2R5ID4gdHIgPiB0ZCB7XHJcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgZ3JleTtcclxufVxyXG5cclxuOmhvc3QgL2RlZXAvIG5nMi1zbWFydC10YWJsZSB0aGVhZCA+IHRyID4gdGggIHsgXHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCBncmV5O1xyXG59ICBcclxuOmhvc3QgL2RlZXAvIG5nMi1zbWFydC10YWJsZSB7XHJcbiAgICBmb250LXNpemU6MTZweDtcclxufVxyXG4uc2F2ZUJ1dHRvbntcclxuICAgIFxyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgd2lkdGg6IDEwMHB4O1xyXG4gICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgZm9udC1zaXplOiAxNHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBmb250LXdlaWdodDogNzAwO1xyXG59XHJcbi5zYXZlQ29udGludWV7XHJcblxyXG5mbG9hdCA6IHJpZ2h0O1xyXG53aWR0aCA6IDIwMHB4O1xyXG5oZWlnaHQgOiAzMHB4O1xyXG5mb250LXNpemUgOiAxNHB4ICFpbXBvcnRhbnQ7XHJcbmZvbnQtd2VpZ2h0IDo3MDA7XHJcbnBhZGRpbmctcmlnaHQgOjEwcHg7XHJcbn1cclxuXHJcblxyXG5cclxuXHJcbiIsIi8qXHJcbi52aWV3IHtcclxuZGlzcGxheSA6IG5vbmUgIWltcG9ydGFudDtcclxufVxyXG50YWJsZSB7IFxyXG5cdFx0d2lkdGg6IDEwMCU7IFxyXG5cdFx0Ym9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTsgXHJcblx0fVxyXG5cdC8qIFplYnJhIHN0cmlwaW5nIFxyXG5cdHRyOm50aC1vZi10eXBlKG9kZCkgeyBcclxuXHRcdGJhY2tncm91bmQ6ICNlZWU7IFxyXG5cdH1cclxuXHR0aCB7IFxyXG5cdFx0YmFja2dyb3VuZDogIzMzMzsgXHJcblx0XHRjb2xvcjogd2hpdGU7IFxyXG5cdFx0Zm9udC13ZWlnaHQ6IGJvbGQ7IFxyXG5cdH1cclxuXHR0ZCwgdGggeyBcclxuXHRcdHBhZGRpbmc6IDZweDsgXHJcblx0XHRib3JkZXI6IDFweCBzb2xpZCAjY2NjOyBcclxuXHRcdHRleHQtYWxpZ246IGxlZnQ7IFxyXG5cdH1cclxuLnNlY29uZC1oZWFkZXJ7XHJcbmJhY2tncm91bmQ6ICNlZWU7XHJcbmNvbG9yIDogYmxhY2s7XHJcbn1cclxuXHJcbiovXG46aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHRhYmxlIHtcbiAgYm9yZGVyLXN0eWxlOiBpbnNldDtcbiAgYm9yZGVyLWNvbG9yOiBpbml0aWFsO1xuICBib3JkZXItaW1hZ2U6IGluaXRpYWw7XG4gIGJvcmRlci13aWR0aDogMnB4O1xuICBkaXNwbGF5OiB0YWJsZTtcbiAgYm9yZGVyLXNwYWNpbmc6IDJweDtcbiAgYm9yZGVyLWNvbG9yOiBncmV5OyB9XG5cbjpob3N0IC9kZWVwLyBuZzItc21hcnQtdGFibGUgdGFibGUgPiB0Ym9keSA+IHRyID4gdGQge1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICBib3JkZXI6IDFweCBzb2xpZCBncmV5OyB9XG5cbjpob3N0IC9kZWVwLyBuZzItc21hcnQtdGFibGUgdGhlYWQgPiB0ciA+IHRoIHtcbiAgYm9yZGVyOiAxcHggc29saWQgZ3JleTsgfVxuXG46aG9zdCAvZGVlcC8gbmcyLXNtYXJ0LXRhYmxlIHtcbiAgZm9udC1zaXplOiAxNnB4OyB9XG5cbi5zYXZlQnV0dG9uIHtcbiAgZmxvYXQ6IHJpZ2h0O1xuICB3aWR0aDogMTAwcHg7XG4gIGhlaWdodDogMzBweDtcbiAgZm9udC1zaXplOiAxNHB4ICFpbXBvcnRhbnQ7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7IH1cblxuLnNhdmVDb250aW51ZSB7XG4gIGZsb2F0OiByaWdodDtcbiAgd2lkdGg6IDIwMHB4O1xuICBoZWlnaHQ6IDMwcHg7XG4gIGZvbnQtc2l6ZTogMTRweCAhaW1wb3J0YW50O1xuICBmb250LXdlaWdodDogNzAwO1xuICBwYWRkaW5nLXJpZ2h0OiAxMHB4OyB9XG4iXX0= */"

/***/ }),

/***/ "./src/app/teachers-profile/teachers-profile.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/teachers-profile/teachers-profile.page.ts ***!
  \***********************************************************/
/*! exports provided: TeachersProfilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeachersProfilePage", function() { return TeachersProfilePage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_school_profile_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/school-profile.service */ "./src/app/services/school-profile.service.ts");
/* harmony import */ var _models_teachers_profile_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../models/teachers-profile-model */ "./src/app/models/teachers-profile-model.ts");
/* harmony import */ var _services_local_store_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/local-store-service.service */ "./src/app/services/local-store-service.service.ts");
/* harmony import */ var _util_httpWrapper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../util/httpWrapper */ "./src/util/httpWrapper.ts");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/fesm5/ng2-smart-table.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var TeachersProfilePage = /** @class */ (function () {
    function TeachersProfilePage(httpClient, schoolProfileService, localStoreService, router) {
        this.httpClient = httpClient;
        this.schoolProfileService = schoolProfileService;
        this.localStoreService = localStoreService;
        this.router = router;
        this.tabledata = [];
        // Testing Smart Tables 
        this.settings = {
            defaultStyle: false,
            actions: {
                position: 'right',
            },
            add: {
                addButtonContent: 'Add     ',
                createButtonContent: 'Create    ',
                cancelButtonContent: '    Cancel',
                confirmCreate: true,
            },
            edit: {
                editButtonContent: 'Edit  ',
                saveButtonContent: 'Update    ',
                cancelButtonContent: '    Cancel',
                confirmSave: true,
            },
            delete: {
                deleteButtonContent: '	Delete',
                confirmDelete: true,
            },
            columns: {
                employmentType: {
                    title: 'Teachers Type', filter: false
                },
                subject: {
                    title: 'Occupation Type', filter: false
                },
                gender: {
                    title: 'Gender', filter: false
                },
                teacherCount: {
                    title: 'Teacher Count', filter: false
                }
            },
            attr: {
                class: 'table table-bordered table-striped'
            }
        };
        this.settings1 = {
            defaultStyle: false,
            actions: false,
            hideSubHeader: true,
            columns: {
                employmentType: {
                    title: 'Teachers Type', filter: false
                },
                subject: {
                    title: 'Occupation Type', filter: false
                },
                gender: {
                    title: 'Gender', filter: false
                },
                teacherCount: {
                    title: 'Teacher Count', filter: false
                }
            },
            attr: {
                class: 'table table-bordered table-striped'
            }
        };
        //Testing Ends
        this.parsedschoolDetailsJson = null;
        this.size = null;
        this.maleFulltimetotal = 0;
        this.femaleFulltimetotal = 0;
        this.maleParttimetotal = 0;
        this.femaleParttimetotal = 0;
        this.arttotal = 0;
        this.petotal = 0;
        this.comptotal = 0;
        this.totalTeachers = 0;
        this.te = new _models_teachers_profile_model__WEBPACK_IMPORTED_MODULE_2__["TeachersProfileModel"]();
        this.shouldHideTable = true;
    }
    TeachersProfilePage.prototype.onDeleteConfirm = function (event) {
        console.log("Delete Event In Console");
        console.log(event);
        if (window.confirm('Are you sure you want to delete?')) {
            event.confirm.resolve();
        }
        else {
            event.confirm.reject();
        }
    };
    TeachersProfilePage.prototype.onCreateConfirm = function (event) {
        console.log("Create Event In Console");
        console.log(event);
        if (window.confirm('Are you sure you want to add new entry?')) {
        }
        else {
            event.confirm.reject();
        }
    };
    TeachersProfilePage.prototype.onSaveConfirm = function (event) {
        console.log("Edit Event In Console");
        console.log(event);
        if (window.confirm('Are you sure you want to update?')) {
            event.confirm.resolve();
        }
        else {
            event.confirm.reject();
        }
    };
    TeachersProfilePage.prototype.getEditGridColumnDefs = function () {
        this.editGridColumnDefs = [
            { headerName: '', checkboxSelection: true, pinned: 'left', width: 50 },
            { headerName: 'Teachers Type', field: 'employmentType', tooltipField: 'employmentType', editable: true },
            { headerName: 'Occupation Type', field: 'subject', tooltipField: 'subject', editable: true },
            { headerName: 'Gender', field: 'gender', tooltipField: 'gender', editable: true },
            { headerName: 'Teacher Count', field: 'teacherCount', tooltipField: 'teacherCount', editable: true }
        ];
    };
    TeachersProfilePage.prototype.getColumnDefs = function () {
        this.columnDefs = [
            { headerName: 'Teachers Type', field: 'employmentType', tooltipField: 'employmentType' },
            { headerName: 'Occupation Type', field: 'subject', tooltipField: 'subject' },
            { headerName: 'Gender', field: 'gender', tooltipField: 'gender' },
            { headerName: 'Teacher Count', field: 'teacherCount', tooltipField: 'teacherCount' }
        ];
    };
    TeachersProfilePage.prototype.saveCallback = function (data) {
        console.log("teacher save callback", data);
        this.httpClient.put("/school/teacherGroups/" + this.school_Id, JSON.stringify(data))
            .subscribe(function (data) {
            console.log(data);
        });
    };
    TeachersProfilePage.prototype.saveandContinueCallback = function (data) {
        console.log("teacher save and continue callback", data);
    };
    TeachersProfilePage.prototype.getRowData = function (school_Id) {
        var _this = this;
        this.httpClient
            .get("/school/teacherGroups/" + school_Id)
            .subscribe(function (data) {
            console.log("data", data);
            _this.rowData = data;
        });
        console.log("this.rowData", this.rowData);
        // this.rowData =  [
        //         { id: '1', activity: 'act1', stakeholder: 'test', resource: 'Money', responsible: 'CWF',deadline: '29-04-2019' },
        //         { id: '2', activity: 'act1', stakeholder: 'test', resource: 'Money', responsible: 'CWF',deadline: '29-04-2019' },
        //         { id: '3', activity: 'act1', stakeholder: 'test', resource: 'Money', responsible: 'CWF',deadline: '29-04-2019' }];
    };
    TeachersProfilePage.prototype.ngOnInit = function () {
        var _this = this;
        this.getColumnDefs();
        this.getEditGridColumnDefs();
        this.data = [];
        this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_5__["LocalDataSource"](this.data);
        console.log('this.localStoreService.isSearchSchoolFlow', this.localStoreService);
        if (this.localStoreService.isSearchSchoolFlow) {
            this.shouldHideTable = false;
            // this.initialisationTextBox();
            this.data = this.localStoreService.sampleJson.schoolAdvancedProfile.teacherDetails;
            this.source.load(this.data);
            this.school_Id = this.localStoreService.sampleJson.schoolProfileBasic.schoolId;
            console.log('this.school_Id', this.school_Id);
        }
        else {
            this.shouldHideTable = true;
            //console.log(this.localStoreService.schoolName);
            this.httpClient.get("/school/SchoolProfilesByName/?name=" + this.localStoreService.schoolName).subscribe(function (res) {
                console.log(res[0]);
                _this.schoolByName = JSON.parse(JSON.stringify(res[0]));
            });
            this.source.load(this.data);
        }
        this.getRowData(this.school_Id);
        //console.log(this.schoolByName)
    };
    TeachersProfilePage.prototype.initialisationTextBox = function () {
        this.parsedschoolDetailsJson = this.schoolProfileService.getSampleJson().schoolProfileAdvanced.teacherDetails;
        this.size = this.parsedschoolDetailsJson.length;
        for (var i = 0; i < this.size; i++) {
            // Parsing JSON and fetching the Teacher Count 
            if ((this.parsedschoolDetailsJson[i].teacherType == 'Full-time') && (this.parsedschoolDetailsJson[i].gender == 'Male')) {
                this.maleFulltimetotal += parseInt(this.parsedschoolDetailsJson[i].teacherCount);
            }
            if ((this.parsedschoolDetailsJson[i].teacherType == 'Full-time') && (this.parsedschoolDetailsJson[i].gender == 'Female')) {
                this.femaleFulltimetotal += parseInt(this.parsedschoolDetailsJson[i].teacherCount);
            }
            if ((this.parsedschoolDetailsJson[i].teacherType == 'Part-time') && (this.parsedschoolDetailsJson[i].gender == 'Male')) {
                this.maleParttimetotal += parseInt(this.parsedschoolDetailsJson[i].teacherCount);
            }
            if ((this.parsedschoolDetailsJson[i].teacherType == 'Part-time') && (this.parsedschoolDetailsJson[i].gender == 'Female')) {
                this.femaleParttimetotal += parseInt(this.parsedschoolDetailsJson[i].teacherCount);
            }
            if (this.parsedschoolDetailsJson[i].occupationType == 'Art & Craft') {
                this.arttotal += parseInt(this.parsedschoolDetailsJson[i].teacherCount);
            }
            if (this.parsedschoolDetailsJson[i].occupationType == 'Physical-Education') {
                this.petotal += parseInt(this.parsedschoolDetailsJson[i].teacherCount);
            }
            if (this.parsedschoolDetailsJson[i].occupationType == 'Computer') {
                this.comptotal += parseInt(this.parsedschoolDetailsJson[i].teacherCount);
            }
        }
        this.totalTeachers = this.maleFulltimetotal + this.femaleFulltimetotal + this.maleParttimetotal + this.femaleParttimetotal + this.arttotal + this.petotal + this.comptotal;
        this.te.maleFulltimetotal = this.maleFulltimetotal;
        this.te.femaleFulltimetotal = this.femaleFulltimetotal;
        this.te.maleParttimetotal = this.maleParttimetotal;
        this.te.femaleParttimetotal = this.femaleParttimetotal;
        this.te.arttotal = this.arttotal;
        this.te.petotal = this.petotal;
        this.te.comptotal = this.comptotal;
        this.te.totalTeachers = this.totalTeachers;
    };
    TeachersProfilePage.prototype.saveTeachers = function () {
        var _this = this;
        this.source.getAll().then(function (res) {
            console.log(JSON.stringify(res));
            _this.httpClient.put("/school/teacherGroups/" + _this.school_Id, JSON.stringify(res))
                .subscribe(function (data) {
                console.log(data);
            });
        });
        this.shouldHideTable = false;
    };
    TeachersProfilePage.prototype.editFunc = function () {
        this.shouldHideTable = true;
    };
    TeachersProfilePage.prototype.saveandContinueFunc = function () {
        var _this = this;
        this.source.getAll().then(function (res) {
            console.log(JSON.stringify(res));
            _this.httpClient.put("/school/teacherGroups/" + _this.school_Id, JSON.stringify(res))
                .subscribe(function (data) {
                console.log(data);
            });
        });
        // Move to Next Tab 
        this.router.navigate(['../school-info/education-dept']);
    };
    TeachersProfilePage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-teachers-profile',
            template: __webpack_require__(/*! ./teachers-profile.page.html */ "./src/app/teachers-profile/teachers-profile.page.html"),
            styles: [__webpack_require__(/*! ./teachers-profile.page.scss */ "./src/app/teachers-profile/teachers-profile.page.scss")]
        }),
        __metadata("design:paramtypes", [_util_httpWrapper__WEBPACK_IMPORTED_MODULE_4__["HttpWrapper"], _services_school_profile_service__WEBPACK_IMPORTED_MODULE_1__["SchoolProfileService"], _services_local_store_service_service__WEBPACK_IMPORTED_MODULE_3__["LocalStoreServiceService"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]])
    ], TeachersProfilePage);
    return TeachersProfilePage;
}());



/***/ })

}]);
//# sourceMappingURL=teachers-profile-teachers-profile-module.js.map