(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["school-info-school-info-module"],{

/***/ "./src/app/school-info/school-info.module.ts":
/*!***************************************************!*\
  !*** ./src/app/school-info/school-info.module.ts ***!
  \***************************************************/
/*! exports provided: SchoolInfoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SchoolInfoPageModule", function() { return SchoolInfoPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/index.js");
/* harmony import */ var _school_info_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./school-info.page */ "./src/app/school-info/school-info.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _school_info_page__WEBPACK_IMPORTED_MODULE_5__["SchoolInfoPage"],
        children: [
            { path: 'school-profile',
                loadChildren: '../school-profile/school-profile.module#SchoolProfilePageModule' },
            { path: 'student-profile',
                loadChildren: '../student-profile/student-profile.module#StudentProfilePageModule' },
            { path: 'teachers-profile',
                loadChildren: '../teachers-profile/teachers-profile.module#TeachersProfilePageModule' },
            { path: 'education-dept',
                loadChildren: '../education-dept/education-dept.module#EducationDeptPageModule' },
            { path: 'stakeholders-profile',
                loadChildren: '../stakeholders-profile/stakeholders-profile.module#StakeholdersProfilePageModule' },
            { path: 'other-info',
                loadChildren: '../other-info/other-info.module#OtherInfoPageModule' },
            { path: 'school-profile/:id',
                loadChildren: '../school-profile/school-profile.module#SchoolProfilePageModule' }
        ]
    },
    {
        path: '',
        redirectTo: '/school-profile',
        pathMatch: 'full'
    }
];
var SchoolInfoPageModule = /** @class */ (function () {
    function SchoolInfoPageModule() {
    }
    SchoolInfoPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_school_info_page__WEBPACK_IMPORTED_MODULE_5__["SchoolInfoPage"]]
        })
    ], SchoolInfoPageModule);
    return SchoolInfoPageModule;
}());



/***/ }),

/***/ "./src/app/school-info/school-info.page.html":
/*!***************************************************!*\
  !*** ./src/app/school-info/school-info.page.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>School Information</ion-title>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header> -->\r\n\r\n<ion-header>\r\n\t<ion-toolbar>\r\n\t  <ion-buttons slot=\"start\">\r\n\t\t<ion-menu-button></ion-menu-button>\r\n\t  </ion-buttons>\r\n\t  <ion-title>\r\n\t\tSchool Information - {{schoolName}}\r\n\t  </ion-title>\r\n\t</ion-toolbar>\r\n  </ion-header>\r\n\r\n<ion-content padding>\r\n\t<ion-tabs tab=\"school-profile\">\r\n\t\t<ion-tab-bar slot=\"top\" color=\"primary\">\r\n\t    \r\n\t\t    <ion-tab-button tab=\"school-profile\">\r\n\t\t      <ion-icon name=\"school\"></ion-icon>\r\n\t\t      <ion-label>School Details</ion-label>\r\n\t\t    </ion-tab-button>\r\n\r\n\t\t    <ion-tab-button tab=\"student-profile\">\r\n\t\t      <ion-icon name=\"contacts\"></ion-icon>\r\n\t\t      <ion-label>Student Details</ion-label>\r\n\t\t    </ion-tab-button>\r\n\r\n\t\t    <ion-tab-button tab=\"teachers-profile\">\r\n\t\t      <ion-icon name=\"people\"></ion-icon>\r\n\t\t      <ion-label>Teachers Details</ion-label>\r\n\t\t\t\t</ion-tab-button>\t\r\n\t\t\t\t<ion-tab-button tab=\"education-dept\">\r\n\t\t      <ion-icon name=\"home\"></ion-icon>\r\n\t\t      <ion-label>Education Dept. Details</ion-label>\r\n\t\t    </ion-tab-button>\r\n\r\n\t\t    <ion-tab-button tab=\"stakeholders-profile\">\r\n\t\t      <ion-icon name=\"person\"></ion-icon>\r\n\t\t      <ion-label>Stakeholders Details</ion-label>\r\n\t\t    </ion-tab-button>\r\n\r\n\t\t     <ion-tab-button tab=\"other-info\">\r\n\t\t      <ion-icon name=\"information-circle\"></ion-icon>\r\n\t\t      <ion-label>Other Info</ion-label>\r\n\t\t    </ion-tab-button>\r\n\r\n\t\t</ion-tab-bar>\r\n\t</ion-tabs>\r\n\t<!-- <ion-tabs>\r\n\t\t<ion-tab-bar slot=\"bottom\">\r\n\t\t    <ion-tab-button tab=\"submit\" color=\"secondary\">\r\n\t\t      <ion-icon name=\"save\"></ion-icon>\r\n\t\t      <ion-label>Submit</ion-label>\r\n\t\t    </ion-tab-button>\r\n\t\t</ion-tab-bar> \r\n\t</ion-tabs>\t -->\t   \t\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/school-info/school-info.page.scss":
/*!***************************************************!*\
  !*** ./src/app/school-info/school-info.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-tab-bar {\n  overflow-x: scroll;\n  -webkit-box-pack: left;\n          justify-content: left;\n  /*height: 80px;*/\n  margin-bottom: -1px; }\n\n::-webkit-scrollbar,\n*::-webkit-scrollbar {\n  display: none; }\n\n.tab-selected {\n  background: #3B5999; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2Nob29sLWluZm8vQzpcXHZhcnNoYVxcY3dcXGNhcmV3b3Jrc3VpXFxDYXJld29ya3Mvc3JjXFxhcHBcXHNjaG9vbC1pbmZvXFxzY2hvb2wtaW5mby5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyxrQkFBa0I7RUFDbEIsc0JBQXFCO1VBQXJCLHFCQUFxQjtFQUNyQixnQkFBQTtFQUNBLG1CQUFtQixFQUFBOztBQUdwQjs7RUFFRSxhQUFhLEVBQUE7O0FBR2Y7RUFDQyxtQkFBbUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3NjaG9vbC1pbmZvL3NjaG9vbC1pbmZvLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi10YWItYmFyIHtcclxuXHRvdmVyZmxvdy14OiBzY3JvbGw7XHJcblx0anVzdGlmeS1jb250ZW50OiBsZWZ0O1xyXG5cdC8qaGVpZ2h0OiA4MHB4OyovXHJcblx0bWFyZ2luLWJvdHRvbTogLTFweDtcclxufVxyXG5cclxuOjotd2Via2l0LXNjcm9sbGJhcixcclxuKjo6LXdlYmtpdC1zY3JvbGxiYXIge1xyXG4gIGRpc3BsYXk6IG5vbmU7XHJcbn1cclxuXHJcbi50YWItc2VsZWN0ZWQge1xyXG5cdGJhY2tncm91bmQ6ICMzQjU5OTk7XHJcbn0gXHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/school-info/school-info.page.ts":
/*!*************************************************!*\
  !*** ./src/app/school-info/school-info.page.ts ***!
  \*************************************************/
/*! exports provided: SchoolInfoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SchoolInfoPage", function() { return SchoolInfoPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_school_profile_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/school-profile.service */ "./src/app/services/school-profile.service.ts");
/* harmony import */ var _app_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../app.config */ "./src/app/app.config.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SchoolInfoPage = /** @class */ (function () {
    function SchoolInfoPage(activatedRoute, router, schoolProfileService) {
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.schoolProfileService = schoolProfileService;
        this.schoolId = '';
        this.schoolName = _app_config__WEBPACK_IMPORTED_MODULE_3__["AppConfig"].schoolName;
    }
    SchoolInfoPage.prototype.ngOnInit = function () {
    };
    SchoolInfoPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-school-info',
            template: __webpack_require__(/*! ./school-info.page.html */ "./src/app/school-info/school-info.page.html"),
            styles: [__webpack_require__(/*! ./school-info.page.scss */ "./src/app/school-info/school-info.page.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _services_school_profile_service__WEBPACK_IMPORTED_MODULE_2__["SchoolProfileService"]])
    ], SchoolInfoPage);
    return SchoolInfoPage;
}());



/***/ })

}]);
//# sourceMappingURL=school-info-school-info-module.js.map