-- MySQL Script generated by MySQL Workbench
-- Wed 03 Apr 2019 01:44:53 PM UTC
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema care_works
--
-- FFG Care Woks Foundation by JP Morgan
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `care_works` DEFAULT CHARACTER SET utf8 ;
USE `care_works` ;

-- -----------------------------------------------------
-- Table `care_works`.`social_division`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `care_works`.`social_division` ;

CREATE TABLE IF NOT EXISTS `care_works`.`social_division` (
  `name` VARCHAR(45) NOT NULL,
  `description` VARCHAR(45) NULL,
  PRIMARY KEY (`name`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `care_works`.`edu_dept_function`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `care_works`.`edu_dept_function` ;

CREATE TABLE IF NOT EXISTS `care_works`.`edu_dept_function` (
  `school_id` BIGINT NOT NULL,
  `designation` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NULL,
  `contact_details` VARCHAR(45) NULL,
  `email_id` VARCHAR(45) NULL,
  PRIMARY KEY (`school_id`, `designation`),
  CONSTRAINT `fk_edu_dept_function_10`
    FOREIGN KEY (`school_id`)
    REFERENCES `care_works`.`school_profile` (`school_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_edu_dept_function_1_idx` ON `care_works`.`edu_dept_function` (`school_id` ASC);


-- -----------------------------------------------------
-- Table `care_works`.`health_sanitation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `care_works`.`health_sanitation` ;

CREATE TABLE IF NOT EXISTS `care_works`.`health_sanitation` (
  `school_id` BIGINT NOT NULL,
  `category` VARCHAR(45) NOT NULL,
  `sub_category` VARCHAR(45) NOT NULL,
  `has_implemented` VARCHAR(45) NOT NULL,
  `remarks` VARCHAR(45) NULL,
  PRIMARY KEY (`school_id`, `category`, `has_implemented`),
  CONSTRAINT `fk_health_sanitation_1`
    FOREIGN KEY (`school_id`)
    REFERENCES `care_works`.`school_profile` (`school_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `care_works`.`toilet_details`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `care_works`.`toilet_details` ;

CREATE TABLE IF NOT EXISTS `care_works`.`toilet_details` (
  `school_id` BIGINT NOT NULL,
  `details` VARCHAR(45) NOT NULL,
  `available` INT NULL,
  `repair` INT NULL,
  `new` INT NULL,
  CONSTRAINT `fk_toilet_details_1`
    FOREIGN KEY (`school_id`)
    REFERENCES `care_works`.`school_profile` (`school_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `care_works`.`water_purifier`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `care_works`.`water_purifier` ;

CREATE TABLE IF NOT EXISTS `care_works`.`water_purifier` (
  `school_id` BIGINT NOT NULL,
  `details` VARCHAR(45) NOT NULL,
  `available` INT NULL,
  `repair` INT NULL,
  `new` INT NULL,
  CONSTRAINT `fk_water_purifier_1`
    FOREIGN KEY (`school_id`)
    REFERENCES `care_works`.`school_profile` (`school_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `care_works`.`classroom_env`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `care_works`.`classroom_env` ;

CREATE TABLE IF NOT EXISTS `care_works`.`classroom_env` (
  `school_id` BIGINT NOT NULL,
  `category` VARCHAR(45) NOT NULL,
  `sub_category` VARCHAR(45) NULL,
  `has_implemented` VARCHAR(45) NOT NULL,
  `remarks` VARCHAR(45) NULL,
  PRIMARY KEY (`category`, `school_id`, `has_implemented`),
  CONSTRAINT `fk_classroom_env_1`
    FOREIGN KEY (`school_id`)
    REFERENCES `care_works`.`school_profile` (`school_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `care_works`.`computer_lab_details`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `care_works`.`computer_lab_details` ;

CREATE TABLE IF NOT EXISTS `care_works`.`computer_lab_details` (
  `school_id` BIGINT NOT NULL,
  `details` VARCHAR(45) NOT NULL,
  `available` INT NULL,
  `repair` INT NULL,
  `new` INT NULL,
  CONSTRAINT `fk_computer_lab_details_1`
    FOREIGN KEY (`school_id`)
    REFERENCES `care_works`.`school_profile` (`school_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `care_works`.`science_lab`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `care_works`.`science_lab` ;

CREATE TABLE IF NOT EXISTS `care_works`.`science_lab` (
  `school_id` BIGINT NOT NULL,
  `details` VARCHAR(45) NOT NULL,
  `has_implemented` VARCHAR(45) NOT NULL,
  CONSTRAINT `fk_science_lab_1`
    FOREIGN KEY (`school_id`)
    REFERENCES `care_works`.`school_profile` (`school_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `care_works`.`status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `care_works`.`status` ;

CREATE TABLE IF NOT EXISTS `care_works`.`status` (
  `status` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`status`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `care_works`.`school_need_assess_status_map`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `care_works`.`school_need_assess_status_map` ;

CREATE TABLE IF NOT EXISTS `care_works`.`school_need_assess_status_map` (
  `school_id` BIGINT NOT NULL,
  `school_profile` VARCHAR(45) NOT NULL,
  `need_assessment` VARCHAR(45) NOT NULL,
  `action_plan` VARCHAR(45) NOT NULL,
  `monitoring` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`school_id`, `school_profile`),
  CONSTRAINT `fk_school_need_assess_status_1`
    FOREIGN KEY (`school_id`)
    REFERENCES `care_works`.`school_profile` (`school_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_school_need_assess_status_map_1`
    FOREIGN KEY (`school_profile`)
    REFERENCES `care_works`.`status` (`status`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_school_need_assess_status_map_1_idx` ON `care_works`.`school_need_assess_status_map` (`school_profile` ASC);


-- -----------------------------------------------------
-- Table `care_works`.`school_env`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `care_works`.`school_env` ;

CREATE TABLE IF NOT EXISTS `care_works`.`school_env` (
  `school_id` BIGINT NOT NULL,
  `particulars` VARCHAR(45) NOT NULL,
  `has_implemented` VARCHAR(45) NOT NULL,
  `total_num` INT NULL,
  `renovation_required` VARCHAR(45) NULL,
  `flooring` VARCHAR(45) NULL,
  `plastering` VARCHAR(45) NULL,
  `water_proofing` VARCHAR(45) NULL,
  `electrification` VARCHAR(45) NULL,
  `plumbing` VARCHAR(45) NULL,
  `blackboard_painting` VARCHAR(45) NULL,
  `wall_mounted_table` VARCHAR(45) NULL,
  `nalikali_chapara` VARCHAR(45) NULL,
  `safety_grill` VARCHAR(45) NULL,
  `external_painting` VARCHAR(45) NULL,
  `internal_painting` VARCHAR(45) NULL,
  `remarks` VARCHAR(45) NULL,
  PRIMARY KEY (`school_id`, `particulars`),
  CONSTRAINT `fk_school_env_1`
    FOREIGN KEY (`school_id`)
    REFERENCES `care_works`.`school_profile` (`school_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `care_works`.`school_status_map`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `care_works`.`school_status_map` ;

CREATE TABLE IF NOT EXISTS `care_works`.`school_status_map` (
  `school_id` BIGINT NOT NULL,
  `school_profile` VARCHAR(45) NOT NULL,
  `need_assessment` VARCHAR(45) NOT NULL,
  `action_plan` VARCHAR(45) NOT NULL,
  `monitoring` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`school_id`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `care_works`.`edu_dept_function`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `care_works`.`edu_dept_function` ;

CREATE TABLE IF NOT EXISTS `care_works`.`edu_dept_function` (
  `school_id` BIGINT NOT NULL,
  `designation` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NULL,
  `contact_details` VARCHAR(45) NULL,
  `email_id` VARCHAR(45) NULL,
  PRIMARY KEY (`school_id`, `designation`),
  CONSTRAINT `fk_edu_dept_function_10`
    FOREIGN KEY (`school_id`)
    REFERENCES `care_works`.`school_profile` (`school_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_edu_dept_function_1_idx` ON `care_works`.`edu_dept_function` (`school_id` ASC);






















