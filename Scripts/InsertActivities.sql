-- -----------------------------------------------------
-- Data for table `care_works`.`activity_master`
-- -----------------------------------------------------
START TRANSACTION;
USE `care_works`;
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('1', '1', 'Toilets & Urinal renevation / new toilet & Urinal construction');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('2', '1', 'Hand wash area');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('3', '1', 'Drinking water facility');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('4', '1', 'Electrification and plumbing');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('5', '1', 'Compound wall, fencing & Main gate');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('6', '1', 'Retro fitting');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('7', '1', 'UG Sump, Bore well & Overhead tank (OHT)');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('8', '1', 'Internal & external painting');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('9', '1', 'Art work');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('10', '1', 'Water proofing');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('11', '1', 'Light proof');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('1', '2', 'Office room & staffroom set up');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('2', '2', 'Nalikali classroom set up');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('3', '2', 'Library infra set up');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('4', '2', 'Computer Lab infra set up');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('5', '2', 'Science Lab infra set up');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('6', '2', 'Classroom set up');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('7', '2', 'Wall Art Work');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('8', '2', 'Teaching Aids');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('9', '2', 'Blackboard Repair/Replacement');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('10', '2', 'Furniture Requirement');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('1', '3', 'Health check-up and follow up (Dental and Eye)');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('2', '3', 'General check-up');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('3', '3', 'First Aid Box and medicine');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('4', '3', 'Training on hygeine and sanitation');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('5', '3', 'Medicines distributed by DOPI');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('6', '3', 'School mental health - Counselling and training');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('1', '4', 'Strengthening School Parliament');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('2', '4', 'Strengthening SDMC');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('3', '4', 'Parents meeting');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('4', '4', 'Teachers training - Computer');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('5', '4', 'Teachers training - English');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('6', '4', 'School Mapping');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('1', '5', 'Sports Materials Requirements');
INSERT INTO `care_works`.`activity_master` (`activity_id`, `category_id`, `activity_detail`) VALUES ('2', '5', 'Life Skill for Students');


COMMIT;

