-- -----------------------------------------------------
-- Data for table `care_works`.`school_category`
-- -----------------------------------------------------
START TRANSACTION;
USE `care_works`;
INSERT INTO `care_works`.`school_category` (`category`, `description`) VALUES ('GLPS', 'Govt. Lower Primary School (1-5th)');
INSERT INTO `care_works`.`school_category` (`category`, `description`) VALUES ('GHPS', 'Govt. Higher Primary School (1-7/8th)');
INSERT INTO `care_works`.`school_category` (`category`, `description`) VALUES ('GMPS', 'Govt. Model Primary School (1-7/8th)');
INSERT INTO `care_works`.`school_category` (`category`, `description`) VALUES ('GHS', 'Govt. High School (8-10th)');

COMMIT;


-- -----------------------------------------------------
-- Data for table `care_works`.`school_type`
-- -----------------------------------------------------
START TRANSACTION;
USE `care_works`;
INSERT INTO `care_works`.`school_type` (`type`, `description`) VALUES ('Boys', 'Boys School');
INSERT INTO `care_works`.`school_type` (`type`, `description`) VALUES ('Girls', 'Girls School');
INSERT INTO `care_works`.`school_type` (`type`, `description`) VALUES ('Co-educaton', '');

COMMIT;


-- -----------------------------------------------------
-- Data for table `care_works`.`school_medium`
-- -----------------------------------------------------
START TRANSACTION;
USE `care_works`;
INSERT INTO `care_works`.`medium` (`medium`) VALUES ('Kannada');
INSERT INTO `care_works`.`medium` (`medium`) VALUES ('English');
INSERT INTO `care_works`.`medium` (`medium`) VALUES ('Tamil');
INSERT INTO `care_works`.`medium` (`medium`) VALUES ('Urdu');
INSERT INTO `care_works`.`medium` (`medium`) VALUES ('Telugu');
INSERT INTO `care_works`.`medium` (`medium`) VALUES ('Others');

COMMIT;


-- -----------------------------------------------------
-- Data for table `care_works`.`role`
-- -----------------------------------------------------
START TRANSACTION;
USE `care_works`;
INSERT INTO `care_works`.`roles` (`name`,`description`) VALUES ('Admin', 'Has access to all editable info');
INSERT INTO `care_works`.`roles` (`name`,`description`) VALUES ('User', 'Has only read access');

COMMIT;
