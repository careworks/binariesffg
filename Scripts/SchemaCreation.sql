SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema care_works
-- -----------------------------------------------------
-- FFG Care Woks Foundation by JP Morgan
DROP SCHEMA IF EXISTS `care_works` ;

-- -----------------------------------------------------
-- Schema care_works
--
-- FFG Care Woks Foundation by JP Morgan
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `care_works` DEFAULT CHARACTER SET utf8 ;
USE `care_works` ;